#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Fichier::Operation
// ----------------------------------

/* Obtenir taille */
long NLib_Fichier_Operation_ObtenirTaille( FILE *f )
{
	// Position initiale
	long positionInitiale;

	// Taille du fichier
	__OUTPUT long taille;

	// Obtenir position initiale
	positionInitiale = ftell( f );

	// Aller � la fin du fichier
	fseek( f,
		0,
		SEEK_END );

	// Obtenir taille
	taille = ftell( f );

	// Restaurer position
	fseek( f,
		positionInitiale,
		SEEK_SET );

	// OK
	return taille;
}

long NLib_Fichier_Operation_ObtenirTaille2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT long sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = NLib_Fichier_Operation_ObtenirTaille( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

/* Calculer CRC */
NS32 NLib_Fichier_Operation_CalculerCRC( FILE *fichier )
{
	// Sortie
	__OUTPUT NS32 crc = 0;

	// Position initiale
	long positionInitiale;

	// Caract�re lu
	char caractere;

	// Obtenir position initiale
	positionInitiale = ftell( fichier );

	// Calculer
	while( !NLib_Fichier_Operation_EstEOF( fichier ) )
	{
		// Lire
		caractere = (char)fgetc( fichier );

		// Additionner
		crc += caractere;
	}

	// Restaurer position
	fseek( fichier,
		positionInitiale,
		SEEK_SET );

	// OK
	return crc;
}

NS32 NLib_Fichier_Operation_CalculerCRC2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT NS32 sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"rb" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return 0;
	}

	// Lire la taille
	sortie = NLib_Fichier_Operation_CalculerCRC( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

/* Est EOF? */
NBOOL NLib_Fichier_Operation_EstEOF( FILE *f )
{
	// La taille
	long taille;

	// Obtenir la taille
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// V�rifier
	return NLib_Fichier_Operation_EstEOF2( f,
		taille );
}

NBOOL NLib_Fichier_Operation_EstEOF2( FILE *f,
	NU32 taille )
{
	// V�rifier
	return (NU32)ftell( f ) >= taille;
}

/* Le fichier existe? */
NBOOL NLib_Fichier_Operation_EstExiste( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
		return NFALSE;
	// Le fichier existe
	else
	{
		// Fermer le fichier
		fclose( fichier );

		// Quitter
		return NTRUE;
	}
}

/* Placer le curseur */
NBOOL NLib_Fichier_Operation_PlacerCaractere( FILE *f,
	char c )
{
	// Caract�re lu
	char cL;

	// Chercher le caract�re
	do
	{
		// Lire caract�re
		cL = (char)fgetc( f );
	} while( !NLib_Fichier_Operation_EstEOF( f )
		&& c != cL );

	// OK
	return c == cL;
}

NBOOL NLib_Fichier_Operation_PlacerChaine( FILE *f,
	const char *s )
{
	// Curseur
	NU32 curseur = 0;

	// Longueur chaine
	NU32 longueur;

	// Caract�re lu
	char cL;

	// Obtenir la taille de la chaine � rechercher
	longueur = strlen( s );

	// Replacer au d�but du fichier
	fseek( f,
		0,
		SEEK_SET );

	// Chercher la chaine
	while( curseur < longueur
		&& !NLib_Fichier_Operation_EstEOF( f ) )
	{
		// Obtenir caract�re
		cL = (char)fgetc( f );

		// Caract�re trouv�
		if( cL == s[ curseur ] )
			curseur++;
		// Impossible de trouver le caract�re
		else
			curseur = 0;
	}

	// OK?
	return ( curseur >= longueur );
}

/* Lire */
__ALLOC char *NLib_Fichier_Operation_LireEntre( FILE *f,
	char c1,
	char c2 )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Buffer
	char *temp;

	// Curseur pour la copie
	NU32 curseur = 0;

	// Caract�re lu
	char cL;

	// Trouver le 1er caract�re
	if( c1 != '\0' )
		if( !NLib_Fichier_Operation_PlacerCaractere( f,
			c1 ) )
			return NULL;

	// Lire
	do
	{
		// Lire caract�re
		do
		{
			cL = (char)fgetc( f );
		} while( NLib_Caractere_EstUnSeparateur( cL ) // forcer la lecture si le premier caract�re est le caract�re de fin
			&& out == NULL );
		
		// Si le caract�re lu n'est pas le caract�re de fin
		if( ( ( c2 == '\0' ) ? !NLib_Caractere_EstUnSeparateur( cL ) : ( cL != c2 ) ) )
		{
			// Si la m�moire n'est pas encore allou�e
			if( out == NULL )
			{
				// Allouer la m�moire
				if( !( out = calloc( 1 /* + 1 caract�re */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Quitter
					return NULL;
				}
				
				// O� copier?
				curseur = 0;
			}
			else
			{
				// Enregistrer l'adresse
				temp = out;

				// Allouer � la nouvelle taille
				if( !( out = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( temp );

					// Quitter
					return NULL;
				}

				// Copier ancienne donn�e
				memcpy( out,
					temp,
					strlen( temp ) );

				// O� copier?
				curseur = strlen( temp );

				// Lib�rer
				NFREE( temp );
			}
	
			// Copier le nouveau caract�re
			out[ curseur ] = cL;
		}
	} while( !NLib_Fichier_Operation_EstEOF( f )
		&& ( ( c2 == '\0' ) ? !NLib_Caractere_EstUnSeparateur( cL ) : ( cL != c2 ) ) );

	// OK
	return out;
}

__ALLOC char *NLib_Fichier_Operation_LireEntre2( FILE *f,
	char c )
{
	return NLib_Fichier_Operation_LireEntre( f,
		c,
		c );
}

// Lire contenu
__ALLOC char *NLib_Fichier_Operation_LireContenu( FILE *f )
{
	// Sortie
	__OUTPUT char *out;

	// Taille du fichier
	long taille;

	// Position actuelle
	long positionActuelle = 0;

	// Obtenir taille fichier
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// Obtenir position actuelle
	positionActuelle = ftell( f );

	// Aller au d�but du fichier
	fseek( f,
		0,
		SEEK_SET );

	// Allouer la m�moire
	if( !( out = calloc( taille + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	fread( out,
		sizeof( char ),
		taille,
		f );

	// Restaurer la position
	fseek( f,
		positionActuelle,
		SEEK_SET );

	// OK
	return out;
}

NU32 NLib_Fichier_Operation_LireNombreInterne( FILE *f,
	NBOOL estConserverPosition,
	NBOOL estSigne )
{
	// Position initiale
	NU32 positionInitiale;

	// Taille du fichier
	NU32 taille;

	// Caract�re lu
	char caractere;

	// Curseur
	NU32 curseur = 0;

#define TAILLE_BUFFER		5096

	// Buffer
	char buffer[ TAILLE_BUFFER ] = { 0, };

	// Obtenir la taille
	taille = NLib_Fichier_Operation_ObtenirTaille( f );

	// Enregistrer position initiale
	positionInitiale = ftell( f );

	// Chercher chiffre ou '-' si sign�
	do
	{
		// Lire
		caractere = (char)fgetc( f );
	} while( ( !NLib_Caractere_EstUnChiffre( caractere )
			&& ( estSigne ? ( caractere != '-' ) : NTRUE ) )
		&& !NLib_Fichier_Operation_EstEOF2( f,
			taille ) );

	// Aucun chiffre
	if( NLib_Fichier_Operation_EstEOF2( f,
		taille ) )
	{
		// Restaurer si n�cessaire
		if( estConserverPosition )
			fseek( f,
				positionInitiale,
				SEEK_SET );

		// Quitter
		return NFICHIER_CODE_ERREUR;
	}

	// Reculer d'une case
	fseek( f,
		-1,
		SEEK_CUR );

	// Vider le buffer
	memset( buffer,
		0,
		TAILLE_BUFFER );

	// Lire nombre
	do
	{
		// Lire
		caractere = (char)fgetc( f );

		// V�rifier
		if( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ? ( caractere == '-' && !curseur ) : NFALSE ) )
			// Ajouter au buffer
			buffer[ curseur++ ] = caractere;
	} while( ( NLib_Caractere_EstUnChiffre( caractere )
			|| ( estSigne ? ( caractere == '-' && curseur == 1 ) : NFALSE ) )
		&& strlen( buffer ) < TAILLE_BUFFER
		&& !NLib_Fichier_Operation_EstEOF2( f,
			taille ) );

#undef TAILLE_BUFFER

	// Restaurer si n�cessaire
	if( estConserverPosition )
		fseek( f,
			positionInitiale,
			SEEK_SET );

	// OK
	return strtol( buffer,
		NULL,
		10 );
}

NU32 NLib_Fichier_Operation_LireNombreNonSigne( FILE *f,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_Operation_LireNombreInterne( f,
		estConserverPosition,
		NFALSE );
}

NU32 NLib_Fichier_Operation_LireNombreSigne( FILE *f,
	NBOOL estConserverPosition )
{
	return NLib_Fichier_Operation_LireNombreInterne( f,
		estConserverPosition,
		NTRUE );
}

// Ecrire
NBOOL NLib_Fichier_Operation_Ecrire( const char *lien,
	const char *data,
	NU32 taille,
	NBOOL ecraserSiExiste )
{
	// Fichier
	FILE *fichier;

	// V�rifier si le fichier existe au besoin
	if( !ecraserSiExiste )
		if( NLib_Fichier_Operation_EstExiste( lien ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_ALREADY_EXISTS );

			// Quitter
			return NFALSE;
		}

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"wb+" ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Ecrire
	fwrite( data,
		sizeof( char ),
		taille,
		fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return NTRUE;
}

// Obtenir extension
__ALLOC char *NLib_Fichier_Operation_ObtenirExtension( const char *nom )
{
	// Etape
	NU32 etape = 0;

	// Curseur
	NU32 curseur = 0;

	// Tableau temporaire
	char *temp;

	// Caract�re
	char buffer;

	// Sortie
	__OUTPUT char *extension = NULL;

	// Parcourir
	while( curseur < strlen( nom ) )
	{
		// Lire le caract�re
		buffer = nom[ curseur ];

		// Suivant l'�tape
		switch( etape )
		{
			// On est au nom du fichier
			case 0:
				if( buffer == '.' )
				{
					if( curseur == 0 )
						return NULL;
					else
						etape++;
				}
				break;

			// On a trouv� un point
			case 1:
				if( buffer != '.' )
				{
					// Allouer la m�moire
					if( !( extension = calloc( 2,
						sizeof( char ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Quitter
						return NULL;
					}
					
					// Copier le caract�re
					extension[ 0 ] = buffer;

					// Etape suivante
					etape++;
				}
				break;
			
			// On est en train de lire l'extension
			case 2:
				// Enregistrer l'ancien �tat
				temp = extension;

				// Allouer
				if( !( extension = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
					
					// Lib�rer
					NFREE( temp );

					// Quitter
					return NULL;
				}

				// Copier
				memcpy( extension,
					temp,
					strlen( temp ) );

				// Ajouter le caract�re
				extension[ strlen( temp ) ] = buffer;

				// Lib�rer
				NFREE( temp );
				break;

			// Etat inconnu
			default:
				// Lib�rer
				NFREE( extension );
				
				// Quitter
				return NULL;
		}

		// Incr�menter le curseur
		curseur++;
	}

	// OK
	return extension;
}

