#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Fichier::NFichierBinaire
// -------------------------------------

/* Construire (priv�e) */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireInterne( const char *lien,
	const char *mode )
{
	// Sortie
	__OUTPUT NFichierBinaire *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NFichierBinaire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir le fichier
	if( !( out->m_fichier = fopen( lien,
		mode ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// D�terminer si il s'agit de lecture/�criture
	switch( mode[ 0 ] )
	{
		case 'w':
			out->m_estLecture = NFALSE;
			break;

		case 'r':
			out->m_estLecture = NTRUE;
			break;

		default:
			break;
	}

	// D�terminer la taille
	if( out->m_estLecture )
		out->m_taille = NLib_Fichier_Operation_ObtenirTaille( out->m_fichier );

	// OK
	return out;
}

/* Construire */
__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireEcriture( const char *lien,
	NBOOL estEcraserContenu )
{
	// Mode
	char mode[ 48 ];
	
	// Cr�er mode
	strcpy( mode,
		estEcraserContenu ?
			"wb+"
			: "wb" );

	// Construire
	return NLib_Fichier_NFichierBinaire_ConstruireInterne( lien,
		mode );
}

__ALLOC NFichierBinaire *NLib_Fichier_NFichierBinaire_ConstruireLecture( const char *lien )
{
	return NLib_Fichier_NFichierBinaire_ConstruireInterne( lien,
		"rb" );
}

/* D�truire */
void NLib_Fichier_NFichierBinaire_Detruire( NFichierBinaire **this )
{
	// Fermer le fichier
	fclose( (*this)->m_fichier );

	// Lib�rer
	NFREE( *this );
}

/* Est EOF? */
NBOOL NLib_Fichier_NFichierBinaire_EstEOF( const NFichierBinaire *this )
{
	return NLib_Fichier_Operation_EstEOF2( this->m_fichier,
		this->m_taille );
}

/* Obtenir taille */
NU32 NLib_Fichier_NFichierBinaire_ObtenirTaille( const NFichierBinaire *this )
{
	return this->m_estLecture ?
		this->m_taille
		: NFICHIER_CODE_ERREUR;
}

/* Est lecture? */
NBOOL NLib_Fichier_NFichierBinaire_EstLecture( const NFichierBinaire *this )
{
	return this->m_estLecture;
}

/* Est �criture? */
NBOOL NLib_Fichier_NFichierBinaire_EstEcriture( const NFichierBinaire *this )
{
	return !this->m_estLecture;
}

/* Lire */
__ALLOC char *NLib_Fichier_NFichierBinaire_Lire( NFichierBinaire *this,
	NU32 taille )
{
	// Sortie
	__OUTPUT char *sortie;

	// Allouer la m�moire
	if( !( sortie = calloc( taille,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	if( !NLib_Fichier_NFichierBinaire_Lire2( this,
		sortie,
		taille ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

		// Lib�rer
		NFREE( sortie );

		// Quitter
		return NULL;
	}

	// OK
	return sortie;
}

NBOOL NLib_Fichier_NFichierBinaire_Lire2( NFichierBinaire *this,
	__OUTPUT char *sortie,
	NU32 taille )
{
	// Lire
	return fread( sortie,
		sizeof( char ),
		taille,
		this->m_fichier ) == taille;
}

/* Ecrire */
NBOOL NLib_Fichier_NFichierBinaire_Ecrire( NFichierBinaire *this,
	const char *buffer,
	NU32 tailleBuffer )
{
	return fwrite( buffer,
		sizeof( char ),
		tailleBuffer,
		this->m_fichier ) == tailleBuffer;
}

