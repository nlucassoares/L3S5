#include "../../../../include/NLib/NLib.h"

// ----------------------------------------
// struct NLib::Fichier::Clef::NFichierClef
// ----------------------------------------

/* Construire */
__ALLOC NFichierClef *NLib_Fichier_Clef_NFichierClef_Construire( const char *lien,
	const char **clefs,
	NU32 nombreClefs )
{
	// Fichier texte
	NFichierTexte *fichier;

	// It�rateur
	NU32 i, j;

	// Sortie
	__OUTPUT NFichierClef *out;

	// Construire le fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NFichierClef ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Allouer valeurs
	if( !( out->m_valeurs = calloc( nombreClefs,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Fermer le fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Enregistrer le nombre de clefs
	out->m_nombreClefs = nombreClefs;

	// Charger
	for( i = 0; i < nombreClefs; i++ )
	{
		// Positionner � la clef
		if( !NLib_Fichier_NFichierTexte_PositionnerProchaineChaine( fichier,
			clefs[ i ] ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NFREE( out->m_valeurs[ j ] );
			NFREE( out );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NULL;
		}

		// Lire
		if( !( out->m_valeurs[ i ] = NLib_Fichier_NFichierTexte_Lire( fichier,
			'\0',
			'\n',
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NFREE( out->m_valeurs[ j ] );
			NFREE( out );

			// Fermer le fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NULL;
		}
	}

	// Fermer le fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );
	
	// OK
	return out;
}

/* D�truire */
void NLib_Fichier_Clef_NFichierClef_Detruire( NFichierClef **this )
{
	// It�rateur
	NU32 i = 0;

	// Lib�rer valeurs
	for( ; i < (*this)->m_nombreClefs; i++ )
		NFREE( (*this)->m_valeurs[ i ] );
	NFREE( (*this)->m_valeurs );

	// Lib�rer
	NFREE( (*this) );
}


/* Obtenir valeur */
const char *NLib_Fichier_Clef_NFichierClef_ObtenirValeur( const NFichierClef *this,
	NU32 clef )
{
	// V�rifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	return this->m_valeurs[ clef ];
}

NU32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( const NFichierClef *this,
	NU32 clef )
{
	// V�rifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return 0;
	}

	return (NU32)strtol( this->m_valeurs[ clef ],
		NULL,
		10 );
}

NS32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur3( const NFichierClef *this,
	NU32 clef )
{
	// V�rifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return 0;
	}

	return strtol( this->m_valeurs[ clef ],
		NULL,
		10 );
}

/* Obtenir copie valeur */
__ALLOC char *NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( const NFichierClef *this,
	NU32 clef )
{
	// Sortie
	__OUTPUT char *out;

	// V�rifier
	if( clef >= this->m_nombreClefs )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( strlen( this->m_valeurs[ clef ] ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		this->m_valeurs[ clef ],
		strlen( this->m_valeurs[ clef ] ) );

	// OK
	return out;
}

