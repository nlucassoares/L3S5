#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------
// namespace NLib::Caractere
// -------------------------

// Est un chiffre?
NBOOL NLib_Caractere_EstUnChiffre( char c )
{
	return ( c >= '0'
		&& c <= '9' );
}

// Est un chiffre hexadecimal?
NBOOL NLib_Caractere_EstUnChiffreHexadecimal( char c )
{
	return ( ( c >= 'A'
				&& c <= 'F' )
			|| ( c >= 'a'
				&& c <= 'f' ) );
}

// Est une lettre?
NBOOL NLib_Caractere_EstUneLettre( char c )
{
	return ( ( c >= 'A'
				&& c <= 'Z' )
			|| ( c >= 'a'
					&& c <= 'z' ) );
}

/* Est un s�parateur? */
NBOOL NLib_Caractere_EstUnSeparateur( char c )
{
	switch( c )
	{
		case ' ':
		case '\n':
		case '\r':
		case '\t':
			return NTRUE;

		default:
			return NFALSE;
	}
}

// Convertir un chiffre en d�cimal
NU32 NLib_Caractere_ConvertirDecimal( char c )
{
	if( NLib_Caractere_EstUnChiffre( c ) )
		return c - '0';
	else
		if( NLib_Caractere_EstUnChiffreHexadecimal( c ) )
			switch( c )
			{
				case 'a':
				case 'A':
					return 0x0A;
				case 'b':
				case 'B':
					return 0x0B;
				case 'c':
				case 'C':
					return 0x0C;
				case 'd':
				case 'D':
					return 0x0D;
				case 'e':
				case 'E':
					return 0x0E;
				case 'f':
				case 'F':
					return 0x0F;

				default:
					return 0;
			}

	// Pas un chiffre
	return NERREUR;
}

// Convertir un nombre en caract�re entre 0 et F
char NLib_Caractere_ConvertirHexadecimal( NU8 nb )
{
	// V�rifier
	if( nb > 15 )
		return 'F';

	// Convertir
	if( nb < 10 )
		return '0' + (char)nb;
	else
		return 'A' + ( (char)nb - 10 );
}

// Est un caract�re acceptable dans HTML
NBOOL NLib_Caractere_EstUnCaractereViableHTMLHref( char c )
{
	// Caract�res tol�r�s
	if( NLib_Caractere_EstUnChiffre( c )
		|| NLib_Caractere_EstUneLettre( c ) )
		return NTRUE;
	else
		switch( c )
		{
			case '/':
			case '\\':
			case '.':
			case '_':
			case '-':
				return NTRUE;

			default:
				return NFALSE;
		}
}

