#include "../../../../include/NLib/NLib.h"

// ---------------------------------------------
// struct NLib::Temps::Animation::NEtatAnimation
// ---------------------------------------------

/* Construire */
__ALLOC NEtatAnimation *NLib_Temps_Animation_NEtatAnimation_Construire( NU32 delaiFrame,
	NU32 nombreFrame,
	NU32 nombreEtapeAnimation,
	const NU32 *ordreAnimation )
{
	// Sortie
	__OUTPUT NEtatAnimation *out;

	// It�rateur
	NU32 i;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NEtatAnimation ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_delaiFrame = delaiFrame;

	// D�terminer nombre d'�tapes animation
	if( ordreAnimation != NULL
		&& nombreEtapeAnimation > 0 )
		// Enregistrer
		out->m_nombreEtapeAnimation = nombreEtapeAnimation;
	else
		// Enregistrer
		out->m_nombreEtapeAnimation = nombreFrame;

	// Allouer la m�moire
	if( !( out->m_ordreAnimation = calloc( out->m_nombreEtapeAnimation,
		sizeof( NU32 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Cr�er ordre
	if( ordreAnimation != NULL
		&& nombreEtapeAnimation > 0 )
		// Ordre utilisateur
		memcpy( out->m_ordreAnimation,
			ordreAnimation,
			sizeof( NU32 ) * out->m_nombreEtapeAnimation );
	else
		// Ordre par d�faut
		for( i = 0; i < out->m_nombreEtapeAnimation; i++ )
			out->m_ordreAnimation[ i ] = i;

	// OK
	return out;
}

__ALLOC NEtatAnimation *NLib_Temps_Animation_NEtatAnimation_Construire2( const NEtatAnimation *src )
{
	// Construire
	return NLib_Temps_Animation_NEtatAnimation_Construire( src->m_delaiFrame,
		src->m_nombreFrame,
		src->m_nombreEtapeAnimation,
		src->m_ordreAnimation );
}

/* D�truire */
void NLib_Temps_Animation_NEtatAnimation_Detruire( NEtatAnimation **this )
{
	// Lib�rer
	NFREE( (*this)->m_ordreAnimation );
	NFREE( (*this) );
}

/* Update */
void NLib_Temps_Animation_NEtatAnimation_Update( NEtatAnimation *this )
{
	// V�rifier
	if( NLib_Temps_ObtenirTick( ) - this->m_dernierUpdate < this->m_delaiFrame )
		return;

	// Enregistrer le temps de l'update
	this->m_dernierUpdate = NLib_Temps_ObtenirTick( );

	// Avancer l'�tape
	if( this->m_etapeAnimation < this->m_nombreEtapeAnimation - 1 )
		this->m_etapeAnimation++;
	else
		this->m_etapeAnimation = 0;

	// Associer la frame
	this->m_frame = this->m_ordreAnimation[ this->m_etapeAnimation ];
}

/* Obtenir frame */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( const NEtatAnimation *this )
{
	return this->m_frame;
}

/* Obtenir nombre frames */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirNombreFrame( const NEtatAnimation *this )
{
	return this->m_nombreFrame;
}

/* Obtenir d�lai entre frames */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirDelaiFrame( const NEtatAnimation *this )
{
	return this->m_delaiFrame;
}

/* Obtenir nombre �tapes animation */
NU32 NLib_Temps_Animation_NEtatAnimation_ObtenirNombreEtapeAnimation( const NEtatAnimation *this )
{
	return this->m_nombreEtapeAnimation;
}

/* Obtenir ordre animation */
const NU32 *NLib_Temps_Animation_NEtatAnimation_ObtenirOrdreAnimation( const NEtatAnimation *this )
{
	return this->m_ordreAnimation;
}

/* Est � la fin de l'animation? */
NBOOL NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( const NEtatAnimation *this )
{
	return NLib_Temps_ObtenirTick( ) - this->m_dernierUpdate >= this->m_delaiFrame
		&& (  this->m_etapeAnimation >= this->m_nombreEtapeAnimation - 1 );
}

/* Remettre � z�ro */
void NLib_Temps_Animation_NEtatAnimation_RemettreAZero( NEtatAnimation *this )
{
	// Retour au d�but
	this->m_etapeAnimation = 0;
	this->m_dernierUpdate = NLib_Temps_ObtenirTick( );
	this->m_frame = this->m_ordreAnimation[ this->m_etapeAnimation ];
}

