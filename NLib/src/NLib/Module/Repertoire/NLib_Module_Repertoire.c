#define NLIB_MODULE_REPERTOIRE_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::Repertoire
// ----------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

/* Initialiser */
NBOOL NLib_Module_Repertoire_Initialiser( void )
{
	// Allouer la m�moire
	if( !( m_repertoireInitial = NLib_Module_Repertoire_ObtenirCourant( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_CURRENT_DIRECTORY_FAILED );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* D�truire */
void NLib_Module_Repertoire_Detruire( void )
{
	// Lib�rer
	NFREE( m_repertoireInitial );
}

/* Changer r�pertoire */
NBOOL NLib_Module_Repertoire_Changer( const char *repertoire )
{
	return ( CHDIR( repertoire ) != NLIB_ERREUR_REPERTOIRE );
}

/* Restaurer r�pertoire initial */
NBOOL NLib_Module_Repertoire_RestaurerInitial( void )
{
	return NLib_Module_Repertoire_Changer( m_repertoireInitial );
}

/* Obtenir r�pertoire courant */
__ALLOC char *NLib_Module_Repertoire_ObtenirCourant( void )
{
	return GETCWD( NULL,
		0 );
}

/* Obtenir r�pertoire initial */
const char *NLib_Module_Repertoire_ObtenirInitial( void )
{
	return m_repertoireInitial;
}

/* Obtenir chemin absolu �l�ment */
__ALLOC char *NLib_Module_Repertoire_ObtenirCheminAbsolu( const char *e )
{
#ifdef IS_WINDOWS
	return _fullpath( NULL,
		e,
		0 );
#else // IS_WINDOWS
	// Sortie
	char *out;
	
	// Allouer la m�moire
	if( !( out = calloc( PATH_MAX + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// R�cup�rer chemin
	if( !realpath( "./",
		out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}
	
	// OK
	return out;
#endif // !IS_WINDOWS
}

/* Compter �l�ments r�pertoire */
NU32 NLib_Module_Repertoire_ObtenirNombreElements( const char *filtre,
	NAttributRepertoire attribut )
{
	// Element
	FIND_DATA element;

	// Handle �l�ment
	NS32 handle;

	// Sortie
	__OUTPUT NU32 out = 0;

	// R�cup�rer le premier fichier
	if( ( handle = FINDFIRST( filtre,
		&element ) ) == NLIB_ERREUR_RECHERCHE_REPERTOIRE )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_ITEM_REPERTORY_FAILED );

		// Quitter
		return 0;
	}

	// Compter
	if( attribut != NATTRIBUT_REPERTOIRE_NORMAL )
		do
		{
			if( element.attrib & attribut )
				out++;
		} while( !FINDNEXT( handle,
			&element ) );
	else
		do
		{
			out++;
		} while( !FINDNEXT( handle,
			&element ) );

	// Fermer recherche
	FINDCLOSE( handle );

	// OK
	return out;
}

#endif // NLIB_MODULE_REPERTOIRE

