#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// NLib::Module::Repertoire::NRepertoire
// -------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

/* Vider (private) */
NBOOL NLib_Module_Repertoire_NRepertoire_ViderInterne( NRepertoire *this )
{
	// It�rateur
	NU32 i = 0;

	// V�rifier �tat
	if( !this->m_estListe )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Lib�rer
		// 2D
			for( ; i < this->m_nbElements; i++ )
				NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ i ] );
		// 1D
			NFREE( this->m_elements );

	// Remettre � z�ro
	this->m_nbElements = 0;

	// OK
	return !( this->m_estListe = NFALSE );
}

/* Construire l'objet */
__ALLOC NRepertoire *NLib_Module_Repertoire_NRepertoire_Construire( void )
{
	// Sortie
	__OUTPUT NRepertoire *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NRepertoire ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Z�ro
	memset( out,
		0,
		sizeof( NRepertoire ) );

	// OK
	return out;
}

/* D�truire l'objet */
void NLib_Module_Repertoire_NRepertoire_Detruire( NRepertoire **this )
{
	// It�rateur
	NU32 i = 0;

	// Lib�rer �l�ments
	for( ; i < (*this)->m_nbElements; i++ )
		NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &(*this)->m_elements[ i ] );

	// D�truire
	NFREE( (*this)->m_elements );

	// Lib�rer
	NFREE( *this );
}

/* Lister les fichiers */
NBOOL NLib_Module_Repertoire_NRepertoire_Lister( NRepertoire *this,
	const char *filtre,
	NAttributRepertoire attributs )
{
	// Fichier
		// Donn�es
			FIND_DATA donneesFichier;
		// Handle
			NS32 handleFichier;

	// It�rateur
	NU32 i = 0,
		j;

	// V�rifier
		// Param�tre
			if( !filtre )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Lib�rer les anciens fichiers
	NLib_Module_Repertoire_NRepertoire_ViderInterne( this );

	// Obtenir le nombre de fichiers
	if( !( this->m_nbElements = NLib_Module_Repertoire_ObtenirNombreElements( filtre,
		attributs ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_NO_FILE_REPERTORY );

		// Quitter
		return NFALSE;
	}

	// Allouer la m�moire
		// 1D
			if( !( this->m_elements = calloc( this->m_nbElements,
				sizeof( NElementRepertoire* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}

	// Initialiser listage
	if( ( handleFichier = FINDFIRST( filtre,
		&donneesFichier ) ) == NLIB_ERREUR_RECHERCHE_REPERTOIRE )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_GET_ITEM_REPERTORY_FAILED );

		// Lib�rer
		NFREE( this->m_elements );

		// Quitter
		return NFALSE;
	}

	// Lister les fichiers
	if( attributs != NATTRIBUT_REPERTOIRE_NORMAL )
	{
		do
		{
			// V�rifier attributs
			if( donneesFichier.attrib & attributs )
			{
				// Construire �l�ment
				if( !( this->m_elements[ i ] = NLib_Module_Repertoire_Element_NElementRepertoire_Construire( donneesFichier.name,
					donneesFichier.size,
					(NAttributRepertoire)donneesFichier.attrib,
					donneesFichier.time_create,
					donneesFichier.time_write,
					donneesFichier.time_access ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Lib�rer
						// 2D
							for( j = 0; j < i; j++ )
								NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ j ] );
						// 1D
							NFREE( this->m_elements );

					// Quitter
					return NFALSE;
				}

				// Fichier suivant
				i++;
			}
		} while( !FINDNEXT( handleFichier,
			&donneesFichier ) );
	}
	else
		do
		{
			// Construire �l�ment
			if( !( this->m_elements[ i ] = NLib_Module_Repertoire_Element_NElementRepertoire_Construire( donneesFichier.name,
				donneesFichier.size,
				(NAttributRepertoire)donneesFichier.attrib,
				donneesFichier.time_create,
				donneesFichier.time_write,
				donneesFichier.time_access ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Lib�rer
					// 2D
						for( j = 0; j < i; j++ )
							NLib_Module_Repertoire_Element_NElementRepertoire_Detruire( &this->m_elements[ j ] );
					// 1D
						NFREE( this->m_elements );

				// Quitter
				return NFALSE;
			}

			// Fichier suivant
			i++;
		} while( !FINDNEXT( handleFichier,
			&donneesFichier ) );

	// V�rifier
	if( this->m_nbElements != i )
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_GET_ITEM_REPERTORY_FAILED );

	// Fermer listage
	FINDCLOSE( handleFichier );

	// OK
	return this->m_estListe = NTRUE;
}

/* Obtenir nombre fichiers */
NU32 NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( const NRepertoire *this )
{
	return this->m_nbElements;
}

/* Obtenir fichier */
const NElementRepertoire *NLib_Module_Repertoire_NRepertoire_ObtenirFichier( const NRepertoire *this,
	NU32 index )
{
	// V�rifier
		// Etat
			if( !this->m_estListe )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

				// Quitter
				return NULL;
			}
		// Param�tre
			if( index >= this->m_nbElements )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NULL;
			}

	// OK
	return this->m_elements[ index ];
}

/* Est fichier existe? */
NBOOL NLib_Module_Repertoire_NRepertoire_EstFichierExiste( const NRepertoire *this,
	const char *nom )
{
	// It�rateur
	NU32 i = 0;

	// Chercher
	for( ; i < this->m_nbElements; i++ )
		if( !_strcmpi( NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom( this->m_elements[ i ] ),
				nom ) )
			// Trouv�!
			return NTRUE;

	// Introuvable
	return NFALSE;
}

#endif // NLIB_MODULE_REPERTOIRE

