#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------
// struct NLib::Module::SDL::Surface::NSurface
// -------------------------------------------

#ifdef NLIB_MODULE_SDL
/* Update la texture (interne) */
NBOOL NLib_Module_SDL_Surface_NSurface_UpdateTextureInterne( NSurface *this )
{
	// Nettoyer
	NLIB_NETTOYER_TEXTURE( this->m_texture );

	// Update
	if( this->m_estTextureUpdate = ( ( this->m_texture = SDL_CreateTextureFromSurface( NLib_Module_SDL_NFenetre_ObtenirRenderer( this->m_handleFenetre ),
		this->m_surface ) ) != NULL ) )
		SDL_SetTextureBlendMode( this->m_texture,
			SDL_BLENDMODE_BLEND );

	// OK?
	return this->m_estTextureUpdate;
}

/* Construire la surface */
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire( const NFenetre *fenetre,
	NUPoint taille )
{
	// Sortie
	__OUTPUT NSurface *out;

	// Construire
	if( !( out = calloc( 1,
		sizeof( NSurface ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_handleFenetre = fenetre;
	out->m_taille = taille;

	// Cr�er la surface
	if( !( out->m_surface = NLib_Module_SDL_Surface_Creer( taille.x,
		taille.y,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NULL;
	}

	// Z�ro
	out->m_zoom = 1;
	NDEFINIR_POSITION( out->m_position,
		0,
		0 );

	// OK
	return out;
}

#ifdef NLIB_MODULE_SDL_IMAGE
__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire2( const NFenetre *fenetre,
	const char *lien )
{
	// Sortie
	__OUTPUT NSurface *out;

	// Construire
	if( !( out = calloc( 1,
		sizeof( NSurface ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger image
	if( !( out->m_surface = NLib_Module_SDL_Image_Charger( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL_IMAGE );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_handleFenetre = fenetre;
	NDEFINIR_POSITION( out->m_taille,
		(NU32)out->m_surface->w,
		(NU32)out->m_surface->h );

	// Update texture
	NLib_Module_SDL_Surface_NSurface_UpdateTextureInterne( out );

	// Z�ro
	out->m_zoom = 1;
	NDEFINIR_POSITION( out->m_position,
		0,
		0 );

	// OK
	return out;
}
#endif // NLIB_MODULE_SDL_IMAGE

__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire3( const NFenetre *fenetre,
	const SDL_Surface *surface )
{
	// Rect
	SDL_Rect r;

	// Copier
	NDEFINIR_POSITION( r,
		0,
		0 );
	NDEFINIR_TAILLE( r,
		surface->w,
		surface->h );

	// Construire
	return NLib_Module_SDL_Surface_NSurface_Construire4( fenetre,
		surface,
		r );
}

__ALLOC NSurface *NLib_Module_SDL_Surface_NSurface_Construire4( const NFenetre *fenetre,
	const SDL_Surface *surface,
	SDL_Rect rect )
{
	// Sortie
	__OUTPUT NSurface *out;

	// Construire
	if( !( out = calloc( 1,
		sizeof( NSurface ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_handleFenetre = fenetre;

	// Copier la surface
	if( !( out->m_surface = NLib_Module_SDL_Surface_Copier2( surface,
		&rect ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Update texture
	if( !NLib_Module_SDL_Surface_NSurface_UpdateTextureInterne( out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Lib�rer
		NLIB_NETTOYER_SURFACE( out->m_surface );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	NDEFINIR_POSITION( out->m_taille,
		(NU32)out->m_surface->w,
		(NU32)out->m_surface->h );

	// Z�ro
	out->m_zoom = 1;
	NDEFINIR_POSITION( out->m_position,
		0,
		0 );

	// OK
	return out;
}

/* D�truire la surface */
void NLib_Module_SDL_Surface_NSurface_Detruire( NSurface **this )
{
	// V�rifier
	if( !(*this) )
		return;

	// D�truire la texture
	NLIB_NETTOYER_TEXTURE( (*this)->m_texture );

	// D�truire la surface
	NLIB_NETTOYER_SURFACE( (*this)->m_surface );

	// Lib�rer
	NFREE( *this );
}

/* Demander update texte */
void NLib_Module_SDL_Surface_NSurface_DemanderUpdateTexture( NSurface *this )
{
	this->m_estTextureUpdate = NFALSE;
}

/* D�finir position */
void NLib_Module_SDL_Surface_NSurface_DefinirPosition( NSurface *this,
	NS32 x,
	NS32 y )
{
	// D�finir la position
	NDEFINIR_POSITION( this->m_position,
		x,
		y );
}

void NLib_Module_SDL_Surface_NSurface_DefinirPosition2( NSurface *this,
	NSPoint position )
{
	NLib_Module_SDL_Surface_NSurface_DefinirPosition( this,
		position.x,
		position.y );
}

/* D�finir zoom */
void NLib_Module_SDL_Surface_NSurface_DefinirZoom( NSurface *this,
	NS32 zoom )
{
	// V�rifier
	if( !zoom )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quitter
		return;
	}

	// Changer le facteur de zoom
	this->m_zoom = zoom;
}

/* D�finir modification couleur */
void NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( NSurface *this,
	NU8 r,
	NU8 g,
	NU8 b )
{
	// V�rifier
	if( !this->m_texture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return;
	}

	// D�finir
	SDL_SetTextureColorMod( this->m_texture,
		r,
		g,
		b );
}

/* D�finir modification alpha */
void NLib_Module_SDL_Surface_NSurface_DefinirModificationAlpha( NSurface *this,
	NU8 a )
{
	// V�rifier
	if( !this->m_texture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return;
	}

	// D�finir
	SDL_SetTextureAlphaMod( this->m_texture,
		a );
}

/* Afficher la texture */
void NLib_Module_SDL_Surface_NSurface_Afficher( const NSurface *this )
{
	// Rect SDL
	SDL_Rect r;

	// Initialiser rect
	NDEFINIR_POSITION( r,
		this->m_position.x,
		this->m_position.y );
	if( this->m_zoom >= 0 )
	{
		NDEFINIR_TAILLE( r,
			this->m_taille.x * this->m_zoom,
			this->m_taille.y * this->m_zoom );
	}
	else
	{
		NDEFINIR_TAILLE( r,
			(NS32)( (double)this->m_taille.x * ( 1.0 / (double)( abs( this->m_zoom ) ) ) ),
			(NS32)( (double)this->m_taille.y * ( 1.0 / (double)( abs( this->m_zoom ) ) ) ) );
	}

	// V�rifier si la texture est � jour
	if( !this->m_estTextureUpdate )
		if( !NLib_Module_SDL_Surface_NSurface_UpdateTextureInterne( (NSurface*)this ) )
			return;

	// Afficher
	SDL_RenderCopy( NLib_Module_SDL_NFenetre_ObtenirRenderer( this->m_handleFenetre ),
		this->m_texture,
		NULL,
		&r );
}

/* Obtenir la taille */
const NUPoint *NLib_Module_SDL_Surface_NSurface_ObtenirTaille( const NSurface *this )
{
	return &this->m_taille;
}

/* Obtenir le facteur de zoom */
const NS32 NLib_Module_SDL_Surface_NSurface_ObtenirZoom( const NSurface *this )
{
	return this->m_zoom;
}

/* Obtenir la position */
const NSPoint *NLib_Module_SDL_Surface_NSurface_ObtenirPosition( const NSurface *this )
{
	return &this->m_position;
}

/* Obtenir la surface */
SDL_Surface *NLib_Module_SDL_Surface_NSurface_ObtenirSurface( const NSurface *this )
{
	return this->m_surface;
}

#endif // NLIB_MODULE_SDL

