#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

#ifdef NLIB_MODULE_SDL
// --------------------------------
// struct NLib::Module::SDL::NCadre
// --------------------------------

/* Calculer coordonn�es (priv�e) */
void NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( NCadre *this )
{
	// D�finir rectangle bordures
		// Haut
			NDEFINIR_POSITION( this->m_coordonnee[ NHAUT ],
				this->m_position.x,
				this->m_position.y );

			NDEFINIR_TAILLE( this->m_coordonnee[ NHAUT ],
				this->m_taille.x,
				this->m_epaisseur );
		// Bas
			NDEFINIR_POSITION( this->m_coordonnee[ NBAS ],
				this->m_position.x,
				this->m_position.y + this->m_taille.y );

			NDEFINIR_TAILLE( this->m_coordonnee[ NBAS ],
				this->m_taille.x + this->m_epaisseur, // Correction
				this->m_epaisseur );
		// Gauche
			NDEFINIR_POSITION( this->m_coordonnee[ NGAUCHE ],
				this->m_position.x,
				this->m_position.y );

			NDEFINIR_TAILLE( this->m_coordonnee[ NGAUCHE ],
				this->m_epaisseur,
				this->m_taille.y );
		// Droite
			NDEFINIR_POSITION( this->m_coordonnee[ NDROITE ],
				this->m_position.x + this->m_taille.x,
				this->m_position.y );

			NDEFINIR_TAILLE( this->m_coordonnee[ NDROITE ],
				this->m_epaisseur,
				this->m_taille.y );
		// Centre
			NDEFINIR_POSITION( this->m_coordonnee[ NDIRECTIONS ],
				this->m_position.x + this->m_epaisseur,
				this->m_position.y + this->m_epaisseur );

			NDEFINIR_TAILLE( this->m_coordonnee[ NDIRECTIONS ],
				this->m_taille.x - this->m_epaisseur,
				this->m_taille.y - this->m_epaisseur );
}

/* Construire l'objet */
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire( NSPoint position,
	NUPoint taille,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre *fenetre,
	NU32 epaisseur )
{
	// Sortie
	__OUTPUT NCadre *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NCadre ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
		// Taille
			NDEFINIR_POSITION( out->m_taille,
				taille.x,
				taille.y );
		// Position
			NDEFINIR_POSITION( out->m_position,
				position.x,
				position.y );
		// Epaisseur
			out->m_epaisseur = epaisseur;
		// Fen�tre
			out->m_fenetre = fenetre;
		// Couleur
			NDEFINIR_COULEUR_A( out->m_couleur,
				couleur.r,
				couleur.g,
				couleur.b,
				couleur.a );
		// Couleur fond
			NDEFINIR_COULEUR_A( out->m_couleurFond,
				couleurFond.r,
				couleurFond.g,
				couleurFond.b,
				couleurFond.a );

	// Calculer les coordonn�es
	NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( out );

	// OK
	return out;
}

__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire2( const NCadre *src )
{
	// Construire
	return NLib_Module_SDL_NCadre_Construire( src->m_position,
		src->m_taille,
		src->m_couleur,
		src->m_couleurFond,
		src->m_fenetre,
		src->m_epaisseur );
}

__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire3( NS32 x,
	NS32 y,
	NU32 w,
	NU32 h,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre *fenetre,
	NU32 epaisseur )
{
	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// D�finir
	NDEFINIR_POSITION( position,
		x,
		y );
	NDEFINIR_POSITION( taille,
		w,
		h );

	// Construire
	return NLib_Module_SDL_NCadre_Construire( position,
		taille,
		couleur,
		couleurFond,
		fenetre,
		epaisseur );
}

/* D�truire l'objet */
void NLib_Module_SDL_NCadre_Detruire( NCadre **this )
{
	NFREE( *this );
}

/* Obtenir position */
const NSPoint *NLib_Module_SDL_NCadre_ObtenirPosition( const NCadre *this )
{
	return &this->m_position;
}

/* Obtenir taille */
const NUPoint *NLib_Module_SDL_NCadre_ObtenirTaille( const NCadre *this )
{
	return &this->m_taille;
}

/* Obtenir couleur */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleur( const NCadre *this )
{
	return &this->m_couleur;
}

/* Obtenir couleur fond */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleurFond( const NCadre *this )
{
	return &this->m_couleurFond;
}

/* Obtenir �paisseur */
NU32 NLib_Module_SDL_NCadre_ObtenirEpaisseur( const NCadre *this )
{
	return this->m_epaisseur;
}

/* D�finir position */
void NLib_Module_SDL_NCadre_DefinirPosition( NCadre *this,
	NS32 x,
	NS32 y )
{
	// D�finir
	NDEFINIR_POSITION( this->m_position,
		x,
		y );

	// Recalculer
	NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( this );
}

/* D�finir taille */
void NLib_Module_SDL_NCadre_DefinirTaille( NCadre *this,
	NU32 x,
	NU32 y )
{
	// D�finir
	NDEFINIR_POSITION( this->m_taille,
		x,
		y );

	// Recalculer
	NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( this );
}

/* D�finir couleur */
void NLib_Module_SDL_NCadre_DefinirCouleur( NCadre *this,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a )
{
	// D�finir
	NDEFINIR_COULEUR_A( this->m_couleur,
		r,
		g,
		b,
		a );
}

/* D�finir couleur fond */
void NLib_Module_SDL_NCadre_DefinirCouleurFond( NCadre *this,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a )
{
	// D�finir
	NDEFINIR_COULEUR_A( this->m_couleurFond,
		r,
		g,
		b,
		a );
}

/* D�finir �paisseur */
void NLib_Module_SDL_NCadre_DefinirEpaisseur( NCadre *this,
	NU32 epaisseur )
{
	// D�finir
	this->m_epaisseur = epaisseur;

	// Recalculer
	NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( this );
}

/* Dessiner */
void NLib_Module_SDL_NCadre_Dessiner( const NCadre *this )
{
	// Dessiner
	NLib_Module_SDL_NFenetre_DessinerRectangle2( (NFenetre*)this->m_fenetre,
		this->m_coordonnee,
		&this->m_couleur,
		&this->m_couleurFond );
}

/* Est en colision? */
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec( const NCadre *this,
	const NSRect *r2 )
{
	return NLib_Math_Geometrie_EstColision( this->m_position,
		this->m_taille,
		this->m_epaisseur,
		r2->m_position,
		r2->m_taille,
		1 );
}

NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec2( const NCadre *this,
	const NCadre *c2 )
{
	return NLib_Math_Geometrie_EstColision( this->m_position,
		this->m_taille,
		this->m_epaisseur,
		c2->m_position,
		c2->m_taille,
		c2->m_epaisseur );
}

NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec3( const NCadre *this,
	NSPoint point )
{
	// Taille
	NUPoint taille;

	// D�finir la taille
	NDEFINIR_POSITION( taille,
		this->m_taille.x + this->m_epaisseur,
		this->m_taille.y + this->m_epaisseur );

	// Evaluer
	return NLib_Math_Geometrie_EstColision2( this->m_position,
		taille,
		point );
}

/* Calculer intersection */
__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection( const NCadre *this,
	const NCadre *c2 )
{
	// Intersection
	NSRect intersection;
	
	// Taille des cadres avec �paisseur
	NUPoint tailleC1,
		tailleC2;

	// D�finir les tailles
	NDEFINIR_POSITION( tailleC1,
		this->m_taille.x + this->m_epaisseur,
		this->m_taille.y + this->m_epaisseur );
	NDEFINIR_POSITION( tailleC2,
		c2->m_taille.x + c2->m_epaisseur,
		c2->m_taille.y + c2->m_epaisseur );

	// Calculer l'intersection
	intersection = NLib_Math_Geometrie_CalculerIntersection( this->m_position,
		tailleC1,
		c2->m_position,
		tailleC2 );

	// V�rifier intersection
	if( NLib_Math_Geometrie_EstIntersectionNulle( intersection ) )
		return NULL;

	// Construire
	return NLib_Module_SDL_NCadre_Construire( intersection.m_position,
		intersection.m_taille,
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
		(NCouleur){ 0, 0, 0, 0 },
		this->m_fenetre,
		1 );
}

__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection2( const NCadre *this,
	const NSRect *r2 )
{
	// Rectangle
	NSRect intersection;

	// Taille cadre
	NUPoint tailleCadre;

	// D�finir taille cadre
	NDEFINIR_POSITION( tailleCadre,
		this->m_taille.x + this->m_epaisseur,
		this->m_taille.y + this->m_epaisseur );

	// Calculer intersection
	intersection = NLib_Math_Geometrie_CalculerIntersection( this->m_position,
		tailleCadre,
		r2->m_position,
		r2->m_taille );

	// V�rifier intersection
	if( NLib_Math_Geometrie_EstIntersectionNulle( intersection ) )
		return NULL;

	// Construire
	return NLib_Module_SDL_NCadre_Construire( intersection.m_position,
		intersection.m_taille,
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
		(NCouleur){ 0, 0, 0, 0 },
		this->m_fenetre,
		1 );
}

/* D�placer */
void NLib_Module_SDL_NCadre_Deplacer( NCadre *this,
	NSPoint positionSouris,
	NBOOL continuerDeplacement )
{
	// Si d�placement d�j� en cours
	if( this->m_estDeplacementActif )
	{
		// Si on demande � arr�ter le d�placement
		if( !continuerDeplacement )
			// Stopper
			this->m_estDeplacementActif = NFALSE;
		else
		{
			// Mettre � jour la position
			NDEFINIR_POSITION( this->m_position,
				positionSouris.x - this->m_decalageDeplacement.x,
				positionSouris.y - this->m_decalageDeplacement.y );

			// Recalculer
			NLib_Module_SDL_NCadre_CalculerCoordonneesInterne( this );
		}
	}
	// D�placement d�bute maintenant
	else
	{
		// Enregistrer d�calage
		NDEFINIR_POSITION( this->m_decalageDeplacement,
			positionSouris.x - this->m_position.x,
			positionSouris.y - this->m_position.y );

		// Le d�placement a d�but�
		this->m_estDeplacementActif = NTRUE;
	}
}

#endif // NLIB_MODULE_SDL

