#define NLIB_MODULE_SDL_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------------
// namespace NLib::Module::SDL
// ---------------------------

#ifdef NLIB_MODULE_SDL
/* Initialiser la SDL */
NBOOL NLib_Module_SDL_Initialiser( void )
{
	// Initialiser la SDL
	return ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_GAMECONTROLLER ) != NSDL_ERREUR );
}

/* D�truire la SDL */
void NLib_Module_SDL_Detruire( void )
{
	// Lib�rer curseur
	if( m_curseur != NULL )
	{
		// D�truire
		SDL_FreeCursor( m_curseur );

		// Oublier
		NDISSOCIER_ADRESSE( m_curseur );
	}

	// D�truire la SDL
	SDL_Quit( );
}

/* Changer curseur */
void NLib_Module_SDL_DefinirCurseur( const SDL_Surface *surface,
	NS32 x,
	NS32 y )
{
	// Lib�rer l'ancien curseur
	if( m_curseur != NULL )
	{
		// D�truire
		SDL_FreeCursor( m_curseur );

		// Oublier
		NDISSOCIER_ADRESSE( m_curseur );
	}
	else
		if( m_curseurDefaut == NULL )
			m_curseurDefaut = SDL_GetCursor( );

	// Cr�er
	if( !( m_curseur = SDL_CreateColorCursor( (SDL_Surface*)surface,
		x,
		y ) ) )
		return;

	// D�finir
	SDL_SetCursor( m_curseur );
}

void NLib_Module_SDL_DefinirCurseur2( const SDL_Surface *surface )
{
	NLib_Module_SDL_DefinirCurseur( surface,
		0,
		0 );
}

/* Restaurer curseur */
void NLib_Module_SDL_RestaurerCurseur( void )
{
	// V�rifier
	if( !m_curseurDefaut )
		return;

	// Restaurer
	SDL_SetCursor( m_curseurDefaut );
}

#endif // NLIB_MODULE_SDL

