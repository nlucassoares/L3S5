#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------------
// struct NLib::Module::SDL::TTF::NPolice
// --------------------------------------

#ifdef NLIB_MODULE_SDL_TTF
/* Construire la police */
__ALLOC NPolice *NLib_Module_SDL_TTF_NPolice_Construire( const char *lien,
	NU32 taille )
{
	return NLib_Module_SDL_TTF_NPolice_Construire2( lien,
		taille,
		(NCouleur){ 0,
			0,
			0,
			0xFF } );
}

__ALLOC NPolice *NLib_Module_SDL_TTF_NPolice_Construire2( const char *lien,
	NU32 taille,
	NCouleur couleur )
{
	// Sortie
	__OUTPUT NPolice *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NPolice ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Cr�er la police
	if( !( out->m_police = TTF_OpenFont( lien,
		(NS32)taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL_TTF );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_couleur = couleur;
	out->m_taille = taille;

	// OK
	return out;
}

/* D�truire la police */
void NLib_Module_SDL_TTF_NPolice_Detruire( NPolice **this )
{
	// Fermer la police
	TTF_CloseFont( (*this)->m_police );

	// Lib�rer
	NFREE( *this );
}

/* Cr�er un texte */
__ALLOC NSurface *NLib_Module_SDL_TTF_NPolice_CreerTexte( const NPolice *this,
	const NFenetre *fenetre,
	const char *texte )
{
	// Couleur
	SDL_Color couleur;

	// Surface
	SDL_Surface *rendu;

	// Sortie
	__OUTPUT NSurface *out;

	// Copier la couleur
	NDEFINIR_COULEUR_A( couleur,
		this->m_couleur.r,
		this->m_couleur.g,
		this->m_couleur.b,
		0xFF );

	// Rendre le texte
	if( !( rendu = TTF_RenderText_Blended( this->m_police,
		texte,
		couleur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL_TTF );

		// Quitter
		return NULL;
	}

	// Cr�er la surface
	if( !( out = NLib_Module_SDL_Surface_NSurface_Construire3( fenetre,
		rendu ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NLIB_NETTOYER_SURFACE( rendu );

		// Quitter
		return NULL;
	}

	// Lib�rer la surface
	NLIB_NETTOYER_SURFACE( rendu );

	// OK
	return out;
}

/* D�finir la couleur */
void NLib_Module_SDL_TTF_NPolice_DefinirCouleur( NPolice *this,
	NU8 r,
	NU8 g,
	NU8 b )
{
	NDEFINIR_COULEUR( this->m_couleur,
		r,
		g,
		b );
}

/* D�finir le style */
void NLib_Module_SDL_TTF_NPolice_DefinirStyle( NPolice *this,
	NStylePolice style )
{
	TTF_SetFontStyle( this->m_police,
		(NS32)style );
}

/* Obtenir la taille */
NU32 NLib_Module_SDL_TTF_NPolice_ObtenirTaille( const NPolice *this )
{
	return this->m_taille;
}

/* Calculer taille texte */
NUPoint NLib_Module_SDL_TTF_NPolice_CalculerTailleTexte( const NPolice *this,
	const char *texte )
{
	// Sortie
	__OUTPUT NUPoint sortie = { 0, 0 };

	// R�cup�rer la taille
	if( TTF_SizeText( this->m_police,
		texte,
		(NS32*)&sortie.x,
		(NS32*)&sortie.y ) == NSDL_ERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL_TTF );

		// Quitter
		return (NUPoint){ 0, 0 };
	}

	// OK
	return sortie;
}

#endif // NLIB_MODULE_SDL_TTF

