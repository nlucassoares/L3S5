#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------
// namespace NLib::Module::SDL::TTF
// --------------------------------

#ifdef NLIB_MODULE_SDL_TTF
/* Initialiser SDL TTF */
NBOOL NLib_Module_SDL_TTF_Initialiser( void )
{
	return TTF_Init( ) != NSDL_ERREUR;
}

/* D�truire SDL TTF */
void NLib_Module_SDL_TTF_Detruire( void )
{
	TTF_Quit( );
}

#endif // NLIB_MODULE_SDL_TTF

