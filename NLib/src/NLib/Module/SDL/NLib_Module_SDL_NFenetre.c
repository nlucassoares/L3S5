#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// struct NLib::Module::SDL::NFenetre
// ----------------------------------

#ifdef NLIB_MODULE_SDL
/* Construire la fen�tre */
__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire( const char *titre,
	NUPoint resolution )
{
	return NLib_Module_SDL_NFenetre_Construire2( titre,
		resolution,
		NTRUE );
}

__ALLOC NFenetre *NLib_Module_SDL_NFenetre_Construire2( const char *titre,
	NUPoint resolution,
	NBOOL estVisible )
{
	// Sortie
	__OUTPUT NFenetre *out;

	// Position de la fen�tre
	NSPoint position;

	// Mode d'affichage actuel
	SDL_DisplayMode modeAffichageEcran;

	// Index renderer
	NU32 indexRenderer;

	// Nombre renderer
	NS32 nombreRenderer;

	// Renderer informations
	SDL_RendererInfo informationsRenderer;

	// Renderer pr�f�r� pr�sent? (NLIB_MODULE_SDL_RENDERER)
	NBOOL estRendererPresent = NFALSE;

	// Obtenir le mode d'affichage
	if( SDL_GetCurrentDisplayMode( 0,
		&modeAffichageEcran ) != 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NULL;
	}

	// D�finir la position
	NDEFINIR_POSITION( position,
		SDL_WINDOWPOS_CENTERED,
		( modeAffichageEcran.h <= (NS32)resolution.y ) ?
			10
			: SDL_WINDOWPOS_CENTERED );

	// Construire la fen�tre
	if( !( out = calloc( 1,
		sizeof( NFenetre ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Cr�er la fen�tre
	if( !( out->m_fenetre = SDL_CreateWindow( titre,
		position.x,
		position.y,
		resolution.x,
		resolution.y,
		estVisible ? SDL_WINDOW_SHOWN : SDL_WINDOW_HIDDEN ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Quitter
		return NULL;
	}

	// V�rifier qu'il y ait bien des renderers de disponible
	if( ( nombreRenderer = SDL_GetNumRenderDrivers( ) ) <= 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Lib�rer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Chercher le renderer qui nous int�resse
	for( indexRenderer = 0; indexRenderer < (NU32)nombreRenderer; indexRenderer++ )
	{
		// Obtenir les informations
		if( SDL_GetRenderDriverInfo( indexRenderer,
			&informationsRenderer ) == NSDL_ERREUR )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_SDL );

			// Lib�rer
			SDL_DestroyWindow( out->m_fenetre );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// V�rifier le nom du renderer
		if( !strcmp( informationsRenderer.name,
			NLIB_MODULE_SDL_RENDERER ) )
		{
			// On a trouv� notre renderer
			estRendererPresent = NTRUE;

			// Sortie
			break;
		}
	}

	// V�rifier
	if( !estRendererPresent )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Lib�rer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Cr�er le renderer
	if( !( out->m_renderer = SDL_CreateRenderer( out->m_fenetre,
		indexRenderer,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SDL );

		// Lib�rer
		SDL_DestroyWindow( out->m_fenetre );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// D�finir la m�thode alpha du renderer
	SDL_SetRenderDrawBlendMode( out->m_renderer,
		SDL_BLENDMODE_BLEND );

	// Enregistrer
	out->m_resolution = resolution;

	// OK
	return out;
}

/* D�truire la fen�tre */
void NLib_Module_SDL_NFenetre_Detruire( NFenetre **this )
{
	// Fermer la fen�tre
	SDL_DestroyWindow( (*this)->m_fenetre );

	// Lib�rer
	NFREE( *this );
}

/* Obtenir la fen�tre */
SDL_Window *NLib_Module_SDL_NFenetre_ObtenirFenetre( const NFenetre *this )
{
	return this->m_fenetre;
}

/* Obtenir le renderer */
SDL_Renderer *NLib_Module_SDL_NFenetre_ObtenirRenderer( const NFenetre *this )
{
	return this->m_renderer;
}

/* Obtenir la r�solution */
const NUPoint *NLib_Module_SDL_NFenetre_ObtenirResolution( const NFenetre *this )
{
	return &this->m_resolution;
}

/* Obtenir l'�venement */
SDL_Event *NLib_Module_SDL_NFenetre_ObtenirEvenement( const NFenetre *this )
{
	return &((NFenetre*)this)->m_evenement;
}

/* Nettoyer la fen�tre */
void NLib_Module_SDL_NFenetre_Nettoyer( NFenetre *this )
{
	NLib_Module_SDL_NFenetre_Nettoyer2( this,
		0,
		0,
		0,
		0xFF );
}

void NLib_Module_SDL_NFenetre_Nettoyer2( NFenetre *this,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a )
{
	// D�finir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		r,
		g,
		b,
		a );

	// Nettoyer
	SDL_RenderClear( this->m_renderer );
}

/* Dessiner un point */
void NLib_Module_SDL_NFenetre_DessinerPoint( NFenetre *this,
	NSPoint p,
	NCouleur couleur )
{
	// D�finir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawPoint( this->m_renderer,
		p.x,
		p.y );
}

/* Dessiner des points */
void NLib_Module_SDL_NFenetre_DessinerPoints( NFenetre *this,
	NSPoint *p,
	NU32 nombrePoint,
	NCouleur couleur )
{
	// D�finir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawPoints( this->m_renderer,
		(SDL_Point*)p,
		nombrePoint );
}

/* Dessiner une ligne */
void NLib_Module_SDL_NFenetre_DessinerLigne( NFenetre *this,
	NSPoint p1,
	NSPoint p2,
	NCouleur couleur )
{
	// D�finir la couleur
	SDL_SetRenderDrawColor( this->m_renderer,
		couleur.r,
		couleur.g,
		couleur.b,
		couleur.a );

	// Dessiner
	SDL_RenderDrawLine( this->m_renderer,
		p1.x,
		p1.y,
		p2.x,
		p2.y );
}

/* Dessiner un rectangle */
void NLib_Module_SDL_NFenetre_DessinerRectangle( NFenetre *this,
	NSRect rectangle,
	NU32 epaisseur,
	NCouleur couleurContour,
	NCouleur couleurFond )
{
	// Rectangle
	SDL_Rect r[ NDIRECTIONS + 1 ];

	// D�finir rectangle bordures
		// Haut
			NDEFINIR_POSITION( r[ NHAUT ],
				rectangle.m_position.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NHAUT ],
				rectangle.m_taille.x,
				epaisseur );
		// Bas
			NDEFINIR_POSITION( r[ NBAS ],
				rectangle.m_position.x,
				rectangle.m_position.y + rectangle.m_taille.y );

			NDEFINIR_TAILLE( r[ NBAS ],
				rectangle.m_taille.x + epaisseur, // Correction
				epaisseur );
		// Gauche
			NDEFINIR_POSITION( r[ NGAUCHE ],
				rectangle.m_position.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NGAUCHE ],
				epaisseur,
				rectangle.m_taille.y );
		// Droite
			NDEFINIR_POSITION( r[ NDROITE ],
				rectangle.m_position.x + rectangle.m_taille.x,
				rectangle.m_position.y );

			NDEFINIR_TAILLE( r[ NDROITE ],
				epaisseur,
				rectangle.m_taille.y );
		// Centre
			NDEFINIR_POSITION( r[ NDIRECTIONS ],
				rectangle.m_position.x + epaisseur,
				rectangle.m_position.y + epaisseur );

			NDEFINIR_TAILLE( r[ NDIRECTIONS ],
				rectangle.m_taille.x - epaisseur,
				rectangle.m_taille.y - epaisseur );

	// Dessiner
	NLib_Module_SDL_NFenetre_DessinerRectangle2( this,
		r,
		&couleurContour,
		&couleurFond );
}

void NLib_Module_SDL_NFenetre_DessinerRectangle2( NFenetre *this,
	const SDL_Rect coordonnees[ NDIRECTIONS + 1 ],
	const NCouleur *couleurContour,
	const NCouleur *couleurFond )
{
	// D�finir la couleur du dessin
	SDL_SetRenderDrawColor( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
		couleurContour->r,
		couleurContour->g,
		couleurContour->b,
		couleurContour->a );

	// Dessiner rectangles
	SDL_RenderFillRects( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
		coordonnees,
		NDIRECTIONS );

	// Si il y a un fond
	if( couleurFond->a != 0 )
	{
		// D�finir la couleur du dessin
		SDL_SetRenderDrawColor( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
			couleurFond->r,
			couleurFond->g,
			couleurFond->b,
			couleurFond->a );

		// Dessiner le rectangle
		SDL_RenderFillRect( NLib_Module_SDL_NFenetre_ObtenirRenderer( this ),
			&coordonnees[ NDIRECTIONS ] );
	}
}

/* Actualiser le renderer */
void NLib_Module_SDL_NFenetre_Update( NFenetre *this )
{
	SDL_RenderPresent( this->m_renderer );
}

/* D�finir un clip rect */
void NLib_Module_SDL_NFenetre_DefinirClipRect( NFenetre *this,
	NSRect rect )
{
	// Rect
	SDL_Rect clipRect;

	// Copier
		// Position
			NDEFINIR_POSITION( clipRect,
				rect.m_position.x,
				rect.m_position.y );
		// Taille
			NDEFINIR_TAILLE( clipRect,
				rect.m_taille.x,
				rect.m_taille.y );

	// D�finir le clip rect
	SDL_RenderSetClipRect( this->m_renderer,
		&clipRect );
}

/* Supprimer le clip rect */
void NLib_Module_SDL_NFenetre_SupprimerClipRect( NFenetre *this )
{
	// Supprimer
	SDL_RenderSetClipRect( this->m_renderer,
		NULL );
}

/* Afficher fen�tre (SDL_WINDOW_SHOWN) */
void NLib_Module_SDL_NFenetre_AfficherFenetre( NFenetre *this )
{
	SDL_ShowWindow( this->m_fenetre );
}

/* Cache fen�tre (SDL_WINDOW_HIDDEN) */
void NLib_Module_SDL_NFenetre_CacheFenetre( NFenetre *this )
{
	SDL_HideWindow( this->m_fenetre );
}

#endif // NLIB_MODULE_SDL

