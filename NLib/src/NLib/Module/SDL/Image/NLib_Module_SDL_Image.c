#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::SDL::Image
// ----------------------------------

#ifdef NLIB_MODULE_SDL_IMAGE
/* Initialiser SDL Image */
NBOOL NLib_Module_SDL_Image_Initialiser( void )
{
	return ( IMG_Init( IMG_INIT_PNG ) == IMG_INIT_PNG );
}


/* D�truire SDL Image */
void NLib_Module_SDL_Image_Detruire( void )
{
	IMG_Quit( );
}

/* Charger une image */
__ALLOC SDL_Surface *NLib_Module_SDL_Image_Charger( const char *lien )
{
	return IMG_Load( lien );
}
#endif // NLIB_MODULE_SDL_IMAGE

