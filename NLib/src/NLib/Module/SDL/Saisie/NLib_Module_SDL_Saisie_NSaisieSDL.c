#define NLIB_MODULE_SDL_SAISIE_NSAISIESDL_INTERNE
#include "../../../../../../NLib/include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::SDL::Saisie::NSaisieSDL
// --------------------------------------------

#ifdef NLIB_MODULE_SDL
/* Supprimer les cadres (priv�e) */
void NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( NSaisieSDL *this )
{
	// It�rateur
	NU32 i = 0;

	// Cadres
		// 2D
			for( ; i < this->m_nbCadres; i++ )
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadre[ i ] );
		// 1D
			NFREE( this->m_cadre );

	// Mettre � z�ro
	this->m_nbCadres = 0;
	this->m_idCadreClique = NSAISIE_SDL_ID_CADRE_AUCUN;
}

/* Effacer caract�re */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_EffacerCaractereInterne( NSaisieSDL *this )
{
	// Taille actuelle
	NS32 tailleActuelle = 0;

	// Cha�ne temporaire
	char *strTemp = NULL;

	// V�rifier
	if( !this->m_texte
		|| !( tailleActuelle = strlen( this->m_texte ) ) )
		return NFALSE;

	// Copier
		// Allouer la m�moire
			if( !( strTemp = (char*)calloc( tailleActuelle + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}
		// Copier
			memcpy( strTemp,
				this->m_texte,
				tailleActuelle );

	// Suppression
	if( this->m_tailleMaximale )
	{
		// Si curseur dans cha�ne (pas au point initial)
		if( this->m_curseur > 0
			&& this->m_curseur - 1 + tailleActuelle - this->m_curseur < this->m_tailleMaximale )
		{
			// Copier la cha�ne
			memcpy( this->m_texte + this->m_curseur - 1,
				strTemp + this->m_curseur,
				tailleActuelle - this->m_curseur );

			// Caract�re de fin
			this->m_texte[ tailleActuelle - 1 ] = '\0';
		}
	}
	else
	{
		// Suppression totale de la cha�ne
		if( tailleActuelle <= 1 )
		{
			// Lib�rer
			NFREE( this->m_texte );
		}
		// Il restera un caract�re ou plus
		else if( this->m_curseur > 0 )
		{
			// R�allouer -1 case
			if( !NLib_Memoire_ReallouerMemoire( &this->m_texte,
				tailleActuelle + 1,
				-1 ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}

			// Copier
				// Apr�s curseur
					memcpy( this->m_texte + this->m_curseur - 1,
						strTemp + this->m_curseur,
						tailleActuelle - this->m_curseur );
				// Clore cha�ne
					this->m_texte[ tailleActuelle - 1 ] = '\0';
		}
	}

	// Lib�rer
	NFREE( strTemp );

	// D�cr�menter curseur
	if( this->m_curseur > 0 )
		this->m_curseur--;

	// OK
	return NTRUE;
}

/* Vider le texte */
void NLib_Module_SDL_Saisie_NSaisieSDL_ViderChaine( NSaisieSDL *this )
{
	// Mettre � z�ro
	memset( this->m_texte,
		0,
		strlen( this->m_texte ) );

	// Curseur � z�ro
	this->m_curseur = 0;
}

/* Mise � jour */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_MiseAJourInterne( NSaisieSDL *this,
	char caractere,
	char **chaine,
	NU32 tailleActuelle,
	NU32 tailleMaximale )
{
	// Cha�ne temporaire
	char *temp = NULL;

	// V�rifier
		// Param�tres
			if( caractere == '\0'
				|| !chaine
				|| ( tailleActuelle >= ( ( tailleMaximale ) >= 0 ? ( tailleMaximale - 1 ) : 0 )
					&& tailleMaximale ) )
				return NFALSE;

	// R�allouer la bonne taille si n�cessaire
	if( !tailleMaximale )
	{
		// Allouer
		if( !( temp = calloc( tailleActuelle + 2,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le caract�re
			// Copier jusqu'au curseur
				memcpy( temp,
					*chaine,
					this->m_curseur * sizeof( char ) );
			// Ajouter caract�re
				temp[ this->m_curseur ] = caractere;
			// Copier depuis curseur jusqu'� la fin
				memcpy( temp + this->m_curseur,
					*chaine + this->m_curseur + 1,
					( (NS32)tailleActuelle - (NS32)this->m_curseur - 1 ) >= 0 ?
						( (NS32)tailleActuelle - (NS32)this->m_curseur - 1 )
						: 0 );

		// Lib�rer
		NFREE( *chaine );

		// Stocker nouvelle adresse
		*chaine = temp;

		// Incr�menter curseur
		this->m_curseur++;
	}
	else
	{
		// Copier ancienne cha�ne
			// Allouer la m�moire
				if( !( temp = (char*)calloc( tailleMaximale + 1,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
					
					// Quitter
					return NFALSE;
				}
			// Copier
				memcpy( temp,
					*chaine,
					tailleMaximale * sizeof( char ) );

		// Ajouter le caract�re
			// Copier jusqu'au curseur
				memcpy( temp,
					*chaine,
					this->m_curseur * sizeof( char ) );
			// Ajouter caract�re
				(*chaine)[ this->m_curseur ] = caractere;
			// Copier depuis curseur jusqu'� la fin
				memcpy( temp + this->m_curseur,
					*chaine + this->m_curseur + 1,
					(NS32)tailleMaximale - (NS32)this->m_curseur + 1 );

		// Lib�rer
		NFREE( temp );

		// Incr�menter curseur
		if( this->m_curseur < tailleMaximale )
			this->m_curseur++;
	}

	// OK
	return NTRUE;
}

// V�rification cadres
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_VerifierCadreInterne( NSaisieSDL *this,
	SDL_Event *e,
	NSPoint *p )
{
	// It�rateur
	NU32 i = 0;

	// Clic?
	if( e->button.button == SDL_BUTTON_LEFT )
	{
		// Si cadre(s) d�fini(s)
		if( this->m_cadre )
		{
			// Cadre englobant tout
			if( this->m_cadre == NSAISIE_SDL_CADRE_TOUT )
			{
				// Noter
				this->m_idCadreClique = NSAISIE_SDL_ID_CADRE_TOUT;

				// Cadre cliqu�!
				return NTRUE;
			}
			else
			{
				// Chercher un cadre OK
				for( i = 0; i < this->m_nbCadres; i++ )
					// Colision?
					if( NLib_Math_Geometrie_EstColision2( *NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadre[ i ] ),
						*NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre[ i ] ),
						*p ) )
					{
						// Noter
						this->m_idCadreClique = i;

						// Cadre cliqu�!
						return NTRUE;
					}

				// Rien n'a �t� trouv�
				return NFALSE;
			}
		}
		else
		// Aucun cadre
			return NFALSE;
	}
	// Pas de clic
	else
		return NFALSE;
}

/* Construire l'objet */
__ALLOC NSaisieSDL *NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NModeSaisieSDL mode,
	NU32 tailleMax,
	SDL_Keycode toucheArret,
	__CALLBACK void ( __cdecl *callbackGestion )( void* ),
	__CALLBACK void ( __cdecl *callbackActualisation )( void* ),
	__CALLBACK NBOOL ( __cdecl *callbackUpdateChaine )( void* ),
	void *argumentCallbackGestion,
	void *argumentCallbackActualisation,
	void *argumentCallbackUpdateChaine,
	const char *chaineInitiale )
{
	// Sortie
	__OUTPUT NSaisieSDL *out;

	// V�rifier param�tres
	if( !( mode & NMODE_SAISIE_SDL_TEXTE_TOTAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NSaisieSDL ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
		// Mode
			out->m_mode = mode;
		// Touche d'arr�t
			out->m_touche = toucheArret;
		// Taille maximale
			out->m_tailleMaximale = tailleMax;
		// Callback
			out->m_callbackGestion = callbackGestion;
			out->m_callbackActualisation = callbackActualisation;
			out->m_callbackUpdateChaine = callbackUpdateChaine;
		// Arguments callback
			out->m_argumentCallbackGestion = argumentCallbackGestion;
			out->m_argumentCallbackActualisation = argumentCallbackActualisation;
			out->m_argumentCallbackUpdateChaine = argumentCallbackUpdateChaine;

	// Allouer si n�cessaire
	if( out->m_tailleMaximale )
		// Allouer
		if( !( out->m_texte = calloc( out->m_tailleMaximale + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NULL;
		}

	// D�finir texte
	NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( out,
		chaineInitiale );

	// OK
	return out;
}

/* D�truire l'objet */
void NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( NSaisieSDL **this )
{
	// Lib�rer
		// Texte
			NFREE( (*this)->m_texte );
		// Cadres
			NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( *this );

	NFREE( *this );
}

/* Saisie */
NRetourSaisieSDL NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( NSaisieSDL *this,
	SDL_Event *e, // Evenement["Communiquer l'�venement pour �viter la boucle infinie de r�p�titions de saisie( )"]
	NBOOL *ptrContinuer, // Handle �tat boucle si externe
	NBOOL autoriseTab, // Autoriser "tabulation"?
	char **callbackChaine ) // Stocke en temps r�el le texte saisi
{
	// Etat si non fourni en entr�e
	NBOOL continuer = NTRUE;

	// R�sultat �venement (si touche press�e, NTRUE)
	NBOOL resultat = NFALSE;

	// Position souris
	NSPoint pSouris;

	// Caract�re press�
	char caractere = 0;

	// Verrouillage accent
	NAccentSaisieSDL accent = NACCENT_SAISIE_SDL_AUCUN;

	// Modificateurs touche
	NBOOL majuscule = NFALSE;
	NBOOL altgr = NFALSE;

	// Initialiser
		// En cours...
			this->m_estEnCours = ptrContinuer ?
				ptrContinuer
				: &continuer;
		// Curseur
			this->m_curseur = this->m_texte ?
				strlen( this->m_texte )
				: 0;

	// Saisie...
	do
	{
		// R�sultat traitement � z�ro (pour appel callback m�j)
		resultat = NFALSE;

		// Lire les events
		while( SDL_PollEvent( e )
			&& *this->m_estEnCours )
		{
			// Caract�re
			caractere = 0;

			// Verrouillage majuscule?
			majuscule = ( SDL_GetModState( ) & KMOD_CAPS ) ? NTRUE : NFALSE;
			majuscule = ( SDL_GetKeyboardState( NULL )[ SDL_SCANCODE_LSHIFT ] || SDL_GetKeyboardState( NULL )[ SDL_SCANCODE_RSHIFT ] ) ?
				!majuscule
				: majuscule;

			// Lecture
			switch( e->type )
			{
				case SDL_WINDOWEVENT:
					switch( e->window.event )
					{
						case SDL_WINDOWEVENT_CLOSE:
							// Oublier
							NDISSOCIER_ADRESSE( this->m_estEnCours );

							// On sort
							return NRETOUR_SAISIE_SDL_QUITTER;

						default:
							break;
					}
					break;
				case SDL_MOUSEMOTION:
					// Mettre � jour position souris
					NDEFINIR_POSITION( pSouris,
						e->motion.x,
						e->motion.y );
					break;
				case SDL_MOUSEBUTTONUP:
					if( NLib_Module_SDL_Saisie_NSaisieSDL_VerifierCadreInterne( this,
						e,
						&pSouris ) )
					{
						// Plus en cours
						*this->m_estEnCours = NFALSE;
						
						// Oublier
						NDISSOCIER_ADRESSE( this->m_estEnCours );

						// Sortie via clic
						return NRETOUR_SAISIE_SDL_CLIC_BOUTON;
					}
					break;
				case SDL_KEYUP:
					// Touches de modificateurs
						// Shift
							switch( e->key.keysym.sym )
							{
								case SDLK_LSHIFT:
								case SDLK_RSHIFT:
									majuscule = !majuscule;
									break;
								case SDLK_RALT:
									altgr = NFALSE;
									break;

								default:
									break;
							}
					break;
				case SDL_KEYDOWN:
					// Touches de modificateurs
					switch( e->key.keysym.sym )
					{
						case SDLK_LSHIFT:
						case SDLK_RSHIFT:
							majuscule = !majuscule;
							break;
						case SDLK_CAPSLOCK:
							majuscule = !majuscule;
							break;
						case SDLK_RALT:
							altgr = NTRUE;
							break;
						case SDLK_TAB:
							if( autoriseTab )
							{
								// Oublier
								NDISSOCIER_ADRESSE( this->m_estEnCours );

								// Suivant!
								return NRETOUR_SAISIE_SDL_TAB;
							}
							break;

						case SDLK_BACKSPACE:
							if( NLib_Module_SDL_Saisie_NSaisieSDL_EffacerCaractereInterne( this ) )
								resultat = NTRUE;
							break;

						default:
							break;
					}

					// Lettres
					if( this->m_mode & NMODE_SAISIE_SDL_LETTRES )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_a:
								NSAISIE_CARACTERE( 'a', 'A', majuscule, caractere );
								break;
							case SDLK_b:
								NSAISIE_CARACTERE( 'b', 'B', majuscule, caractere );
								break;
							case SDLK_c:
								NSAISIE_CARACTERE( 'c', 'C', majuscule, caractere );
								break;
							case SDLK_d:
								NSAISIE_CARACTERE( 'd', 'D', majuscule, caractere );
								break;
							case SDLK_e:
								if( !altgr )
									NSAISIE_CARACTERE( 'e', 'E', majuscule, caractere );
								break;
							case SDLK_f:
								NSAISIE_CARACTERE( 'f', 'F', majuscule, caractere );
								break;
							case SDLK_g:
								NSAISIE_CARACTERE( 'g', 'G', majuscule, caractere );
								break;
							case SDLK_h:
								NSAISIE_CARACTERE( 'h', 'H', majuscule, caractere );
								break;
							case SDLK_i:
								NSAISIE_CARACTERE( 'i', 'I', majuscule, caractere );
								break;
							case SDLK_j:
								NSAISIE_CARACTERE( 'j', 'J', majuscule, caractere );
								break;
							case SDLK_k:
								NSAISIE_CARACTERE( 'k', 'K', majuscule, caractere );
								break;
							case SDLK_l:
								NSAISIE_CARACTERE( 'l', 'L', majuscule, caractere );
								break;
							case SDLK_m:
								NSAISIE_CARACTERE( 'm', 'M', majuscule, caractere );
								break;
							case SDLK_n:
								NSAISIE_CARACTERE( 'n', 'N', majuscule, caractere );
								break;
							case SDLK_o:
								NSAISIE_CARACTERE( 'o', 'O', majuscule, caractere );
								break;
							case SDLK_p:
								NSAISIE_CARACTERE( 'p', 'P', majuscule, caractere );
								break;
							case SDLK_q:
								NSAISIE_CARACTERE( 'q', 'Q', majuscule, caractere );
								break;
							case SDLK_r:
								NSAISIE_CARACTERE( 'r', 'R', majuscule, caractere );
								break;
							case SDLK_s:
								NSAISIE_CARACTERE( 's', 'S', majuscule, caractere );
								break;
							case SDLK_t:
								NSAISIE_CARACTERE( 't', 'T', majuscule, caractere );
								break;
							case SDLK_u:
								NSAISIE_CARACTERE( 'u', 'U', majuscule, caractere );
								break;
							case SDLK_v:
								NSAISIE_CARACTERE( 'v', 'V', majuscule, caractere );
								break;
							case SDLK_w:
								NSAISIE_CARACTERE( 'w', 'W', majuscule, caractere );
								break;
							case SDLK_x:
								NSAISIE_CARACTERE( 'x', 'X', majuscule, caractere );
								break;
							case SDLK_y:
								NSAISIE_CARACTERE( 'y', 'Y', majuscule, caractere );
								break;
							case SDLK_z:
								NSAISIE_CARACTERE( 'z', 'Z', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Chiffres
					if( this->m_mode & NMODE_SAISIE_SDL_CHIFFRES )
					{
						switch( e->key.keysym.sym )
						{
							// Clavier
							case SDLK_0:
								NSAISIE_CARACTERE( '\0', '0', majuscule, caractere );
								break;
							case SDLK_1:
								NSAISIE_CARACTERE( '\0', '1', majuscule, caractere );
								break;
							case SDLK_2:
								NSAISIE_CARACTERE( '\0', '2', majuscule, caractere );
								break;
							case SDLK_3:
								NSAISIE_CARACTERE( '\0', '3', majuscule, caractere );
								break;
							case SDLK_4:
								NSAISIE_CARACTERE( '\0', '4', majuscule, caractere );
								break;
							case SDLK_5:
								NSAISIE_CARACTERE( '\0', '5', majuscule, caractere );
								break;
							case SDLK_6:
								NSAISIE_CARACTERE( '\0', '6', majuscule, caractere );
								break;
							case SDLK_7:
								NSAISIE_CARACTERE( '\0', '7', majuscule, caractere );
								break;
							case SDLK_8:
								NSAISIE_CARACTERE( '\0', '8', majuscule, caractere );
								break;
							case SDLK_9:
								NSAISIE_CARACTERE( '\0', '9', majuscule, caractere );
								break;

							// Keypad
							case SDLK_KP_0:
								NSAISIE_CARACTERE( '0', '0', majuscule, caractere );
								break;
							case SDLK_KP_1:
								NSAISIE_CARACTERE( '1', '1', majuscule, caractere );
								break;
							case SDLK_KP_2:
								NSAISIE_CARACTERE( '2', '2', majuscule, caractere );
								break;
							case SDLK_KP_3:
								NSAISIE_CARACTERE( '3', '3', majuscule, caractere );
								break;
							case SDLK_KP_4:
								NSAISIE_CARACTERE( '4', '4', majuscule, caractere );
								break;
							case SDLK_KP_5:
								NSAISIE_CARACTERE( '5', '5', majuscule, caractere );
								break;
							case SDLK_KP_6:
								NSAISIE_CARACTERE( '6', '6', majuscule, caractere );
								break;
							case SDLK_KP_7:
								NSAISIE_CARACTERE( '7', '7', majuscule, caractere );
								break;
							case SDLK_KP_8:
								NSAISIE_CARACTERE( '8', '8', majuscule, caractere );
								break;
							case SDLK_KP_9:
								NSAISIE_CARACTERE( '9', '9', majuscule, caractere );
								break;
							case SDLK_KP_PERIOD:
								NSAISIE_CARACTERE( '.', '.', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Espaces
					if( this->m_mode & NMODE_SAISIE_SDL_ESPACES )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_SPACE:
								NSAISIE_CARACTERE( ' ', ' ', majuscule, caractere );
								break;
							case SDLK_TAB:
								if( !autoriseTab )
									NSAISIE_CARACTERE( '\t', '\t', majuscule, caractere );
								break;

							default:
								break;
						}
					}

					// Caract�res sp�ciaux
					if( this->m_mode & NMODE_SAISIE_SDL_CARACTERES_SPECIAUX )
					{
						switch( e->key.keysym.sym )
						{
							case SDLK_0:
								NSAISIE_CARACTERE_VERIFICATION( '�', '@', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_1:
								NSAISIE_CARACTERE_VERIFICATION( '&', '\0', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_2:
								if( !majuscule
									|| altgr )
								{
									// G�rer les accents...
									if( altgr )
									{
										if( accent == NACCENT_SAISIE_SDL_TILDE )
										{
											// Changer caract�re
											caractere = '~';

											// Annuler l'accent
											accent = NACCENT_SAISIE_SDL_AUCUN;
										}
										else
											accent = NACCENT_SAISIE_SDL_TILDE;
									}
									else
										caractere = '�';
								}
								break;
							case SDLK_3:
								NSAISIE_CARACTERE_VERIFICATION( '"', '#', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_4:
								NSAISIE_CARACTERE_VERIFICATION( '\'', '{', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_5:
								NSAISIE_CARACTERE_VERIFICATION( '(', '[', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_6:
								NSAISIE_CARACTERE_VERIFICATION( '-', '|', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_7:
								if( !majuscule
									|| altgr )
								{
									if( altgr )
									{
										if( accent == NACCENT_SAISIE_SDL_GRAVE )
										{
											// Changer caract�re
											caractere = '`';

											// Annuler accent
											accent = NACCENT_SAISIE_SDL_AUCUN;
										}
										else
											accent = NACCENT_SAISIE_SDL_GRAVE;
									}
									else
										caractere = '�';
								}
								break;
							case SDLK_8:
								NSAISIE_CARACTERE_VERIFICATION( '_', '\\', altgr, caractere, ( !majuscule || altgr ) );
								break;
							case SDLK_9:
								NSAISIE_CARACTERE_VERIFICATION( '�', '^', altgr, caractere, ( !majuscule || altgr ) );
								break;

							case SDLK_e:
								NSAISIE_CARACTERE( '\0', '�', altgr, caractere );
								break;

							case SDLK_DOLLAR:
								NSAISIE_CARACTERE( NSAISIE_CARACTERE( '$', '�', altgr, caractere ), NSAISIE_CARACTERE( '�', '�', altgr, caractere ), majuscule, caractere );
								break;
							case 249: // SDLK_PERCENT
								NSAISIE_CARACTERE( '�', '%', majuscule, caractere );
								break;
							case SDLK_CARET:
								if( !majuscule )
								{
									if( accent == NACCENT_SAISIE_SDL_CIRCONFLEXE )
									{
										// Changer caract�re
										caractere = '^';

										// Annuler accent
										accent = NACCENT_SAISIE_SDL_AUCUN;
									}
									else
										accent = NACCENT_SAISIE_SDL_CIRCONFLEXE;
								}
								else
								{
									if( accent == NACCENT_SAISIE_SDL_TREMA )
									{
										// Changer caract�re
										caractere = '�';

										// Annuler accent
										accent = NACCENT_SAISIE_SDL_AUCUN;
									}
									else
										accent = NACCENT_SAISIE_SDL_TREMA;
								}
								break;
							case SDLK_ASTERISK:
								NSAISIE_CARACTERE( '*', '�', majuscule, caractere );
								break;
							case SDLK_COMMA:
								NSAISIE_CARACTERE( ',', '?', majuscule, caractere );
								break;
							case SDLK_SEMICOLON:
								NSAISIE_CARACTERE( ';', '.', majuscule, caractere );
								break;
							case SDLK_COLON:
								NSAISIE_CARACTERE( ':', '/', majuscule, caractere );
								break;
							case SDLK_EXCLAIM:
								NSAISIE_CARACTERE( '!', '�', majuscule, caractere );
								break;
							case SDLK_LESS:
								NSAISIE_CARACTERE( '<', '>', majuscule, caractere );
								break;
							case SDLK_EQUALS:
								NSAISIE_CARACTERE( '=', '+', majuscule, caractere );
								break;
							case SDLK_RIGHTPAREN:
								NSAISIE_CARACTERE( NSAISIE_CARACTERE( ')', ']', altgr, caractere ), NSAISIE_CARACTERE( '�', ']', altgr, caractere ), majuscule, caractere );
								break;

							case SDLK_KP_PLUS:
								caractere = '+';
								break;
							case SDLK_KP_MINUS:
								caractere = '-';
								break;
							case SDLK_KP_MULTIPLY:
								caractere = '*';
								break;
							case SDLK_KP_DIVIDE:
								caractere = '/';
								break;

							default:
								break;
						}
					}

					// G�rer les accents
					switch( accent )
					{
						case NACCENT_SAISIE_SDL_AIGU:
							break;
						case NACCENT_SAISIE_SDL_GRAVE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								
								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_CIRCONFLEXE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								
								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_TREMA:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'e':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'i':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'u':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'y':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'E':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'I':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'U':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								default:
									break;
							}
							break;
						case NACCENT_SAISIE_SDL_TILDE:
							switch( caractere )
							{
								case 'a':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'n':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'o':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								case 'A':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'N':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;
								case 'O':
									NSAISIE_CARACTERE_AVEC_ACCENT( '�', caractere, accent );
									break;

								default:
									break;
							}
							break;

						default:
							break;
					} // switch( accent )

					// V�rifier la pression de la touche d'arr�t
					if( e->key.keysym.sym == this->m_touche )
						// Plus en cours
						*this->m_estEnCours = NFALSE;

					// V�rifier pression �chap
					if( e->key.keysym.sym == SDLK_ESCAPE )
					{
						// Oublier
						NDISSOCIER_ADRESSE( this->m_estEnCours );

						// Echap
						return NRETOUR_SAISIE_SDL_ECHAP;
					}

					// Mettre � jour la cha�ne
					if( NLib_Module_SDL_Saisie_NSaisieSDL_MiseAJourInterne( this,
						caractere,
						&this->m_texte,
						this->m_texte ?
							strlen( this->m_texte )
							: 0,
						this->m_tailleMaximale ) )
						resultat = NTRUE;
					break;
			} // switch
		} // while

		// Callback cha�ne
		if( callbackChaine
			&& this->m_texte )
		{
			// Lib�rer ce qu'il y avait avant
			NFREE( *callbackChaine );

			// Allouer la m�moire n�cessaire
			if( !( *callbackChaine = (char*)calloc( strlen( this->m_texte ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Retenter...
				continue;
			}

			// Copier
			memcpy( *callbackChaine,
				this->m_texte,
				strlen( this->m_texte ) * sizeof( char ) );
		}
		else if( !this->m_texte )
			NFREE( *callbackChaine );

		// Appels callback
			// Gestion
				if( this->m_callbackGestion )
					this->m_callbackGestion( this->m_argumentCallbackGestion );
			// Actualisation
				if( this->m_callbackActualisation )
					this->m_callbackActualisation( this->m_argumentCallbackActualisation );
			// Mise � jour cha�ne!
				if( this->m_callbackUpdateChaine )
					if( resultat )
						this->m_callbackUpdateChaine( this->m_argumentCallbackUpdateChaine );
	} while( *this->m_estEnCours );

	// Oublier continuer
	NDISSOCIER_ADRESSE( this->m_estEnCours );

	// OK
	return NRETOUR_SAISIE_SDL_OK;
}

/* D�finir */
// Cadres
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCadres( NSaisieSDL *this,
	const NCadre **cadres,
	NU32 nb )
{
	// It�rateurs
	NU32 i, j;

	// V�rifier
		// Param�tres
			if( !cadres
				|| !nb )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Supprimer les anciens cadres
	NLib_Module_SDL_Saisie_NSaisieSDL_SupprimerCadresInterne( this );

	// Sauvegarder nombre cadre(s)
	this->m_nbCadres = nb;

	// Allouer la m�moire
	if( !( this->m_cadre = (NCadre**)calloc( this->m_nbCadres,
		sizeof( NCadre* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Cr�er les cadres
	for( i = 0; i < this->m_nbCadres; i++ )
		if( !( this->m_cadre[ i ] = NLib_Module_SDL_NCadre_Construire2( cadres[ i ] ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
				// 2D
					for( j = 0; j < i; j++ )
						NLib_Module_SDL_NCadre_Detruire( &this->m_cadre[ j ] );
				// 1D
					NFREE( this->m_cadre );

			// Quitter
			return NFALSE;
		}

	// OK
	return NTRUE;
}

// Touche d'arr�t
void NLib_Module_SDL_Saisie_NSaisieSDL_DefinirToucheArret( NSaisieSDL *this,
	SDL_Keycode touche )
{
	this->m_touche = touche;
}

// Curseur
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirCurseur( NSaisieSDL *this,
	NU32 curseur )
{
	// V�rifier
		// Param�tre
			if( ( curseur >= this->m_tailleMaximale
					&& this->m_tailleMaximale )
				|| curseur >= strlen( this->m_texte ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

				// Quitter
				return NFALSE;
			}

	// Enregistrer
	this->m_curseur = curseur;

	// OK
	return NTRUE;
}

/* D�finir texte */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( NSaisieSDL *this,
	const char *texte )
{
	// Taille texte
	NU32 tailleTexte;

	// V�rifier
	if( !texte )
		return NFALSE;

	// Calculer taille texte
	tailleTexte = strlen( texte );

	if( this->m_tailleMaximale != 0 )
		memcpy( this->m_texte,
			texte,
			( tailleTexte >= this->m_tailleMaximale ) ?
			( ( this->m_tailleMaximale - 1 ) >= 0 ?
				this->m_tailleMaximale - 1
				: 0 )
			: tailleTexte );
	else
	{
		// Lib�rer ancien texte
		NFREE( this->m_texte );

		// Allouer de quoi stocker le texte
		if( !( this->m_texte = calloc( tailleTexte + 1,
				sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Quitter
			return NFALSE;
		}

		// Copier
		memcpy( this->m_texte,
			texte,
			tailleTexte );
	}

	// Placer le curseur
	this->m_curseur = ( this->m_texte != NULL )
		? tailleTexte
		: 0;

	// OK
	return NTRUE;
}

/* Obtenir texte */
const char *NLib_Module_SDL_NSaisieSDL_ObtenirTexte( const NSaisieSDL *this )
{
	return this->m_texte;
}

/* Obtenir cadre cliqu� */
NU32 NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirCadreClique( const NSaisieSDL *this )
{
	return this->m_idCadreClique;
}

/* Obtenir texte */
const char *NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( const NSaisieSDL *this )
{
	return this->m_texte;
}

/* Est en cours? */
NBOOL NLib_Module_SDL_Saisie_NSaisieSDL_EstEnCours( const NSaisieSDL *this )
{
	return ( this->m_estEnCours != NULL ) ?
		*this->m_estEnCours
		: NFALSE;
}

#endif // NLIB_MODULE_SDL

