#define RESEAU_INTERNE
#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------
// namespace NLib::Module::Reseau
// ------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Initialiser r�seau */
NBOOL NLib_Module_Reseau_Initialiser( void )
{
#ifdef IS_WINDOWS
	if( WSAStartup( MAKEWORD( 2, 2 ),
		&m_contexteWSA ) != EXIT_SUCCESS )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_WINSOCK );

		// Quitter
		return NFALSE;
	}
#endif IS_WINDOWS

	// OK
	return NTRUE;
}

/* D�truire r�seau */
void NLib_Module_Reseau_Detruire( void )
{
	// Sous windows
#ifdef IS_WINDOWS
	WSACleanup( );
#endif // IS_WINDOWS
}

#endif // NLIB_MODULE_RESEAU

