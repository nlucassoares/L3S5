#include "../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------------
// namespace NLib::Module::Reseau::Client::Thread
// ----------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Emission */
void NLib_Module_Reseau_Client_Thread_Emission( NClient *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// D�lais
		NLib_Temps_Attendre( 1 );

		// Si le client est vivant
		if( NLib_Module_Reseau_Client_NClient_EstConnecte( client )
			&& !NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
		{
			// Si il y a quelque chose � �crire dans le flux
			if( NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) ) > 0 )
				// Si on peut �crire dans le flux
				if( NLib_Module_Reseau_Socket_Selection_EstEcritureDisponible( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
				{
					// R�cup�rer le packet
					if( !( packet = NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) ) ) )
						continue;

					// Ecrire dans le flux
					if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( packet,
						NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
							__FUNCTION__,
							errno );

						// Tuer le client
						NLib_Module_Reseau_Client_NClient_ActiverErreur( client );
					}

					// Lib�rer le packet
					NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

					// Supprimer le packet du cache
					NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( client ) );
				}
		}
	} while( NLib_Module_Reseau_Client_NClient_EstThreadEnCours( client ) );
}

/* Reception */
void NLib_Module_Reseau_Client_Thread_Reception( NClient *client )
{
	// Packet
	NPacket *packet;

	do
	{
		// D�lais
		NLib_Temps_Attendre( 1 );

		// Si le client est connect�
		if( NLib_Module_Reseau_Client_NClient_EstConnecte( client )
			&& !NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
		{
			// Si on peut lire dans le flux
			if( NLib_Module_Reseau_Socket_Selection_EstLectureDisponible( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ) ) )
			{
				// Lire dans le flux
				if( !( packet = NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( NLib_Module_Reseau_Client_NClient_ObtenirSocket( client ),
					NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( client ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
						__FUNCTION__,
						errno );

					// Tuer le client
					NLib_Module_Reseau_Client_NClient_ActiverErreur( client );
				}
				// Transmettre au callback
				else
					if( !client->m_callbackReceptionPacket( packet,
						NLib_Module_Reseau_Client_NClient_ObtenirArgumentCallback( client ) ) )
						NLib_Module_Reseau_Client_NClient_ActiverErreur( client );

				// Lib�rer le packet
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );
			}
		}
	} while( NLib_Module_Reseau_Client_NClient_EstThreadEnCours( client ) );
}

/* Update */
void NLib_Module_Reseau_Client_Thread_Update( NClient *client )
{
	do
	{
		// Attendre
		NLib_Temps_Attendre( 1 );

		// Client connect�?
		if( NLib_Module_Reseau_Client_NClient_EstConnecte( client ) )
			// Erreur?
			if( NLib_Module_Reseau_Client_NClient_EstErreur( client ) )
				// D�connecter
				NLib_Module_Reseau_Client_NClient_Deconnecter( client );
	} while( NLib_Module_Reseau_Client_NClient_EstThreadEnCours( client ) );
}

#endif // NLIB_MODULE_RESEAU

