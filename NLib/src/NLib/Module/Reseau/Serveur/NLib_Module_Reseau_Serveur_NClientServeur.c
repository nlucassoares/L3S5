#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------------------
// struct NLib::Module::Reseau::Serveur::NClientServeur
// ----------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Obtenir adresse ip
__ALLOC char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIPInterne( const NClientServeur *this )
{
	// Output
	__OUTPUT char *out;

	// Allocate memory
	if( !( out = calloc( INET_ADDRSTRLEN + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Obtenir adresse
	inet_ntop( AF_INET,
		(PVOID)&this->m_contexteAdressage.sin_addr,
		out,
		INET_ADDRSTRLEN * sizeof( char ) );

	// OK
	return out;
}

// Construire le client
__ALLOC NClientServeur *NLib_Module_Reseau_Serveur_NClientServeur_Construire( SOCKET socketServeur,
	__CALLBACK NBOOL ( *callbackReception )( const NClientServeur*,
		const NPacket* ),
	const NBOOL *estConnexionAutorisee,
	NU32 tempsAvantTimeout )
{
	// Sortie
	__OUTPUT NClientServeur *out;

	// Socket client
	SOCKET socketClient;

	// Adressage socket client
	SOCKADDR_IN addrSocketClient;
	
	// Taille du contexte d'adressage
	NS32 tailleContexteAdressage = sizeof( SOCKADDR_IN );

	// Accepter la requ�te
	if( ( socketClient = accept( socketServeur,
		(struct sockaddr*)&addrSocketClient,
		&tailleContexteAdressage ) ) == INVALID_SOCKET )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_ACCEPT_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer le client
	if( !( out = calloc( 1,
		sizeof( NClientServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer la socket
		closesocket( socketClient );

		// Quitter
		return NULL;
	}

	// Enregistrer la socket
	out->m_socket = socketClient;
	memcpy( &out->m_contexteAdressage,
		&addrSocketClient,
		sizeof( SOCKADDR_IN ) );

	// V�rifier si la connexion est autoris�e
	if( !(*estConnexionAutorisee) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SERVER_CLOSED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Si on a un timeout
	if( tempsAvantTimeout > 0 )
		// D�finir le timeout
		setsockopt( out->m_socket,
			SOL_SOCKET,
			SO_RCVTIMEO,
			(char*)&tempsAvantTimeout,
			sizeof( NU32 ) );

	// Obtenir ip
	if( !( out->m_ip = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIPInterne( out ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Cr�er le cache packet
	if( !( out->m_cachePacket = NLib_Module_Reseau_Packet_NCachePacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Lib�rer
		NFREE( out->m_ip );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire le ping
	if( !( out->m_ping = NLib_Module_Reseau_NPing_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer la socket
		closesocket( out->m_socket );

		// Lib�rer
		NLib_Module_Reseau_Packet_NCachePacket_Detruire( &out->m_cachePacket );

		NFREE( out->m_ip );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_callbackReception = callbackReception;
	out->m_tempsAvantTimeout = tempsAvantTimeout;

	// Z�ro
	out->m_estMort = NFALSE;
	out->m_estEnCours = NTRUE;

	// Cr�er les threads
		// Emission
			out->m_threadEmission = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_Reseau_Serveur_Thread_Client_ThreadEmission,
				out,
				0,
				NULL );
		// Reception
			out->m_threadReception = CreateThread( NULL,
				0,
				(LPTHREAD_START_ROUTINE)NLib_Module_Reseau_Serveur_Thread_Client_ThreadReception,
				out,
				0,
				NULL );
	// OK
	return out;
}

// D�truire le client
void NLib_Module_Reseau_Serveur_NClientServeur_Detruire( NClientServeur **this )
{
	// Arr�ter le client
	(*this)->m_estEnCours = NFALSE;

	// Attendre que les threads se terminent
	while( WaitForSingleObject( (*this)->m_threadEmission,
			INFINITE ) != WAIT_OBJECT_0
		|| WaitForSingleObject( (*this)->m_threadReception,
			INFINITE ) != WAIT_OBJECT_0 )
		NLib_Temps_Attendre( 1 );

	// Fermer les threads
	CloseHandle( (*this)->m_threadEmission );
	CloseHandle( (*this)->m_threadReception );

	// Fermer la socket
	closesocket( (*this)->m_socket );

	// D�truire ping
	NLib_Module_Reseau_NPing_Detruire( &(*this)->m_ping );

	// Lib�rer socket
	NFREE( (*this)->m_ip );

	// D�truire le cache packet
	NLib_Module_Reseau_Packet_NCachePacket_Detruire( &(*this)->m_cachePacket );

	// Lib�rer la m�moire
	NFREE( (*this) );
}

// Est mort?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstMort( const NClientServeur *this )
{
	return this->m_estMort;
}

// Est en cours?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( const NClientServeur *this )
{
	return this->m_estEnCours;
}

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( const NClientServeur *this )
{
	return NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( this->m_cachePacket );
}

// Tuer un client
void NLib_Module_Reseau_Serveur_NClientServeur_Tuer( NClientServeur *this )
{
	this->m_estMort = NTRUE;
}

// D�finir identifiant
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( NClientServeur *this,
	NU32 identifiant )
{
	this->m_identifiantUnique = identifiant;
}

// Obtenir l'identifiant
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( const NClientServeur *this )
{
	return this->m_identifiantUnique;
}

// Obtenir SOCKET
SOCKET NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( const NClientServeur *this )
{
	return this->m_socket;
}

// Obtenir cache packet
NCachePacket *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( const NClientServeur *this )
{
	return this->m_cachePacket;
}

// Obtenir IP
const char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIP( const NClientServeur *this )
{
	return this->m_ip;
}

// Donner un handle de donn�e au client
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( NClientServeur *this,
	char *data )
{
	this->m_donneeExterne = data;
}

// Obtenir les donn�es externes enregistr�es pour ce client
char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( const NClientServeur *this )
{
	return this->m_donneeExterne;
}

// Obtenir ping
const NPing *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( const NClientServeur *this )
{
	return this->m_ping;
}

// Ajouter un packet
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( NClientServeur *this,
	__WILLBEOWNED NPacket *packet )
{
	// Ajouter le packet au cache
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this->m_cachePacket,
		packet );
}

NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( NClientServeur *this,
	const NPacket *packet )
{
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( this->m_cachePacket,
		packet );
}

// Est timeout autoris�?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutorise( const NClientServeur *this )
{
	return this->m_tempsAvantTimeout > 0;
}

// Obtenir temps avant timeout
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeout( const NClientServeur *this )
{
	return this->m_tempsAvantTimeout;
}

#endif // NLIB_MODULE_RESEAU

