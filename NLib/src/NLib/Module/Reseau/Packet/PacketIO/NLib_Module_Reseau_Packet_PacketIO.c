#include "../../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

#ifdef NLIB_MODULE_RESEAU
#ifndef NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
/* PacketIO */
// Cr�er base buffer (priv�e) [HEADER/CRC32]
NBOOL NLib_Module_Reseau_Packet_PacketIO_CreerBaseBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	NU32 *curseurBuffer,
	const char *data,
	NU32 tailleData,
	NU32 curseurData,
	NEtapePacketIO etape )
{
	// Taille � copier dans le buffer
	NU32 tailleACopierBuffer;

	// Calculer taille � copier dans le buffer
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			tailleACopierBuffer = ( tailleData - curseurData >= ( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) - sizeof( NU32 )) ) ?
				( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) - sizeof( NU32 ) )
				: ( tailleData - curseurData );
			break;

		default:
			tailleACopierBuffer = ( tailleData - curseurData >= ( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) ) ) ?
				( NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) )
				: ( tailleData - curseurData );
			break;
	}

	// Copier
	memcpy( buffer,
		NHEADER_BUFFER,
		NTAILLE_HEADER_BUFFER );

	// Incr�menter curseur
	*curseurBuffer += NTAILLE_HEADER_BUFFER;

	// Ajouter crc
	*((NU32*)&buffer[ *curseurBuffer ]) = NLib_Memoire_CalculerCRC( data + curseurData,
		tailleACopierBuffer );

	// Incr�menter
	*curseurBuffer += sizeof( NU32 );

	// OK
	return NTRUE;
}

// Cr�er un buffer (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_CreerBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	const char *data,
	NU32 tailleData,
	NU32 *curseurData,
	NEtapePacketIO etape )
{
	// Curseur buffer
	NU32 curseurBuffer = 0;

	// Taille � ajouter
	NU32 tailleAAjouter;

	// Cr�er base header
	NLib_Module_Reseau_Packet_PacketIO_CreerBaseBufferInterne( buffer,
		&curseurBuffer,
		data,
		tailleData,
		*curseurData,
		etape );

	// Ajouter taille si au d�but
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Ajouter taille
			*((NU32*)&buffer[ curseurBuffer ]) = tailleData;

			// Incr�menter curseur
			curseurBuffer += sizeof( NU32 );
			break;

		default:
			break;
	}

	// Calculer taille � ajouter
	tailleAAjouter = ( tailleData - *curseurData ) >= ( NBUFFER_PACKET - curseurBuffer ) ?
		NBUFFER_PACKET - curseurBuffer
		: ( tailleData - *curseurData );

	// Ajouter donn�es
	memcpy( buffer + curseurBuffer,
		data + *curseurData,
		tailleAAjouter );

	// Incr�menter le curseur
	*curseurData += tailleAAjouter;

	// Ajouter selon �tape
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Recalculer CRC (ajout taille propre � l'header)
			*((NU32*)&buffer[ NTAILLE_HEADER_BUFFER ]) = NLib_Memoire_CalculerCRC( buffer + NTAILLE_HEADER_BUFFER + sizeof( NU32 ),
				NBUFFER_PACKET - NTAILLE_HEADER_BUFFER - sizeof( NU32 ) );
			break;
		
		default:
			break;
	}

	// OK
	return NTRUE;
}

// Recevoir un buffer (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_RecevoirBufferInterne( __OUTPUT char buffer[ NBUFFER_PACKET ],
	SOCKET socket )
{
	// Taille re�ue
	NU32 tailleLue = 0;

	// Taille � lire
	NU32 tailleALire;

	// Code recv
	NS32 code;

	// Lire
	while( tailleLue < NBUFFER_PACKET )
	{
		// Calculer la taille � lire
		tailleALire = NBUFFER_PACKET - tailleLue;

		// Lire flux
		if( ( code = recv( socket,
				buffer + tailleLue,
				tailleALire,
				0 ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
				__FUNCTION__,
				errno );

			// Quitter
			return NFALSE;
		}

		// Incr�menter la taille lue
		tailleLue += (NU32)code;
	}

	// OK
	return NTRUE;
}

// Interpr�ter un buffer (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_InterpreterBufferInterne( __OUTPUT char **out,
	NU32 *tailleCouranteOut,
	NU32 *tailleTotalePacket,
	const char buffer[ NBUFFER_PACKET ],
	NEtapePacketIO etape )
{
	// Curseur buffer
	NU32 curseurBuffer = 0;

	// CRC32
	NU32 CRC32;

	// Header
	char header[ NTAILLE_HEADER_BUFFER + 1 ];

	// Lire l'header
	memcpy( header,
		buffer + curseurBuffer,
		NTAILLE_HEADER_BUFFER );

	// Incr�menter le curseur
	curseurBuffer += NTAILLE_HEADER_BUFFER;

	// V�rifier l'header
	if( memcmp( header,
		NHEADER_BUFFER,
		NTAILLE_HEADER_BUFFER ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Lire le CRC32
	CRC32 = *( (NU32*)( buffer + curseurBuffer ) );

	// Incr�menter curseur
	curseurBuffer += sizeof( NU32 );

	// V�rifier CRC donn�es
	if( NLib_Memoire_CalculerCRC( buffer + curseurBuffer,
		NBUFFER_PACKET - curseurBuffer ) != CRC32 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CRC );

		// Quitter
		return NFALSE;
	}

	// En fonction de l'�tape
	switch( etape )
	{
		case NETAPE_PACKET_IO_BUFFER_HEADER:
			// Lire la taille
			*tailleTotalePacket = *((NU32*)( buffer + curseurBuffer ));

			// Incr�menter le curseur
			curseurBuffer += sizeof( NU32 );
			break;

		default:
			break;
	}

	// Lire donn�es
	if( !NLib_Memoire_AjouterData( out,
		*tailleCouranteOut,
		buffer + curseurBuffer,
		NBUFFER_PACKET - curseurBuffer ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Quitter
		return NFALSE;
	}

	// Incr�menter taille
	*tailleCouranteOut += NBUFFER_PACKET - curseurBuffer;

	// OK
	return NTRUE;
}

// Recevoir un packet (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketInterne( __OUTPUT char **data,
	__OUTPUT NU32 *taille,
	SOCKET socket )
{
	// Etape
	NEtapePacketIO etape = NETAPE_PACKET_IO_BUFFER_HEADER;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Taille totale du packet
	NU32 tailleTotalePacket;

	// Lire
	do
	{
		// Vider buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );
		
		// Lire buffer
		if( !NLib_Module_Reseau_Packet_PacketIO_RecevoirBufferInterne( buffer,
			socket ) )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
				__FUNCTION__,
				errno );

			// Lib�rer
			NFREE( *data );

			// Quitter
			return NFALSE;
		}

		// Interpr�ter le packet
		if( !NLib_Module_Reseau_Packet_PacketIO_InterpreterBufferInterne( data,
			taille,
			&tailleTotalePacket,
			buffer,
			etape ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Lib�rer
			NFREE( *data );

			// Quitter
			return NFALSE;
		}

		// G�rer �tape
		switch( etape )
		{
			case NETAPE_PACKET_IO_BUFFER_HEADER:
				etape = NETAPE_PACKET_IO_BUFFER_CONTENU;
				break;

			default:
				break;
		}
	} while( *taille < tailleTotalePacket );

	// OK
	return NTRUE;
}

// Envoyer un buffer (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerBufferInterne( const char buffer[ NBUFFER_PACKET ],
	SOCKET socket )
{
	// Taille envoy�e
	NU32 tailleEnvoyee = 0;

	// Code send
	NS32 code;

	// Envoyer le buffer
	while( tailleEnvoyee < NBUFFER_PACKET )
	{
		// Envoyer
		if( ( code = send( socket,
				buffer + tailleEnvoyee,
				NBUFFER_PACKET - tailleEnvoyee,
				0 ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
				__FUNCTION__,
				errno );

			// Quitter
			return NFALSE;
		}

		// Incr�menter la taille
		tailleEnvoyee += (NU32)code;
	}

	// OK
	return NTRUE;
}

// Envoyer un packet (priv�e)
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketInterne( const char *data,
	NU32 taille,
	SOCKET socket )
{
	// Curseur data
	NU32 curseurData = 0;

	// Etape
	NEtapePacketIO etape = NETAPE_PACKET_IO_BUFFER_HEADER;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Envoyer
	while( curseurData < taille )
	{
		// Vider le buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Cr�er le buffer
		NLib_Module_Reseau_Packet_PacketIO_CreerBufferInterne( buffer,
			data,
			taille,
			&curseurData,
			etape );

		// Envoyer
		if( !NLib_Module_Reseau_Packet_PacketIO_EnvoyerBufferInterne( buffer,
			socket ) )
		{
			// Notifier
			NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_SEND,
				__FUNCTION__,
				errno );

			// Quitter
			return NFALSE;
		}

		// Etape suivante?
		switch( etape )
		{
			case NETAPE_PACKET_IO_BUFFER_HEADER:
				etape = NETAPE_PACKET_IO_BUFFER_CONTENU;
				break;

			default:
				break;
		}
	}

	// OK
	return NTRUE;
}

// Recevoir un packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( SOCKET socket,
	NBOOL unused )
{
	// Donn�es
	char *data = NULL;

	// Taille
	NU32 taille = 0;

	// Output
	__OUTPUT NPacket *out;

	// R�f�rencer param�tre
	NREFERENCER( unused );

	// Recevoir
	if( !NLib_Module_Reseau_Packet_PacketIO_RecevoirPacketInterne( &data,
		&taille,
		socket ) )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_SOCKET_RECV,
			__FUNCTION__,
			errno );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( out = NLib_Module_Reseau_Packet_NPacket_Construire3( data,
			taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( data );

		// Quitter
		return NULL;
	}

	// Lib�rer
	NFREE( data );

	// OK
	return out;
}

// Envoyer un packet
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( const NPacket *packet,
	SOCKET socket )
{
	return NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacketInterne( NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ),
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ),
		socket );
}
#endif // !NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
#endif // NLIB_MODULE_RESEAU

