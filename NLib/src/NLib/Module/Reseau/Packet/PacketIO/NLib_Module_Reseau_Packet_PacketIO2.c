#include "../../../../../../include/NLib/NLib.h"

#ifdef NLIB_MODULE_RESEAU
#ifdef NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

// Recevoir un packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( SOCKET socket,
	NBOOL estTimeoutAutorise )
{
	// Code retour recv
	NS32 code;

	// Buffer
	char buffer[ NBUFFER_PACKET ];

	// Packet
	__OUTPUT NPacket *packet;

	// Construire le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Lire
	do
	{
		// Vider buffer
		memset( buffer,
			0,
			NBUFFER_PACKET );

		// Lire
		if( ( code = recv( socket,
			buffer,
			NBUFFER_PACKET,
			0 ) ) == INVALID_SOCKET
			&& ( estTimeoutAutorise ?
					( WSAGetLastError( ) != WSAETIMEDOUT )
					: NTRUE ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Lib�rer
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Quitter
			return NULL;
		}
		
		// Copier
		if( code > 0 )
		{
			// Ajouter donn�es
			if( !NLib_Module_Reseau_Packet_NPacket_AjouterData( packet,
				buffer,
				(NU32)code ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_MEMORY );

				// Lib�rer
				NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

				// Quitter
				return NULL;
			}
		}
		else
			break;

		// Attendre
		NLib_Temps_Attendre( 1 );
	} while( code > 0 );

	// V�rifier
	if( NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) == 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SOCKET_RECV );

		// D�truire le packet
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NULL;
	}

	// OK
	return packet;
}

// Envoyer un packet
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( const NPacket *packet,
	SOCKET socket )
{
	// Taille � envoyer
	NU32 tailleAEnvoyer;

	// Taille envoy�e
	NU32 tailleEnvoyee = 0;
	
	// Code send
	NS32 code;

	// Taille du packet
	NU32 taillePacket;

	// R�cup�rer la taille packet
	taillePacket = NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet );

	// Envoyer
	while( tailleEnvoyee < taillePacket )
	{
		// Calculer la taille � envoyer
		tailleAEnvoyer = ( ( taillePacket - tailleEnvoyee >= NBUFFER_PACKET ) ?
			NBUFFER_PACKET
			: taillePacket - tailleEnvoyee );

		// Envoyer
		if( ( code = send( socket,
			NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet ) + tailleEnvoyee,
			tailleAEnvoyer,
			0 ) ) == INVALID_SOCKET
			|| !code )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}

		// Ajouter
		tailleEnvoyee += code;
	}

	// OK
	return NTRUE;
}

#endif // NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
#endif // NLIB_MODULE_RESEAU

