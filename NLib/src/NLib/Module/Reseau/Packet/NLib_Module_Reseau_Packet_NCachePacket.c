#include "../../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------------------
// struct NLib::Module::Reseau::Packet::NCachePacket
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

/* Construire cache packet */
__ALLOC NCachePacket *NLib_Module_Reseau_Packet_NCachePacket_Construire( void )
{
	// Output
	__OUTPUT NCachePacket *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NCachePacket ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

// D�truire cache packet
void NLib_Module_Reseau_Packet_NCachePacket_Detruire( NCachePacket **this )
{
	// Supprimer tous les packets
	NLib_Module_Reseau_Packet_NCachePacket_Vider( *this );

	// Fermer mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Lib�rer la m�moire
	NFREE( (*this) );
}

void NLib_Module_Reseau_Packet_NCachePacket_Vider( NCachePacket *this )
{
	while( NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( this ) )
		NLib_Temps_Attendre( 1 );
}

// Obtenir le nombre de packets
NU32 NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( const NCachePacket *this )
{
	// R�sultat
	__OUTPUT NU32 out;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return 0;
	}

	// Copier le r�sultat
	out = this->m_nombrePacket;

	// Release le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return out;
}

// Obtenir le packet � envoyer
__ALLOC NPacket *NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( const NCachePacket *this )
{
	// NPacket
	__OUTPUT NPacket *packet;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return NULL;
	}

	// V�rifier le contenu du tableau de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );
		
		// Release mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NULL;
	}

	// Construire le packet
	if( !( packet = NLib_Module_Reseau_Packet_NPacket_Construire2( this->m_packet[ 0 ] ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Release le mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NULL;
	}

	// Release le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return packet;
}

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( const NCachePacket *this )
{
	return NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( this ) > 0;
}

// Ajouter un packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( NCachePacket *this,
	__WILLBEOWNED NPacket *packet )
{
	// Ancienne adresse
	NPacket **ancienCache;

	// Index ajout
	NU32 index;

	// It�rateur
	NU32 i;

	// Lock le mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Lib�rer
		NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

		// Quitter
		return NFALSE;
	}

	if( this->m_packet == NULL )
	{
		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( 1,
			sizeof( NPacket* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Lib�rer
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Release mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Ajout � la case 0
		index = 0;
	}
	else
	{
		// Enregistrer ancien cache
		ancienCache = this->m_packet;

		// Allouer le nouveau cache
		if( !( this->m_packet = calloc( this->m_nombrePacket + 1,
			sizeof( NPacket* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Lib�rer
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Copier les packets
		for( i = 0; i < this->m_nombrePacket; i++ )
			this->m_packet[ i ] = ancienCache[ i ];

		// Ajout � la fin
		index = this->m_nombrePacket;

		// Lib�rer ancien cache
		NFREE( ancienCache );
	}

	// Ajouter
	this->m_packet[ index ] = (NPacket*)packet;

	// Incr�menter le nombre de packets
	this->m_nombrePacket++;

	// Release le mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( NCachePacket *this,
	const NPacket *packet )
{
	// Packet � ajouter
	NPacket *copiePacket;

	// Construire la copie
	if( !( copiePacket = NLib_Module_Reseau_Packet_NPacket_Construire2( packet ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Ex�cuter l'ajout
	return NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( this,
		copiePacket );
}

// Supprimer le packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NCachePacket *this )
{
	// Ancien cache
	NPacket **ancienCache;

	// It�rateur
	NU32 i;

	// Lock mutex
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return NFALSE;
	}

	// V�rifier le nombre de packet
	if( this->m_nombrePacket == 0 )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Release mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Lib�rer la m�moire
	NLib_Module_Reseau_Packet_NPacket_Detruire( &this->m_packet[ 0 ] );

	// Si c'�tait le dernier packet
	if( this->m_nombrePacket == 1 )
	{
		// Lib�rer le conteneur
		NFREE( this->m_packet );
	}
	else
	{
		// Enregistrer l'ancien cache
		ancienCache = this->m_packet;

		// Allouer la m�moire
		if( !( this->m_packet = calloc( this->m_nombrePacket - 1,
			sizeof( NPacket* ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Restaurer
			this->m_packet = ancienCache;

			// Release mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quiter
			return NFALSE;
		}

		// Copier
		for( i = 1; i < this->m_nombrePacket; i++ )
			this->m_packet[ i - 1 ] = ancienCache[ i ];

		// Lib�rer
		NFREE( ancienCache );
	}

	// D�cr�menter le nombre de packet
	this->m_nombrePacket--;

	// Release mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_RESEAU

