#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------
// namespace NLib::Module
// ----------------------

/* Destruction de modules interne (priv�e) */
void NLib_Module_DetruireInterne( NEtapeModule etape )
{
	// D�truire
	for( ; (NS32)etape >= 0; etape-- )
		switch( etape )
		{
			case NETAPE_MODULE_RESEAU:
#ifdef NLIB_MODULE_RESEAU
				NLib_Module_Reseau_Detruire( );
#endif // NLIB_MODULE_RESEAU
				break;

			case NETAPE_MODULE_SDL:
#ifdef NLIB_MODULE_SDL
				NLib_Module_SDL_Detruire( );
#endif // NLIB_MODULE_SDL
				break;

			case NETAPE_MODULE_SDL_IMAGE:
#ifdef NLIB_MODULE_SDL_IMAGE
				NLib_Module_SDL_Image_Detruire( );
#endif // NLIB_MODULE_SDL_IMAGE
				break;

			case NETAPE_MODULE_SDL_TTF:
#ifdef NLIB_MODULE_SDL_TTF
				NLib_Module_SDL_TTF_Detruire( );
#endif // NLIB_MODULE_SDL_TTF
				break;

			case NETAPE_MODULE_REPERTOIRE:
#ifdef NLIB_MODULE_REPERTOIRE
				NLib_Module_Repertoire_Detruire( );
#endif // NLIB_MODULE_REPERTOIRE
				break;

			case NETAPE_MODULE_FMODEX:
#ifdef NLIB_MODULE_FMODEX
				NLib_Module_FModex_Detruire( );
#endif // NLIB_MODULE_FMODEX
				break;

			default:
				break;
		}
}

/* Initialiser les modules */
NBOOL NLib_Module_Initialiser( void )
{
	// Etape d'initialisation
	NEtapeModule etape = (NEtapeModule)0;

#define ERREUR( ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED ); \
 \
		/* D�truire les modules d�j� initialis�s */ \
		NLib_Module_DetruireInterne( etape - 1 ); \
 \
		/* Quitter */ \
		return NFALSE; \
	}

	// Initialiser
	for( ; etape < NETAPES_MODULE; etape++ )
		switch( etape )
		{
			case NETAPE_MODULE_RESEAU:
				// Initialiser le r�seau
#ifdef NLIB_MODULE_RESEAU
				if( !NLib_Module_Reseau_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_RESEAU
				break;

			case NETAPE_MODULE_SDL:
#ifdef NLIB_MODULE_SDL
				if( !NLib_Module_SDL_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_SDL
				break;

			case NETAPE_MODULE_SDL_IMAGE:
#ifdef NLIB_MODULE_SDL_IMAGE
				if( !NLib_Module_SDL_Image_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_SDL_IMAGE
				break;

			case NETAPE_MODULE_SDL_TTF:
#ifdef NLIB_MODULE_SDL_TTF
				if( !NLib_Module_SDL_TTF_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_SDL_TTF
				break;

			case NETAPE_MODULE_REPERTOIRE:
#ifdef NLIB_MODULE_REPERTOIRE
				if( !NLib_Module_Repertoire_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_REPERTOIRE
				break;

			case NETAPE_MODULE_FMODEX:
#ifdef NLIB_MODULE_FMODEX
				if( !NLib_Module_FModex_Initialiser( ) )
					ERREUR( );
#endif // NLIB_MODULE_FMODEX
				break;

			default:
				break;
		}

	// OK
	return NTRUE;
}

/* D�truire les modules */
void NLib_Module_Detruire( void )
{
	// D�truire
	NLib_Module_DetruireInterne( (NEtapeModule)( NETAPES_MODULE - 1 ) );	
}

