#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Module::FModex::NMusique
// -------------------------------------

#ifdef NLIB_MODULE_FMODEX
/* Construire */
__ALLOC NMusique *NLib_Module_FModex_NMusique_Construire( const char *lien,
	NU32 volume,
	NBOOL estRepete )
{
	// Sortie
	__OUTPUT NMusique *out;

	// V�rifier param�tre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NMusique ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// R�cup�rer le contexte
	out->m_contexte = NLib_Module_FModex_ObtenirContexte( );

	// Cr�er la musique
	if( FMOD_System_CreateSound( (FMOD_SYSTEM*)out->m_contexte,
		lien,
		FMOD_2D | FMOD_HARDWARE | FMOD_CREATESTREAM | ( estRepete ? FMOD_LOOP_NORMAL : 0 ),
		NULL,
		&out->m_musique ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Activer les r�p�titions
	if( FMOD_Sound_SetLoopCount( out->m_musique,
		NLIB_MODULE_FMODEX_REPETITION_INFINIE ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// D�truire le son
		FMOD_Sound_Release( out->m_musique );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Z�ro
	out->m_estLecture = NFALSE;
	out->m_positionPause = 0;

	// Enregistrer
	out->m_volume = volume;

	// OK
	return out;
}

/* D�truire */
void NLib_Module_FModex_NMusique_Detruire( NMusique **this )
{
	// Arr�ter la lecture
	if( (*this)->m_estLecture )
		FMOD_Channel_Stop( (*this)->m_canal );

	// D�truire le son
	FMOD_Sound_Release( (*this)->m_musique );

	// Lib�rer
	NFREE( (*this) );
}

/* Lire */
NBOOL NLib_Module_FModex_NMusique_Lire( NMusique *this )
{
	// V�rifier �tat
	if( this->m_estLecture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Update FMod
	NLib_Module_FModex_Update( );

	// Lire
	if( FMOD_System_PlaySound( (FMOD_SYSTEM*)this->m_contexte,
		FMOD_CHANNEL_FREE,
		this->m_musique,
		NTRUE,
		&this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// D�finir le volume
	if( FMOD_Channel_SetVolume( this->m_canal,
		(float)this->m_volume / 100.0f ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );
		
		// Quitter
		return NFALSE;
	}

	// D�finir la position de reprise
	if( FMOD_Channel_SetPosition( this->m_canal,
		this->m_positionPause,
		FMOD_TIMEUNIT_MS ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Restaurer le temps 0
		this->m_positionPause = 0;

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// Lancer la musique
	if( FMOD_Channel_SetPaused( this->m_canal,
		NFALSE ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( this->m_canal );

		// Quitter
		return NFALSE;
	}

	// On est en lecture
	return this->m_estLecture = NTRUE;
}

/* Pause */
NBOOL NLib_Module_FModex_NMusique_Pause( NMusique *this )
{
	// V�rifier �tat
	if( !this->m_estLecture )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// R�cup�rer le temps actuel
	if( FMOD_Channel_GetPosition( this->m_canal,
		&this->m_positionPause,
		FMOD_TIMEUNIT_MS ) != FMOD_OK )
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_FMODEX );

	// Stopper la lecture
	if( FMOD_Channel_Stop( this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// On est en pause
	return !( this->m_estLecture = NFALSE );
}

/* Arr�ter */
NBOOL NLib_Module_FModex_NMusique_Arreter( NMusique *this )
{
	// V�rifier
	if( !this->m_estLecture )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Z�ro
	this->m_positionPause = 0;

	// Stopper la lecture
	if( FMOD_Channel_Stop( this->m_canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// OK
	return !( this->m_estLecture = NFALSE );
}

/* Est en lecture? */
NBOOL NLib_Module_FModex_NMusique_EstLecture( const NMusique *this )
{
	return this->m_estLecture;
}

/* Est en pause? */
NBOOL NLib_Module_FModex_NMusique_EstPause( const NMusique *this )
{
	return !this->m_estLecture;
}

/* Changer le volume */
NBOOL NLib_Module_FModex_NMusique_DefinirVolume( NMusique *this,
	NU32 volume )
{
	// V�rifier param�tre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;

	}
	// Enregistrer
	this->m_volume = volume;

	// D�finir sur le canal si lecture en cours
	if( this->m_estLecture )
		return FMOD_Channel_SetVolume( this->m_canal,
			(float)volume / 100.0f ) == FMOD_OK;

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_FMODEX

