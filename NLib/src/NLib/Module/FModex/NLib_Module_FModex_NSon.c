#include "../../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Module::FModex::NSon
// ---------------------------------

#ifdef NLIB_MODULE_FMODEX
/* Construire */
__ALLOC NSon *NLib_Module_FModex_NSon_Construire( const char *lien,
	NU32 volume )
{
	// Sortie
	__OUTPUT NSon *out;

	// V�rifier param�tre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NSon ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// R�cup�rer le contexte
	out->m_contexte = NLib_Module_FModex_ObtenirContexte( );

	// Cr�er le son
	if( FMOD_System_CreateSound( (FMOD_SYSTEM*)out->m_contexte,
		lien,
		FMOD_2D | FMOD_HARDWARE | FMOD_CREATESAMPLE,
		NULL,
		&out->m_son ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_volume = volume;

	// OK
	return out;
}

/* D�truire */
void NLib_Module_FModex_NSon_Detruire( NSon **this )
{
	// D�truire le son
	FMOD_Sound_Release( (*this)->m_son );

	// Lib�rer
	NFREE( (*this) );
}

/* Lire */
NBOOL NLib_Module_FModex_NSon_Lire( NSon *this )
{
	// Canal de lecture
	FMOD_CHANNEL *canal;

	// Update FMod
	NLib_Module_FModex_Update( );

	// Lire
	if( FMOD_System_PlaySound( (FMOD_SYSTEM*)this->m_contexte,
		FMOD_CHANNEL_FREE,
		this->m_son,
		NTRUE,
		&canal ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Quitter
		return NFALSE;
	}

	// D�finir le volume
	if( FMOD_Channel_SetVolume( canal,
		(float)this->m_volume / 100.0f ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( canal );
		
		// Quitter
		return NFALSE;
	}

	// Lancer le son
	if( FMOD_Channel_SetPaused( canal,
		NFALSE ) != FMOD_OK )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );

		// Tuer le canal
		FMOD_Channel_Stop( canal );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Changer le volume */
NBOOL NLib_Module_FModex_NSon_DefinirVolume( NSon *this,
	NU32 volume )
{
	// V�rifier param�tre
	if( volume == 0
		|| volume > 100 )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;

	}
	// Enregistrer
	this->m_volume = volume;

	// OK
	return NTRUE;
}

#endif // NLIB_MODULE_FMODEX

