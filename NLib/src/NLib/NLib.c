#define NLIB_INTERNE
#include "../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------
// namespace NLib
// --------------

/* Initialiser NLib */
NBOOL NLib_Initialiser( void ( *callbackNotificationErreur )( const NErreur* ) )
{
#ifdef _DEBUG
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif // _DEBUG

	// Couleur console
	system( "color 0F" );

	// Donner graine pour NLib_Temps_ObtenirNombreAleatoire
	srand( (NU32)time( NULL ) );

	// Modifier le callback par d�faut au besoin
	if( callbackNotificationErreur != NULL )
		NLib_Erreur_Notification_ModifierCallbackFluxErreur( callbackNotificationErreur );

	// Initialiser les modules
	if( !NLib_Module_Initialiser( ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return NFALSE;
	}

	// OK
	return ( m_estInitialise = NTRUE );
}

/* D�truire NLib */
void NLib_Detruire( void )
{
	// Est initialis�?
	if( !m_estInitialise )
		return;

	// Notifier fermeture
	system( "title Fermeture..." );

	// D�truire les modules
	NLib_Module_Detruire( );

	// Plus initialis�
	m_estInitialise = NFALSE;
}

