#include "../../../../include/NLib/NLib.h"

// ----------------------------------
// struct NLib::Math::Graphe::NGraphe
// ----------------------------------

/* Construire */
__ALLOC NGraphe *NLib_Math_Graphe_NGraphe_Construire( const char *lienFichier,
	NTypeRepresentationGraphe typeRepresentation )
{
	// Sortie
	__OUTPUT NGraphe *out;

	// Allouer la mémoire
	if( !( out = calloc( 1,
		sizeof( NGraphe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le graphe
	if( !NLib_Math_Graphe_Representation_Construire( &out->m_representation,
		lienFichier,
		typeRepresentation ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Libérer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_typeRepresentation = typeRepresentation;

	// OK
	return out;
}

/* Détruire */
void NLib_Math_Graphe_NGraphe_Detruire( NGraphe **this )
{
	// Détruire le graphe
	NLib_Math_Graphe_Representation_NEnsembleRepresentationGraphe_Detruire( &(*this)->m_representation,
		(*this)->m_typeRepresentation );

	// Libérer
	NFREE( *this );
}

/* Obtenir type représentation */
NTypeRepresentationGraphe NLib_Math_Graphe_NGraphe_ObtenirTypeRepresentation( const NGraphe *this )
{
	return this->m_typeRepresentation;
}

/* Obtenir représentation (privée) */
char **NLib_Math_Graphe_NGraphe_ObtenirRepresentationInterne( const NGraphe *this )
{
	switch( this->m_typeRepresentation )
	{
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_MATRICE_ADJACENCE:
			return (char**)&this->m_representation.m_matriceAdjacence;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_FILE_SUCCESSEUR:
			return (char**)&this->m_representation.m_fileSuccesseur;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_SUCCESSEUR_PREDECESSEUR:
			return (char**)&this->m_representation.m_listeSuccesseurPredecesseur;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_ADJACENCE:
			return (char**)&this->m_representation.m_listeAdjacence;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_PRINCIPALE:
			return (char**)&this->m_representation.m_listePrincipale;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_MATRICE_CREUSE:
			return (char**)&this->m_representation.m_matriceCreuse;

		default:
			return NULL;
	}
}

/* Obtenir représentation */
const void *NLib_Math_Graphe_NGraphe_ObtenirRepresentation( const NGraphe *this )
{
	// Représentation
	__OUTPUT char **representation;

	// Obtenir représentation
	if( !( representation = NLib_Math_Graphe_NGraphe_ObtenirRepresentationInterne( this ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NULL;
	}

	// OK
	return *representation;
}

