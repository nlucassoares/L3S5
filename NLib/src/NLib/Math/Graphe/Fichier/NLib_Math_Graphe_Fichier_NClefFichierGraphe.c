#define NLIB_MATH_GRAPHE_FICHIER_NCLEFFICHIERGRAPHE_INTERNE
#include "../../../../../include/NLib/NLib.h"

// ----------------------------------------------------
// enum NLib::Math::Graphe::Fichier::NClefFichierGraphe
// ----------------------------------------------------

/* Obtenir la clef */
const char *NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NClefFichierGraphe clef )
{
	return NClefFichierGrapheTexte[ clef ];
}

