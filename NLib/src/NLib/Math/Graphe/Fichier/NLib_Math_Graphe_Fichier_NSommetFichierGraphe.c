#include "../../../../../include/NLib/NLib.h"

// --------------------------------------------------------
// struct NLib::Math::Graphe::Fichier::NSommetFichierGraphe
// --------------------------------------------------------

// Maccro lecture ligne
#define OBTENIR_LIGNE( ) ( ligne = NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( fichier, \
	'#' ) )

/* Construire */
__ALLOC NSommetFichierGraphe *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Construire( NFichierTexte *fichier,
	NU32 identifiant,
	NBOOL estValue )
{
	// Sortie
	__OUTPUT NSommetFichierGraphe *out;

	// Ligne
	char *ligne;

	// Clef
	char clef[ 128 ];

	// Curseur
	NU32 curseur = 0;

	// It�rateur
	NU32 i;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NSommetFichierGraphe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Lire sommet
		// Clef
			// Obtenir ligne
				if( !( ligne = OBTENIR_LIGNE( ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Composer clef
				snprintf( clef,
					128,
					"%s %d",
					NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_SOMMET ),
					identifiant );
			// V�rifier clef
				if( !NLib_Chaine_PlacerALaChaine( ligne,
					clef,
					&curseur ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( ligne );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lib�rer
				NFREE( ligne );
		// Lire successeurs
			// Z�ro
				curseur = 0;
			// Obtenir ligne
				if( !( ligne = OBTENIR_LIGNE( ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lire nombre successeurs
				if( !( out->m_nombreSuccesseur = NLib_Chaine_LireNombreNonSigne( ligne,
					&curseur,
					NFALSE ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( ligne );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lire clef
				if( !NLib_Chaine_PlacerALaChaine( ligne,
					NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_SUCCESSEUR ),
					&curseur ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( ligne );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Allouer la m�moire
				if( !( out->m_successeur = calloc( out->m_nombreSuccesseur,
					sizeof( NU32 ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( ligne );
					NFREE( out );

					// Quitter
					return NULL;
				}
				if( estValue )
					if( !( out->m_valeurSuccesseur = calloc( out->m_nombreSuccesseur,
						sizeof( NS32 ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Lib�rer
						NFREE( out->m_successeur );
						NFREE( ligne );
						NFREE( out );

						// Quitter
						return NULL;
					}
			// Lire successeurs
				for( i = 0; i < out->m_nombreSuccesseur; i++ )
					if( ( out->m_successeur[ i ] = NLib_Chaine_LireNombreNonSigne( ligne,
						&curseur,
						NFALSE ) ) == NERREUR )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Lib�rer
						NFREE( out->m_valeurSuccesseur );
						NFREE( out->m_successeur );
						NFREE( ligne );
						NFREE( out );

						// Quitter
						return NULL;
					}
			// Si valu�, lire valeurs ar�tes
				if( estValue )
					for( i = 0; i < out->m_nombreSuccesseur; i++ )
						out->m_valeurSuccesseur[ i ] = NLib_Chaine_LireNombreSigne( ligne,
							&curseur,
							NFALSE );
			// Lib�rer
				NFREE( ligne );
		// Lire pr�d�cesseurs
			// Z�ro
				curseur = 0;
			// Obtenir ligne
				if( !( ligne = OBTENIR_LIGNE( ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( out->m_valeurSuccesseur );
					NFREE( out->m_successeur );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lire nombre pr�d�cesseur
				if( !( out->m_nombrePredecesseur = NLib_Chaine_LireNombreNonSigne( ligne,
					&curseur,
					NFALSE ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( out->m_valeurSuccesseur );
					NFREE( ligne );
					NFREE( out->m_successeur );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lire clef
				if( !NLib_Chaine_PlacerALaChaine( ligne,
					NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_PREDECESSEUR ),
					&curseur ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Lib�rer
					NFREE( ligne );
					NFREE( out->m_valeurSuccesseur );
					NFREE( out->m_successeur );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Allouer la m�moire
				if( !( out->m_predecesseur = calloc( out->m_nombrePredecesseur,
					sizeof( NU32 ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					NFREE( ligne );
					NFREE( out->m_valeurSuccesseur );
					NFREE( out->m_successeur );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// Lire pr�d�cesseurs
				for( i = 0; i < out->m_nombrePredecesseur; i++ )
					if( ( out->m_predecesseur[ i ] = NLib_Chaine_LireNombreNonSigne( ligne,
						&curseur,
						NFALSE ) ) == NERREUR )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Lib�rer
						NFREE( out->m_predecesseur );
						NFREE( ligne );
						NFREE( out->m_valeurSuccesseur );
						NFREE( out->m_successeur );
						NFREE( out );

						// Quitter
						return NULL;
					}
			// Lib�rer
				NFREE( ligne );

	// Enregistrer
	out->m_identifiant = identifiant;

	// OK
	return out;
}

/* D�truire */
void NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Detruire( NSommetFichierGraphe **this )
{
	// Lib�rer
	NFREE( (*this)->m_successeur );
	NFREE( (*this)->m_valeurSuccesseur );
	NFREE( (*this)->m_predecesseur );
	NFREE( *this );
}

/* Obtenir identifiant */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirIdentifiant( const NSommetFichierGraphe *this )
{
	return this->m_identifiant;
}

/* Obtenir nombre pr�d�cesseurs */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombrePredecesseur( const NSommetFichierGraphe *this )
{
	return this->m_nombrePredecesseur;
}

/* Obtenir nombre successeurs */
NU32 NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombreSuccesseur( const NSommetFichierGraphe *this )
{
	return this->m_nombreSuccesseur;
}

/* Obtenir pr�decesseurs */
const NU32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirPredecesseur( const NSommetFichierGraphe *this )
{
	return this->m_predecesseur;
}

/* Obtenir successeurs */
const NU32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirSuccesseur( const NSommetFichierGraphe *this )
{
	return this->m_successeur;
}

const NS32 *NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirValeurSuccesseur( const NSommetFichierGraphe *this )
{
	return this->m_valeurSuccesseur;
}

