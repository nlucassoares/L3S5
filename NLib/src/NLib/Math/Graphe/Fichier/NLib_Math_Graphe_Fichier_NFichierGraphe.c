#include "../../../../../include/NLib/NLib.h"

// --------------------------------------------------
// struct NLib::Math::Graphe::Fichier::NFichierGraphe
// --------------------------------------------------

// Maccro lecture ligne
#define OBTENIR_LIGNE( ) ( ligne = NLib_Fichier_NFichierTexte_LireProchaineLigneSansCommentaire( fichier, \
	'#' ) )

/* Construire */
__ALLOC NFichierGraphe *NLib_Math_Graphe_Fichier_NFichierGraphe_Construire( const char *lien )
{
	// Sortie
	__OUTPUT NFichierGraphe *out;

	// Fichier
	NFichierTexte *fichier;

	// Curseur
	NU32 curseur = 0;

	// Ligne
	char *ligne;

	// Buffer
	char *buffer;

	// It�rateurs
	NU32 i,
		j;

	// Ouvrir fichier
	if( !( fichier = NLib_Fichier_NFichierTexte_ConstruireLecture( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NFichierGraphe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Nombre sommet
		// Lire ligne
			if( !OBTENIR_LIGNE( ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Lib�rer
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Lire clef
			if( !NLib_Chaine_PlacerALaChaine( ligne,
				NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_NOMBRE_SOMMET ),
				&curseur ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

				// Lib�rer
				NFREE( ligne );
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Lire nombre
			if( !( out->m_nombreSommet = NLib_Chaine_LireNombreNonSigne( ligne,
					&curseur,
					NFALSE ) )
				|| out->m_nombreSommet == NERREUR )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

				// Lib�rer
				NFREE( ligne );
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Lib�rer
			NFREE( ligne );

	// Est valu�?
		// Z�ro
			curseur = 0;
		// Lire ligne
			if( !OBTENIR_LIGNE( ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Lib�rer
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Lire clef
			if( !NLib_Chaine_PlacerALaChaine( ligne,
				NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_EST_VALUE ),
				&curseur ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

				// Lib�rer
				NFREE( ligne );
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Lire valeur
			if( !( buffer = NLib_Chaine_LireEntre2( ligne,
				'"',
				&curseur,
				NFALSE ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_KEY_NOT_FOUND );

				// Lib�rer
				NFREE( ligne );
				NFREE( out );

				// Fermer fichier
				NLib_Fichier_NFichierTexte_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Enregistrer
			out->m_estValue = !_strcmpi( buffer,
				NLib_Math_Graphe_Fichier_NClefFichierGraphe_ObtenirClef( NCLEF_FICHIER_GRAPHE_OUI ) );
		// Lib�rer
			NFREE( buffer );
			NFREE( ligne );

	// Allouer la m�moire
	if( !( out->m_sommet = calloc( out->m_nombreSommet,
		sizeof( NSommetFichierGraphe* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
		printf( "IL faut allouer %d\n", out->m_nombreSommet );
		// Lib�rer
		NFREE( out );

		// Fermer fichier
		NLib_Fichier_NFichierTexte_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Lire sommets
	for( i = 0; i < out->m_nombreSommet; i++ )
		// Lire
		if( !( out->m_sommet[ i ] = NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Construire( fichier,
			i + 1,
			out->m_estValue ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Detruire( &out->m_sommet[ j ] );
			NFREE( out->m_sommet );
			NFREE( out );
			
			// Fermer fichier
			NLib_Fichier_NFichierTexte_Detruire( &fichier );

			// Quitter
			return NULL;
		}

	// Fermer fichier
	NLib_Fichier_NFichierTexte_Detruire( &fichier );

	// OK
	return out;
}

/* D�truire */
void NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( NFichierGraphe **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire sommets
	for( ; i < (*this)->m_nombreSommet; i++ )
		NLib_Math_Graphe_Fichier_NSommetFichierGraphe_Detruire( &(*this)->m_sommet[ i ] );

	// Lib�rer
	NFREE( *this );
}

/* Est valu�? */
NBOOL NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( const NFichierGraphe *this )
{
	return this->m_estValue;
}

/* Obtenir nombre sommets */
NU32 NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( const NFichierGraphe *this )
{
	return this->m_nombreSommet;
}

/* Obtenir sommet */
const NSommetFichierGraphe *NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirSommet( const NFichierGraphe *this,
	NU32 sommet )
{
	return this->m_sommet[ sommet ];
}

