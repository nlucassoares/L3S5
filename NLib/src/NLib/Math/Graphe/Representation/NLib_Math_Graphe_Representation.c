#include "../../../../../include/NLib/NLib.h"

// --------------------------------------------
// namespace NLib::Math::Graphe::Representation
// --------------------------------------------

/* Construire */
NBOOL NLib_Math_Graphe_Representation_Construire( NEnsembleRepresentationGraphe *ensembleRepresentation,
	const char *lienFichier,
	NTypeRepresentationGraphe typeRepresentation )
{
	// Fichier
	NFichierGraphe *fichier;

	// Charger le fichier
	if( !( fichier = NLib_Math_Graphe_Fichier_NFichierGraphe_Construire( lienFichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire la repr�sentation
	switch( typeRepresentation )
	{
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_MATRICE_ADJACENCE:
			if( !( ensembleRepresentation->m_matriceAdjacence = NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Construire( fichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// D�truire fichier
				NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( &fichier );

				// Quitter
				return NFALSE;
			}
			break;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_FILE_SUCCESSEUR:
			if( !( ensembleRepresentation->m_fileSuccesseur = NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Construire( fichier ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// D�truire fichier
				NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( &fichier );

				// Quitter
				return NFALSE;
			}
			break;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_SUCCESSEUR_PREDECESSEUR:
			break;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_ADJACENCE:
			break;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_LISTE_PRINCIPALE:
			break;
		case NTYPE_REPRESENTATION_GRAPHE_ADJACENCE_MATRICE_CREUSE:
			break;
		
		default:
			// D�truire le fichier
			NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( &fichier );

			// Quitter
			return NFALSE;
	}

	// D�truire le fichier
	NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( &fichier );

	// OK
	return NTRUE;
}

