#include "../../../../../../include/NLib/NLib.h"

// -----------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NMatriceAdjacence
// -----------------------------------------------------------------------

/* Construire */
__ALLOC NMatriceAdjacence *NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Construire( const NFichierGraphe *fichier )
{
	// Sortie
	__OUTPUT NMatriceAdjacence *out = NULL;
	
	// It�rateurs
	NU32 i,
		j;

	// Sommet fichier
	const NSommetFichierGraphe *sommet;

	// Successeurs
	const NU32 *successeur;

	// Valeur
	const NS32 *valeurSuccesseur;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NMatriceAdjacence ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer la matrice
		// 1D
			if( !( out->m_matrice = calloc( NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ),
				sizeof( NU32* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// 2D
			for( i = 0; i < NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ); i++ )
				if( !( out->m_matrice[ i ] = calloc( NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ),
					sizeof( NU32 ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					for( j = 0; j < i; j++ )
						NFREE( out->m_matrice[ j ] );
					NFREE( out->m_matrice );
					NFREE( out );

					// Quitter
					return NULL;
				}

	// Si valu�
	if( NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( fichier ) )
	{
		// Allouer la m�moire
			// 1D
				if( !( out->m_valeur = calloc( NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ),
					sizeof( NS32* ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					for( i = 0; i < NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ); i++ )
						NFREE( out->m_matrice[ i ] );
					NFREE( out->m_matrice );
					NFREE( out );

					// Quitter
					return NULL;
				}
			// 2D
				for( i = 0; i < NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ); i++ )
					if( !( out->m_valeur[ i ] = calloc( NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ),
						sizeof( NS32 ) ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

						// Lib�rer
						for( j = 0; j < i; j++ )
							NFREE( out->m_valeur[ j ] );
						NFREE( out->m_valeur );
						for( i = 0; i < NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ); i++ )
							NFREE( out->m_matrice[ i ] );
						NFREE( out->m_matrice );
						NFREE( out );

						// Quitter
						return NULL;
					}
	}

	// Remplir matrice
	for( i = 0; i < NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier ); i++ )
	{
		// Obtenir sommet
		sommet = NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirSommet( fichier,
			i );

		// Obtenir successeurs
		successeur = NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirSuccesseur( sommet );
		valeurSuccesseur = NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirValeurSuccesseur( sommet );

		// Parcourir
		for( j = 0; j < NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombreSuccesseur( sommet ); j++ )
		{
			// Remplir matrice
			out->m_matrice[ i ][ successeur[ j ] - 1 ] = 1;

			// Remplir valeur
			if( NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( fichier ) )
				out->m_valeur[ i ][ successeur[ j ] - 1 ] = valeurSuccesseur[ j ];
		}
	}

	// Enregistrer
	out->m_nombreSommets = NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier );
	out->m_estValue = NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( fichier );

	// OK
	return out;
}

/* D�truire */
void NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_Detruire( NMatriceAdjacence **this )
{
	// It�rateur
	NU32 i;

	// Lib�rer
		// 2D
			for( i = 0; i < (*this)->m_nombreSommets; i++ )
			{
				// Matrice
				NFREE( (*this)->m_matrice[ i ] );

				// Est valu�?
				if( (*this)->m_estValue )
					NFREE( (*this)->m_valeur[ i ] );
			}
		// 1D
			NFREE( (*this)->m_matrice );
			NFREE( (*this)->m_valeur );

	// Lib�rer
	NFREE( *this );
}

/* Est orient�? */
NBOOL NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_EstOriente( const NMatriceAdjacence *this )
{
	// It�rateurs
	NU32 i,
		j;

	// Chercher si sym�trique
	for( i = 0; i < this->m_nombreSommets; i++ )
		for( j = 0; j < this->m_nombreSommets; j++ )
			// (i;j) == (j;i)?
			if( this->m_matrice[ i ][ j ] != this->m_matrice[ j ][ i ] )
				// Pas sym�trique, le graphe est donc orient�
				return NTRUE;

	// OK
	return NFALSE;
}

/* Est valu�? */
NBOOL NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_EstValue( const NMatriceAdjacence *this )
{
	return this->m_estValue;
}

/* Obtenir nombre sommets */
NU32 NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirNombreSommet( const NMatriceAdjacence *this )
{
	return this->m_nombreSommets;
}

/* Obtenir matrice */
const NU32 **NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirMatrice( const NMatriceAdjacence *this )
{
	return this->m_matrice;
}

/* Obtenir valeur */
const NS32 **NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence_ObtenirValeur( const NMatriceAdjacence *this )
{
	return this->m_valeur;
}

