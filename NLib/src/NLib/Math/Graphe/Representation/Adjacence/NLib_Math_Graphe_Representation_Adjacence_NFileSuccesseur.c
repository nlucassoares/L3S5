#include "../../../../../../include/NLib/NLib.h"

// ---------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NFileSuccesseur
// ---------------------------------------------------------------------

/* Construire */
__ALLOC NFileSuccesseur *NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Construire( const NFichierGraphe *fichier )
{
	// Sortie
	__OUTPUT NFileSuccesseur *out;

	// It�rateurs
	NU32 i,
		j;

	// Curseur
	NU32 curseur = 0;

	// Taille file successeur
	NU32 tailleFileSuccesseur = 0;

	// Sommet
	const NSommetFichierGraphe *sommet;
	const NU32 *successeur;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NFileSuccesseur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_nombreSommet = NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( fichier );

	// Calculer la taille de la file des successeurs
	for( i = 0; i < out->m_nombreSommet; i++ )
		tailleFileSuccesseur += NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombreSuccesseur( NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirSommet( fichier,
			i ) );

	// Allouer FS/APS
	if( !( out->m_fileSuccesseur = calloc( tailleFileSuccesseur,
			sizeof( NU32 ) ) )
		|| !( out->m_adressePremierSuccesseur = calloc( out->m_nombreSommet + 1,
				sizeof( NU32 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out->m_fileSuccesseur );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer valuation
	out->m_estValue = NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( fichier );

	// Si les ar�tes sont valu�es
	if( out->m_estValue )
		// Allouer la m�moire
		if( !( out->m_valeur = calloc( tailleFileSuccesseur,
			sizeof( NS32 ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Lib�rer
			NFREE( out->m_fileSuccesseur );
			NFREE( out->m_adressePremierSuccesseur );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Remplir
	for( i = 0; i < out->m_nombreSommet; i++ )
	{
		// Obtenir sommet
		sommet = NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirSommet( fichier,
			i );

		// Obtenir successeurs
		successeur = NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirSuccesseur( sommet );

		// Enregistrer indice
		out->m_adressePremierSuccesseur[ i ] = curseur;

		// Construire file
		for( j = 0; j < NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirNombreSuccesseur( sommet ); j++ )
		{
			// Enregistrer successeur
			out->m_fileSuccesseur[ curseur ] = successeur[ j ];

			// Si valu�
			if( out->m_estValue )
				out->m_valeur[ curseur ] = NLib_Math_Graphe_Fichier_NSommetFichierGraphe_ObtenirValeurSuccesseur( sommet )[ j ];

			// Incr�menter curseur
			curseur++;
		}
	}

	// Ajouter adresse virtuelle
	out->m_adressePremierSuccesseur[ i ] = curseur;

	// OK
	return out;
}

/* D�truire */
void NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_Detruire( NFileSuccesseur **this )
{
	// Lib�rer
	NFREE( (*this)->m_valeur );
	NFREE( (*this)->m_adressePremierSuccesseur );
	NFREE( (*this)->m_fileSuccesseur );
	NFREE( *this );
}

/* Obtenir successeurs */
const NU32 *NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_ObtenirSuccesseur( const NFileSuccesseur *this,
	NU32 sommet )
{
	return &this->m_fileSuccesseur[ this->m_adressePremierSuccesseur[ sommet ] ];
}

/* Obtenir nombre successeurs */
NU32 NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur_ObtenirNombreSuccesseur( const NFileSuccesseur *this,
	NU32 sommet )
{
	return this->m_adressePremierSuccesseur[ sommet + 1 ] - this->m_adressePremierSuccesseur[ sommet ];
}

