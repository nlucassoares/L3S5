#include "../../../../include/NLib/NLib.h"

// -------------------------------------------
// struct NLib::Math::Probabilite::NExperience
// -------------------------------------------

/* Construire (proba entre 0 et 100, avec total <= 100) */
__ALLOC NExperience *NLib_Math_Probabilite_NExperience_Construire( const NU32 *probabilite,
	NU32 nombreProbabilite )
{
	// Sortie
	__OUTPUT NExperience *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NExperience ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Allouer probabilit�
	if( !( out->m_probabilite = calloc( nombreProbabilite,
		sizeof( NU32 ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out->m_probabilite,
		probabilite,
		sizeof( NU32 ) * nombreProbabilite );

	// Enregistrer
	out->m_nombreEvenement = nombreProbabilite;

	// OK
	return out;
}

/* D�truire */
void NLib_Math_Probabilite_NExperience_Detruire( NExperience **this )
{
	// Lib�rer probabilit�s
	NFREE( (*this)->m_probabilite );

	// Lib�rer
	NFREE( *this );
}

/* Faire une exp�rience (retourne l'identifiant de l'�venement) */
NU32 NLib_Math_Probabilite_NExperience_Lancer( const NExperience *this )
{
	// It�rateur
	NU32 i = 0;

	// Valeur courante
	NU32 valeurCourante = 0;

	// Valeur al�atoire
	NU32 alea;

	// Tirer une valeur
	alea = NLib_Temps_ObtenirNombreAleatoire( )%101;

	// D�terminer l'�venement
	for( ; i < this->m_nombreEvenement; i++ )
	{
		// Ajouter valeur probabilit�
		valeurCourante += this->m_probabilite[ i ];

		// V�rifier
		if( alea <= valeurCourante )
			return i;
	}

	// Exp�rience sans r�sultat compris dans les bornes
	return NERREUR;
}

