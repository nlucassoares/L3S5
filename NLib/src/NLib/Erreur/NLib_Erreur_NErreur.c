#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// ----------------------------
// struct NLib::Erreur::NErreur
// ----------------------------

/* Copier les informations dans l'erreur (priv�e) */
NBOOL NLib_Erreur_NErreur_CopierTexteInterne( NErreur *this,
	const char *message,
	const char *fichier )
{
	// Tailles cha�nes
	NU32 tailleMessage,
		tailleFichier;

	// Allouer la m�moire
	if( !( this->m_message = (char*)calloc( ( tailleMessage = strlen( message ) ) + 1,
			sizeof( char ) ) )
		|| !( this->m_fichier = (char*)calloc( ( tailleFichier = strlen( fichier ) ) + 1,
			sizeof( char ) ) ) )
	{
		// Lib�rer
		NFREE( this->m_message );

		// Quitter
		return NFALSE;
	}

	// Copier donn�es
		// Message
			memcpy( this->m_message,
				message,
				tailleMessage );
		// Fichier
			memcpy( this->m_fichier,
				fichier,
				tailleFichier );

	// OK
	return NTRUE;
}

/* Construire l'objet */
__ALLOC NErreur *NLib_Erreur_NErreur_Construire( NCodeErreur code,
	const char *message,
	NU32 codeUtilisateur,
	const char *fichier,
	NU32 ligne,
	NNiveauErreur niveau )
{
	// Sortie
	__OUTPUT NErreur *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( NErreur ) ) ) )
	{
		// Notifier
		printf( "%s( )::Impossible d'allouer la memoire.\n",
			__FUNCTION__ );

		// Quitter
		return NULL;
	}

	// Copier
	out->m_code = code;
	out->m_ligne = ligne;
	out->m_codeUtilisateur = codeUtilisateur;
	out->m_niveau = niveau;

	// Copier texte
	NLib_Erreur_NErreur_CopierTexteInterne( out,
		message,
		fichier );

	// OK
	return out;
}

/* D�truire l'objet */
void NLib_Erreur_NErreur_Detruire( NErreur **this )
{
	// Lib�rer
	NFREE( (*this)->m_message );
	NFREE( (*this)->m_fichier );

	// D�truire
	NFREE( *this );
}

/* Obtention */
// Codes
NCodeErreur NLib_Erreur_NErreur_ObtenirCode( const NErreur *this )
{
	return this->m_code;
}

NU32 NLib_Erreur_NErreur_ObtenirCodeUtilisateur( const NErreur *this )
{
	return this->m_codeUtilisateur;
}

// Message
const char *NLib_Erreur_NErreur_ObtenirMessage( const NErreur *this )
{
	return this->m_message;
}

// Ligne
NU32 NLib_Erreur_NErreur_ObtenirLigne( const NErreur *this )
{
	return this->m_ligne;
}

// Fichier
const char *NLib_Erreur_NErreur_ObtenirFichier( const NErreur *this )
{
	return this->m_fichier;
}

// Niveau
NNiveauErreur NLib_Erreur_NErreur_ObtenirNiveau( const NErreur *this )
{
	return this->m_niveau;
}

