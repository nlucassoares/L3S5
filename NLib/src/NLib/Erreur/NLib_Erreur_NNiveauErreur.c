#define NLIB_ERREUR_NNIVEAUERREUR_INTERNE
#include "../../../include/NLib/NLib.h"

/*
	@author SOARES Lucas
*/

// --------------------------------
// enum NLib::Erreur::NNiveauErreur
// --------------------------------

/* Traduire */
const char *NLib_Erreur_NNiveauErreur_Traduire( NNiveauErreur niveau )
{
	return ( niveau >= NNIVEAUX_ERREUR ?
		NULL
		: NNiveauErreurTexte[ niveau ] );
}

