#ifndef NLIB_MATH_GRAPHE_NGRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_NGRAPHE_PROTECT

// ----------------------------------
// struct NLib::Math::Graphe::NGraphe
// ----------------------------------

typedef struct NGraphe
{
	// Type de représentation
	NTypeRepresentationGraphe m_typeRepresentation;

	// Représentation
	NEnsembleRepresentationGraphe m_representation;
} NGraphe;

/* Construire */
__ALLOC NGraphe *NLib_Math_Graphe_NGraphe_Construire( const char *lienFichier,
	NTypeRepresentationGraphe );

/* Détruire */
void NLib_Math_Graphe_NGraphe_Detruire( NGraphe** );

/* Obtenir type représentation */
NTypeRepresentationGraphe NLib_Math_Graphe_NGraphe_ObtenirTypeRepresentation( const NGraphe* );

/* Obtenir représentation */
const void *NLib_Math_Graphe_NGraphe_ObtenirRepresentation( const NGraphe* );

#endif // !NLIB_MATH_GRAPHE_NGRAPHE_PROTECT

