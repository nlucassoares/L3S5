#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEPRINCIPALE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEPRINCIPALE_PROTECT

// ----------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NListePrincipale
// ----------------------------------------------------------------------

typedef struct NListePrincipale
{
	int V;
} NListePrincipale;

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTEPRINCIPALE_PROTECT

