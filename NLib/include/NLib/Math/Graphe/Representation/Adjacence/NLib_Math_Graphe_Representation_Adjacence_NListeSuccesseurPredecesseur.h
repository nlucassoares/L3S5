#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTESUCCESSEURPREDECESSEUR_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTESUCCESSEURPREDECESSEUR_PROTECT

// ----------------------------------------------------------------------------------
// struct NLib::Math::Graphe::Representation::Adjacence::NListeSuccesseurPredecesseur
// ----------------------------------------------------------------------------------

typedef struct NListeSuccesseurPredecesseur
{
	int V;
} NListeSuccesseurPredecesseur;

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_NLISTESUCCESSEURPREDECESSEUR_PROTECT

