#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_PROTECT

// -------------------------------------------------------
// namespace NLib::Math::Graphe::Representation::Adjacence
// -------------------------------------------------------

// struct NLib::Math::Graphe::Representation::Adjacence::NMatriceAdjacence
#include "NLib_Math_Graphe_Representation_Adjacence_NMatriceAdjacence.h"

// struct NLib::Math::Graphe::Representation::Adjacence::NFileSuccesseur
#include "NLib_Math_Graphe_Representation_Adjacence_NFileSuccesseur.h"

// struct NLib::Math::Graphe::Representation::Adjacence::NListeSuccesseurPredecesseur
#include "NLib_Math_Graphe_Representation_Adjacence_NListeSuccesseurPredecesseur.h"

// struct NLib::Math::Graphe::Representation::Adjacence::NListeAdjacence
#include "NLib_Math_Graphe_Representation_Adjacence_NListeAdjacence.h"

// struct NLib::Math::Graphe::Representation::Adjacence::NListePrincipale
#include "NLib_Math_Graphe_Representation_Adjacence_NListePrincipale.h"

// struct NLib::Math::Graphe::Representation::Adjacence::NMatriceCreuse
#include "NLib_Math_Graphe_Representation_Adjacence_NMatriceCreuse.h"


#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_ADJACENCE_PROTECT

