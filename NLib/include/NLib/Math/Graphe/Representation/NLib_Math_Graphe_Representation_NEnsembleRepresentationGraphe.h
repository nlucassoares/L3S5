#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_NENSEMBLEREPRESENTATIONGRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_NENSEMBLEREPRESENTATIONGRAPHE_PROTECT

// -----------------------------------------------------------------------
// union NLib::Math::Graphe::Representation::NEnsembleRepresentationGraphe
// -----------------------------------------------------------------------

typedef union NEnsembleRepresentationGraphe
{
	NMatriceAdjacence *m_matriceAdjacence;
	NFileSuccesseur *m_fileSuccesseur;
	NListeSuccesseurPredecesseur *m_listeSuccesseurPredecesseur;
	NListeAdjacence *m_listeAdjacence;
	NListePrincipale *m_listePrincipale;
	NMatriceCreuse *m_matriceCreuse;
} NEnsembleRepresentationGraphe;

/* Destruction d'un graphe */
void NLib_Math_Graphe_Representation_NEnsembleRepresentationGraphe_Detruire( NEnsembleRepresentationGraphe*,
	NTypeRepresentationGraphe );

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_NENSEMBLEREPRESENTATIONGRAPHE_PROTECT

