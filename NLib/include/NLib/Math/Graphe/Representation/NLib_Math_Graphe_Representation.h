#ifndef NLIB_MATH_GRAPHE_REPRESENTATION_PROTECT
#define NLIB_MATH_GRAPHE_REPRESENTATION_PROTECT

// --------------------------------------------
// namespace NLib::Math::Graphe::Representation
// --------------------------------------------

// enum NLib::Math::Graphe::Representation::NTypeRepresentationGraphe
#include "NLib_Math_Graphe_Representation_NTypeRepresentationGraphe.h"

// namespace NLib::Math::Graphe::Representation::Adjacence
#include "Adjacence/NLib_Math_Graphe_Representation_Adjacence.h"

// ... INCIDENCE

// union NLib::Math::Graphe::Representation::NEnsembleRepresentationGraphe
#include "NLib_Math_Graphe_Representation_NEnsembleRepresentationGraphe.h"

/* Construire */
NBOOL NLib_Math_Graphe_Representation_Construire( NEnsembleRepresentationGraphe*,
	const char*,
	NTypeRepresentationGraphe );

#endif // !NLIB_MATH_GRAPHE_REPRESENTATION_PROTECT

