#ifndef NLIB_MATH_GRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_PROTECT

// ----------------------------
// namespace NLib::Math::Graphe
// ----------------------------

// namespace NLib::Math::Graphe::Fichier
#include "Fichier/NLib_Math_Graphe_Fichier.h"

// namespace NLib::Math::Graphe::Representation
#include "Representation/NLib_Math_Graphe_Representation.h"

// struct NLib::Math::Graphe::NGraphe
#include "NLib_Math_Graphe_NGraphe.h"

#endif // !NLIB_MATH_GRAPHE_PROTECT

