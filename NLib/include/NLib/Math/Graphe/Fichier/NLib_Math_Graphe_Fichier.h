#ifndef NLIB_MATH_GRAPHE_FICHIER_PROTECT
#define NLIB_MATH_GRAPHE_FICHIER_PROTECT

// -------------------------------------
// namespace NLib::Math::Graphe::Fichier
// -------------------------------------

// enum NLib::Math::Graphe::Fichier::NClefFichierGraphe
#include "NLib_Math_Graphe_Fichier_NClefFichierGraphe.h"

// struct NLib::Math::Graphe::Fichier::NSommetFichierGraphe
#include "NLib_Math_Graphe_Fichier_NSommetFichierGraphe.h"

// struct NLib::Math::Graphe::Fichier::NFichierGraphe
#include "NLib_Math_Graphe_Fichier_NFichierGraphe.h"

#endif // !NLIB_MATH_GRAPHE_FICHIER_PROTECT

