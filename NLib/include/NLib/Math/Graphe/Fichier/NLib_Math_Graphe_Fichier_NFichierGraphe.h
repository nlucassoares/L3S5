#ifndef NLIB_MATH_GRAPHE_FICHIER_NFICHIERGRAPHE_PROTECT
#define NLIB_MATH_GRAPHE_FICHIER_NFICHIERGRAPHE_PROTECT

// --------------------------------------------------
// struct NLib::Math::Graphe::Fichier::NFichierGraphe
// --------------------------------------------------

typedef struct NFichierGraphe
{
	// Nombre sommets
	NU32 m_nombreSommet;

	// Est valu�?
	NBOOL m_estValue;

	// Sommets
	NSommetFichierGraphe **m_sommet;
} NFichierGraphe;

/* Construire */
__ALLOC NFichierGraphe *NLib_Math_Graphe_Fichier_NFichierGraphe_Construire( const char* );

/* D�truire */
void NLib_Math_Graphe_Fichier_NFichierGraphe_Detruire( NFichierGraphe** );

/* Est valu�? */
NBOOL NLib_Math_Graphe_Fichier_NFichierGraphe_EstValue( const NFichierGraphe* );

/* Obtenir nombre sommets */
NU32 NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirNombreSommet( const NFichierGraphe* );

/* Obtenir sommet */
const NSommetFichierGraphe *NLib_Math_Graphe_Fichier_NFichierGraphe_ObtenirSommet( const NFichierGraphe*,
	NU32 );

#endif // !NLIB_MATH_GRAPHE_FICHIER_NFICHIERGRAPHE_PROTECT

