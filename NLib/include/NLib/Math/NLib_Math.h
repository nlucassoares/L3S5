#ifndef NLIB_MATH_PROTECT
#define NLIB_MATH_PROTECT

/*
	Ensemble de fonctions mathématiques

	@author SOARES Lucas
*/

// --------------------
// namespace NLib::Math
// --------------------

// namespace NLib::Math::Geometrie
#include "Geometrie/NLib_Math_Geometrie.h"

// namespace NLib::Math::Probabilite
#include "Probabilite/NLib_Math_Probabilite.h"

// namespace NLib::Math::Graphe
#include "Graphe/NLib_Math_Graphe.h"

/* Absolue */
NS32 NLib_Math_Abs( NS32 );

/* Minimum */
NS32 NLib_Math_Minimum( NS32 x,
	NS32 y );
NU32 NLib_Math_Minimum2( NU32 x,
	NU32 y );

/* Maximum */
NS32 NLib_Math_Maximum( NS32 x,
	NS32 y );
NU32 NLib_Math_Maximum2( NU32 x,
	NU32 y );

/* Obtenir nombre digit */
NU32 NLib_Math_ObtenirNombreDigit( NU32 );

#endif // !NLIB_MATH_PROTECT

