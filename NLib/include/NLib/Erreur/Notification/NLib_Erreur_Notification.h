#ifndef NLIB_ERREUR_NOTIFICATION_PROTECT
#define NLIB_ERREUR_NOTIFICATION_PROTECT

/*
	Notification d'erreur aupr�s de NLib

	@author SOARES Lucas
*/

// ------------------------------------
// namespace NLib::Erreur::Notification
// ------------------------------------

// On veut afficher les avertissements?
//#define NLIB_ERREUR_NOTIFICATION_AFFICHER_AVERTISSEMENTS

/* Notifier une erreur */
void NLib_Erreur_Notification_Notifier( __WILLBEOWNED NErreur* );

/* Modifier callback flux erreur */
NBOOL NLib_Erreur_Notification_ModifierCallbackFluxErreur( __CALLBACK void ( *callbackFluxSortie )( const NErreur* ) );

/* Restaurer callback flux erreur initial */
void NLib_Erreur_Notification_RestaurerCallbackFluxErreurInitial( void );

/* Flux standard */
void NLib_Erreur_Notification_FluxStandard( const NErreur *erreur );

#ifdef NLIB_ERREUR_NOTIFICATION_INTERNE
/* Flux actif */
__CALLBACK static void ( *m_fluxActif )( const NErreur* ) = NLib_Erreur_Notification_FluxStandard;
#endif // NLIB_ERREUR_NOTIFICATION_INTERNE

// D�clarer une erreur/avertissement
// Simple
#ifdef NLIB_ERREUR_NOTIFICATION_DEBOGUER_LOG
#define NOTIFIER_ERREUR( code ) \
	NLib_Erreur_Notification_Notifier( \
		NLib_Erreur_NErreur_Construire( code, \
			__FUNCTION__, \
			0, \
			__FILE__, \
			__LINE__, \
			NNIVEAU_ERREUR_ERREUR ) )

#define NOTIFIER_AVERTISSEMENT( code ) \
	NLib_Erreur_Notification_Notifier( \
		NLib_Erreur_NErreur_Construire( code, \
			__FUNCTION__, \
			0, \
			__FILE__, \
			__LINE__, \
			NNIVEAU_ERREUR_AVERTISSEMENT ) )

// Cr��e par l'utilisateur
#define NOTIFIER_ERREUR_UTILISATEUR( code, message, codeUtilisateur ) \
	NLib_Erreur_Notification_Notifier( \
		NLib_Erreur_NErreur_Construire( code, \
			message, \
			codeUtilisateur, \
			__FILE__, \
			__LINE__, \
			NNIVEAU_ERREUR_ERREUR ) )

#define NOTIFIER_AVERTISSEMENT_UTILISATEUR( code, message, codeUtilisateur ) \
	NLib_Erreur_Notification_Notifier( \
		NLib_Erreur_NErreur_Construire( code, \
			message, \
			codeUtilisateur, \
			__FILE__, \
			__LINE__, \
			NNIVEAU_ERREUR_AVERTISSEMENT ) )

#else // NLIB_ERREUR_NOTIFICATION_DEBOGUER_LOG
// Simple
#define NOTIFIER_ERREUR( code ) \
	;

#define NOTIFIER_AVERTISSEMENT( code ) \
	;

// Cr��e par l'utilisateur
#define NOTIFIER_ERREUR_UTILISATEUR( code, message, codeUtilisateur ) \
	;

#define NOTIFIER_AVERTISSEMENT_UTILISATEUR( code, message, codeUtilisateur ) \
	;
#endif // !NLIB_ERREUR_NOTIFICATION_DEBOGUER_LOG

#endif // !NLIB_ERREUR_NOTIFICATION_PROTECT

