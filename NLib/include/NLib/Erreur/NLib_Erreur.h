#ifndef NLIB_ERREUR_PROTECT
#define NLIB_ERREUR_PROTECT

/*
	Espace de nom pour la gestion des erreurs via NLib

	@author SOARES Lucas
*/

// ----------------------
// namespace NLib::Erreur
// ----------------------

// enum NLib::Erreur::NCodeErreur
#include "NLib_Erreur_NCodeErreur.h"

// enum NLib::Erreur::NNiveauErreur
#include "NLib_Erreur_NNiveauErreur.h"

// struct NLib::Erreur::NErreur
#include "NLib_Erreur_NErreur.h"

// Erreur standard
#define NERREUR		(NU32)~0

// namespace NLib::Erreur::Notification
#include "Notification/NLib_Erreur_Notification.h"

#endif // !NLIB_ERREUR_PROTECT

