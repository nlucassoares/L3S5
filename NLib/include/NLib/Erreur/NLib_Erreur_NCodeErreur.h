#ifndef NLIB_ERREUR_NCODEERREUR_PROTECT
#define NLIB_ERREUR_NCODEERREUR_PROTECT

/*
	Code d'erreur pour NLib

	@author SOARES Lucas
*/

// ------------------------------
// enum NLib::Erreur::NCodeErreur
// ------------------------------

// Code NErreur
typedef enum
{
	// Pas d'erreur
	NERREUR_AUCUNE,

	// D�bogage
	NERREUR_DEBOGAGE,

	// Echec allocation
	NERREUR_ALLOCATION_FAILED,

	// Echec du constructeur
	NERREUR_CONSTRUCTOR_FAILED,

	// Erreur utilisateur (cr��e manuellement par le programme externe � NLib) --> Utilise code utilisateur
	NERREUR_USER,

	// NULL pointeur
	NERREUR_NULL_POINTER,

	// Erreur param�tre
	NERREUR_PARAMETER_ERROR,

	// Out of memory
	NERREUR_OUT_MEMORY,

	// Echec initialisation
	NERREUR_INIT_FAILED,

	// Timeout
	NERREUR_TIMEOUT,

	// File not found
	NERREUR_FILE_NOT_FOUND,

	// File already exists
	NERREUR_FILE_ALREADY_EXISTS,
			
	// File cannot be opened
	NERREUR_FILE_CANNOT_BE_OPENED,

	// Ne peut pas remplacer le fichier
	NERREUR_FILE_CANNOT_BE_OVERRIDE,

	// Ne peut pas �crire dans le fichier
	NERREUR_FILE_CANT_WRITE,

	// Ne peut pas lire dans le fichier
	NERREUR_FILE_CANT_READ,

	// Fichier vide
	NERREUR_FILE_EMPTY,

	// Clef introuvable fichier
	NERREUR_FILE_KEY_NOT_FOUND,

	// Erreur CRC
	NERREUR_CRC,

	// File (non pr�cis�)
	NERREUR_FILE,

	// Erreur flux
	NERREUR_STREAM_ERROR,

	// D�j� initialis�
	NERREUR_ALREADY_INITIALIZED,

	// Initialisation incompl�te
	NERREUR_INIT_UNCOMPLETE,

	// SDL
	NERREUR_SDL,

	// SDL_IMG
	NERREUR_SDL_IMAGE,

	// SDL_TTF
	NERREUR_SDL_TTF,

	// WinSock
	NERREUR_WINSOCK,

	// Socket
	NERREUR_SOCKET,

	// Socket surcharg�e
	NERREUR_SOCKET_OVERLOADED,

	// Connexion socket �chou�e
	NERREUR_SOCKET_CONNECTION_FAILED,

	// Accept socket �chou�e
	NERREUR_SOCKET_ACCEPT_FAILED,

	// Recv failed
	NERREUR_SOCKET_RECV,

	// Send failed
	NERREUR_SOCKET_SEND,

	// Impossible de se connecter
	NERREUR_UNABLE_TO_CONNECT,

	// PortAudio
	NERREUR_PORTAUDIO,

	// Obtention r�pertoire
	NERREUR_GET_CURRENT_DIRECTORY_FAILED,

	// Obtention �l�ment r�pertoire
	NERREUR_GET_ITEM_REPERTORY_FAILED,

	// Erreur d'�tat objet
	NERREUR_OBJECT_STATE,

	// Pas de fichiers dans le r�pertoire
	NERREUR_NO_FILE_REPERTORY,

	// Taille incorrecte
	NERREUR_INCORRECT_SIZE,

	// Archive vide
	NERREUR_EMPTY_ARCHIVE,

	// Date incorrecte
	NERREUR_INCORRECT_DATE,

	// Packet inconnu
	NERREUR_UNKNOWN_PACKET,

	// Echec login
	NERREUR_LOGIN_FAILED,

	// Echec logout
	NERREUR_LOGOUT_FAILED,

	// Echec export
	NERREUR_EXPORT_FAILED,

	// Erreur thread
	NERREUR_THREAD,

	// Erreur mutex
	NERREUR_MUTEX,

	// Erreur lock mutex
	NERREUR_LOCK_MUTEX,

	// Erreur m�moire
	NERREUR_MEMORY,

	// Serveur ferm�
	NERREUR_SERVER_CLOSED,

	// Socket bind
	NERREUR_SOCKET_BIND,

	// Socket listen
	NERREUR_SOCKET_LISTEN,

	// FMod
	NERREUR_FMODEX,

	// R�pertoire
	NERREUR_DIRECTORY,

	// Erreur syntaxe
	NERREUR_SYNTAX,

	// Erreur inconnue
	NERREUR_UNKNOWN,

	// Total...
	NERREURS
} NCodeErreur;

/* Obtention texte erreurs */
const char *NLib_Erreur_NCodeErreur_Traduire( NCodeErreur );

/* Savoir si un code utilise le code utilisateur */
NBOOL NLib_Erreur_NCodeErreur_EstUtilisateurCodeUtilisateur( NCodeErreur );

// Cha�ne erreurs
#ifdef NLIB_ERREUR_NCODEERREUR_INTERNE
static const char NCodeErreurTexte[ NERREURS ][ 32 ] =
{
	// Pas d'erreur
	"OK",

	// Erreur volontaire pour d�bogage
	"DBG",

	// Echec allocation
	"Echec allocation",

	// Echec du constructeur
	"Echec constructeur",

	// Erreur utilisateur (cr��e manuellement par le programme externe � NLib)
	"Erreur utilisateur",

	// NULL pointeur
	"Pointeur NULL",

	// Erreur param�tre
	"Parametre(s) incorrect(s)",

	// Out of memory
	"Out of memory",

	// Echec initialisation
	"Echec initialisation",

	// Timeout
	"Temps ecoule",

	// File not found
	"Fichier introuvable",

	// File already exists
	"Fichier existe deja",

	// File cannot be opened
	"Echec ouverture fichier",

	// Ne peut pas remplacer le fichier
	"Fichier protege",

	// Impossible d'�crire dans le fichier
	"Echec ecriture fichier",

	// Impossible de lire dans le fichier
	"Echec lecture fichier",

	// Fichier vide
	"Fichier vide",

	// Fichier clef introuvable
	"Clef introuvable fichier",

	// Erreur CRC
	"Erreur CRC",

	// File (non pr�cis�)
	"Erreur avec un fichier",

	// Erreur avec le flux
	"Erreur flux",

	// D�j� initialis�
	"Deja initialise",

	// Initialisation incompl�te
	"Initialisation incomplete",

	// SDL
	"SDL",

	// SDL Image
	"SDL_Image",

	// SDL TTF
	"SDL_TTF",

	// WinSock
	"Winsock",

	// Socket
	"Socket",

	// Socket surcharg�e
	"Socket surchargee",

	// Connexion socket �chou�e
	"Connexion socket echouee",

	// accept a �chou�
	"accept( ) echoue",

	// recv a �chou�
	"recv( ) echoue",

	// send a �chou�
	"send( ) echoue",

	// Impossible de se connecter
	"Impossible de se connecter",

	// PortAudio
	"PortAudio",

	// Erreur obtention r�pertoire courant
	"Echec obtention repertoire",

	// Erreur obtention �l�ment r�pertoire
	"Erreur obtention elt rep",

	// Erreur d'�tat objet
	"Erreur etat objet",

	// Erreur pas de fichier dans le r�pertoire
	"Erreur aucun fichier rep",

	// Taille incorrecte
	"Taille incorrecte",

	// Archive vide
	"Archive vide",
	
	// Date incorrecte
	"Date incorrecte",

	// Packet inconnu
	"Packet inconnu",

	// Echec login
	"Echec login",

	// Echec logout
	"Echec logout",

	// Echec export
	"Echec export",

	// Erreur thread
	"Erreur thread",

	// Erreur mutex
	"Erreur mutex",

	// Erreur lock mutex
	"Erreur lock mutex",

	// Erreur m�moire
	"Erreur memoire",

	// Serveur ferm�
	"Serveur ferme",

	// Bind socket
	"Erreur bind socket",

	// Listen socket
	"Erreur listen socket",

	// Erreur FModex
	"Erreur FModex",

	// Erreur r�pertoire
	"Erreur repertoire",

	// Erreur de syntaxe
	"Erreur de syntaxe",

	// Erreur inconnue
	"Erreur inconnue"
};
#endif // NLIB_ERREUR_NCODEERREUR_INTERNE

#endif // !NLIB_ERREUR_NCODEERREUR_PROTECT

