#ifndef NLIB_PROTECT
#define NLIB_PROTECT

/*
	Librairie NLib fournissant un support complet sur tous les
	besoins �l�mentaires avec une gestion:
		- Des caract�res
		- Des cha�nes
		- Des erreurs
		- De la m�moire
		- Des fichiers
		
	Fournit �galement une partie "module", activable ou d�sacti
	vable selon le projet via des d�clarations comportant:
		- Module SDL [GUI]
			=> Module SDL_Image
			=> Module SDL_TTF
		- Module FModex [GUI]
		- Module R�pertoire
			=> Liste des fichiers dans les r�pertoires
		- Module r�seau
			=> Packets
			=> Serveur (avec gestion des clients pour envoi/re
			ception packet)
			=> Client (avec gestion envoi/reception packet)

	Linkage automatique des librairies sous windows

	@author SOARES Lucas
*/

// CRT NO WARNINGS
#define _CRT_SECURE_NO_WARNINGS // Windows

// _CRT_RAND_S
#define _CRT_RAND_S	// Activer rand_s

// Standard headers
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <time.h>

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif // _DEBUG

#include <stdlib.h>


// D�finition pr�processeur
#include "Preprocesseur/NLib_Preprocesseur.h"

// Warnings en moins
#ifdef IS_WINDOWS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma warning( disable:4706 ) // assignation au sein d'une expression conditionnelle
#pragma warning( disable:4054 ) // 'cast de type'�: du pointeur fonction 'void (__cdecl *const )(const NErreur *)' vers le pointeur donn�e 'void *'
#endif

// namespace NLib::Type
#include "Type/NLib_Type.h"

// namespace NLib::Erreur
#include "Erreur/NLib_Erreur.h"

// namespace NLib::Caractere
#include "Caractere/NLib_Caractere.h"

// namespace NLib::Memoire
#include "Memoire/NLib_Memoire.h"

// namespace NLib::Temps
#include "Temps/NLib_Temps.h"

// namespace NLib::Fichier
#include "Fichier/NLib_Fichier.h"

// namespace NLib::Chaine
#include "Chaine/NLib_Chaine.h"

// namespace NLib::Math
#include "Math/NLib_Math.h"

// namespace NLib::Mutex
#include "Mutex/NLib_Mutex.h"

// namespace NLib::Module
#include "Module/NLib_Module.h"

// --------------
// namespace NLib
// --------------

/* Initialiser NLib */
NBOOL NLib_Initialiser( void ( *callbackNotificationErreur )( const NErreur* ) );

/* D�truire NLib */
void NLib_Detruire( void );

#ifdef NLIB_INTERNE
static NBOOL m_estInitialise = (NBOOL)0;
#endif // NLIB_INTERNE

#endif // !NLIB_PROTECT

