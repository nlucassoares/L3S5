#ifndef NLIB_TYPE_POINT_PROTECT
#define NLIB_TYPE_POINT_PROTECT

/*
	Définition des types NSPoint/NUPoint

	@author SOARES Lucas
*/

// ---------------------------
// namespace NLib::Type::Point
// ---------------------------

// struct NLib::Type::Point::NUPoint
#include "NLib_Type_Point_NUPoint.h"

// struct NLib::Type::Point::NSPoint
#include "NLib_Type_Point_NSPoint.h"

#endif // !NLIB_TYPE_POINT_PROTECT

