#ifndef NLIB_TYPE_POINT_NSPOINT_PROTECT
#define NLIB_TYPE_POINT_NSPOINT_PROTECT

/*
	Un point sign�

	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Type::Point::NSPoint
// ---------------------------------

typedef struct
{
	// Coordonn�e x
	NS32 x;

	// Coordonn�e y
	NS32 y;
} NSPoint;

#endif // !NLIB_TYPE_POINT_NSPOINT_PROTECT

