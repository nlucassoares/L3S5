#ifndef NLIB_TYPE_POINT_NUPOINT_PROTECT
#define NLIB_TYPE_POINT_NUPOINT_PROTECT

/*
	Un point non sign�

	@author SOARES Lucas
*/

// ---------------------------------
// struct NLib::Type::Point::NUPoint
// ---------------------------------

typedef struct
{
	// Coordonn�e x
	NU32 x;

	// Coordonn�e y
	NU32 y;
} NUPoint;

#endif // !NLIB_TYPE_POINT_NUPOINT_PROTECT

