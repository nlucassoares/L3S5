#ifndef NLIB_TYPE_RECT_NURECT_PROTECT
#define NLIB_TYPE_RECT_NURECT_PROTECT

/*
	Un rectangle � la position non sign�e

	@author SOARES Lucas
*/

// -------------------------------
// struct NLib::Type::Rect::NURect
// -------------------------------

typedef struct
{
	// Position
	NUPoint m_position;

	// Taille
	NUPoint m_taille;
} NURect;


#endif // !NLIB_TYPE_RECT_NURECT_PROTECT

