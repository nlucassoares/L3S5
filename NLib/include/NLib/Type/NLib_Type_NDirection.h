#ifndef NLIB_TYPE_NDIRECTION_PROTECT
#define NLIB_TYPE_NDIRECTION_PROTECT

/*
	Liste de directions

	@author SOARES Lucas
*/

// ---------------------------
// enum NLib::Type::NDirection
// ---------------------------

typedef enum
{
	NHAUT,
	NBAS,
	NGAUCHE,
	NDROITE,

	NDIRECTIONS
} NDirection;

// Assert taille
nassert( sizeof( NDirection ) == sizeof( NU32 ),
	"NLib::Type::NDirection" );

#endif // !NLIB_TYPE_NDIRECTION_PROTECT

