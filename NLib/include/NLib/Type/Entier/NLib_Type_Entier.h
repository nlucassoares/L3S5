#ifndef NLIB_TYPE_ENTIER_PROTECT
#define NLIB_TYPE_ENTIER_PROTECT

/*
	D�finitions des types entiers

	@author SOARES Lucas
*/

// ----------------------------
// namespace NLib::Type::Entier
// ----------------------------

// NU64
typedef unsigned long long int NU64;

// NS64
typedef signed long long int NS64;

// NU32
typedef unsigned int NU32;

// NU16
typedef unsigned short NU16;

// NU8
typedef unsigned char NU8;

// NS32
typedef signed int NS32;

// NS16
typedef signed short NS16;

// NS8
typedef signed char NS8;

// V�rifier la taille
nassert( ( sizeof( NU64 ) == 8
		&& sizeof( NS64 ) == 8
		&& sizeof( NU32 ) == 4
		&& sizeof( NS32 ) == 4
		&& sizeof( NU16 ) == 2
		&& sizeof( NS16 ) == 2
		&& sizeof( NU8 ) == 1
		&& sizeof( NS8 ) == 1 ),
	"NLib_Type_Entier" );

#endif // !NLIB_TYPE_ENTIER_PROTECT

