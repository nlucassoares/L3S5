#ifndef NLIB_TYPE_PROTECT
#define NLIB_TYPE_PROTECT

/*
	D�finition des types de base n�cessaires �
	NLib

	@author SOARES Lucas
*/

// --------------------
// namespace NLib::Type
// --------------------

// type NBOOL
#include "NLib_Type_NBool.h"

// namespace NLib::Type::Entier
#include "Entier/NLib_Type_Entier.h"

// namespace NLib::Type::Point
#include "Point/NLib_Type_Point.h"

// namespace NLib::Type::Rect
#include "Rect/NLib_Type_Rect.h"

// struct NLib::Type::NCouleur
#include "NLib_Type_NCouleur.h"

// enum NLib::Type::NDirection
#include "NLib_Type_NDirection.h"

#endif // !NLIB_TYPE_PROTECT

