#ifndef NLIB_TYPE_NCOULEUR_PROTECT
#define NLIB_TYPE_NCOULEUR_PROTECT

/*
	Définition d'une couleur, prenant en compte
	l'alpha

	@author SOARES Lucas
*/

// ---------------------------
// struct NLib::Type::NCouleur
// ---------------------------

typedef struct
{
	NU8 r,
		g,
		b,
		a;
} NCouleur;

#endif // !NLIB_TYPE_NCOULEUR_PROTECT

