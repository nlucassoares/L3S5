#ifndef NLIB_TYPE_NBOOL
#define NLIB_TYPE_NBOOL

/*
	Type bool�en (NBOOL/NTRUE/NFALSE)

	@author SOARES Lucas
*/

// NBOOL type
typedef unsigned int NBOOL;

// Boolean constant
#define NFALSE	(NBOOL)0
#define NTRUE	(NBOOL)1

#endif // !BOOLEAN_PROTECT

