#ifndef NLIB_TEMPS_PROTECT
#define NLIB_TEMPS_PROTECT

/*
	Gestion du temps

	@author SOARES Lucas
*/

// ---------------------
// namespace NLib::Temps
// ---------------------

// namespace NLib::Temps::Animation
#include "Animation/NLib_Temps_Animation.h"

/* Obtenir le nombre de ms depuis le lancement */
NU32 NLib_Temps_ObtenirTick( void );
NU64 NLib_Temps_ObtenirTick64( void );

/* Attendre (ms) */
void NLib_Temps_Attendre( NU32 );

/* Obtenir nombre al�atoire */
NU32 NLib_Temps_ObtenirNombreAleatoire( void );

#endif // !NLIB_TEMPS_PROTECT

