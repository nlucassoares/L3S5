#ifndef NLIB_FICHIER_PROTECT
#define NLIB_FICHIER_PROTECT

/*
	Déclaration d'opérations sur les fichiers

	@author SOARES Lucas
*/

// -----------------------
// namespace NLib::Fichier
// -----------------------

// Code erreur fichier
static const NU32 NFICHIER_CODE_ERREUR = NERREUR;

// namespace NLib::Fichier::Operation
#include "Operation/NLib_Fichier_Operation.h"

// struct NLib::Fichier::NFichierTexte
#include "NLib_Fichier_NFichierTexte.h"

// struct NLib::Fichier::NFichierBinaire
#include "NLib_Fichier_NFichierBinaire.h"

// namespace NLib::Fichier::Clef
#include "Clef/NLib_Fichier_Clef.h"

#endif // !NLIB_FICHIER_PROTECT

