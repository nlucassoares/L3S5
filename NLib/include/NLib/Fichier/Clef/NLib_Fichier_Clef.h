#ifndef NLIB_FICHIER_CLEF_PROTECT
#define NLIB_FICHIER_CLEF_PROTECT

/*
	D�finition des �l�ments n�cessaires pour le
	chargement d'un fichier compos�s de clef/valeurs
*/

// -----------------------------
// namespace NLib::Fichier::Clef
// -----------------------------

// struct NLib::Fichier::Clef::NFichierClef
#include "NLib_Fichier_Clef_NFichierClef.h"

#endif // !NLIB_FICHIER_CLEF_PROTECT

