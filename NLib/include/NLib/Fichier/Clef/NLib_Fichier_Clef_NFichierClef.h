#ifndef NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT
#define NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT

/*
	Expression d'un fichier compos� de clefs/valeurs

	L'ordre des clefs dans le tableau transmis corre
	spond � l'ordre de stockage dans cette structure
	Il s'agira donc de donner les m�mes identifiants
	de clefs pour r�cup�rer les valeurs.

	@author SOARES Lucas
*/

// ----------------------------------------
// struct NLib::Fichier::Clef::NFichierClef
// ----------------------------------------

typedef struct
{
	// Nombre clefs
	NU32 m_nombreClefs;

	// Valeurs
	char **m_valeurs;
} NFichierClef;

/* Construire */
__ALLOC NFichierClef *NLib_Fichier_Clef_NFichierClef_Construire( const char *lien,
	const char **clefs,
	NU32 nombreClefs );

/* D�truire */
void NLib_Fichier_Clef_NFichierClef_Detruire( NFichierClef** );

/* Obtenir valeur */
const char *NLib_Fichier_Clef_NFichierClef_ObtenirValeur( const NFichierClef*,
	NU32 clef );
NU32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( const NFichierClef*,
	NU32 clef );
NS32 NLib_Fichier_Clef_NFichierClef_ObtenirValeur3( const NFichierClef*,
	NU32 clef );

/* Obtenir copie valeur */
__ALLOC char *NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( const NFichierClef*,
	NU32 clef );

#endif // !NLIB_FICHIER_CLEF_NFICHIERCLEF_PROTECT

