#ifndef NLIB_FICHIER_OPERATION_PROTECT
#define NLIB_FICHIER_OPERATION_PROTECT

/*
	Operation sur les fichiers

	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Fichier::Operation
// ----------------------------------

/* Est EOF */
NBOOL NLib_Fichier_Operation_EstEOF( FILE* );
NBOOL NLib_Fichier_Operation_EstEOF2( FILE*,
	NU32 taille );

/* Obtenir taille */
long NLib_Fichier_Operation_ObtenirTaille( FILE* );
long NLib_Fichier_Operation_ObtenirTaille2( const char* );

/* Calculer CRC */
NS32 NLib_Fichier_Operation_CalculerCRC( FILE* );
NS32 NLib_Fichier_Operation_CalculerCRC2( const char* );

/* Le fichier existe? */
NBOOL NLib_Fichier_Operation_EstExiste( const char *lien );

/* Placer le curseur */
NBOOL NLib_Fichier_Operation_PlacerCaractere( FILE*,
	char );
NBOOL NLib_Fichier_Operation_PlacerChaine( FILE*,
	const char* );

/* Lire */
__ALLOC char *NLib_Fichier_Operation_LireEntre( FILE*,
	char c1,
	char c2 );

__ALLOC char *NLib_Fichier_Operation_LireEntre2( FILE*,
	char c );

__ALLOC char *NLib_Fichier_Operation_LireContenu( FILE* );

NU32 NLib_Fichier_Operation_LireNombreNonSigne( FILE*,
	NBOOL estConserverPosition );
NU32 NLib_Fichier_Operation_LireNombreSigne( FILE*,
	NBOOL estConserverPosition );

/* Ecrire */
NBOOL NLib_Fichier_Operation_Ecrire( const char *lien,
	const char *data,
	NU32 taille,
	NBOOL ecraserSiExiste );

/* Obtenir extension */
__ALLOC char *NLib_Fichier_Operation_ObtenirExtension( const char* );

#endif // !NLIB_FICHIER_OPERATION_PROTECT

