#ifndef NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT

/*
	Les diff�rents attibuts d'un fichier dans un r�pertoire

	@author SOARES Lucas
*/

// -----------------------------------------------------------
// enum NLib::Module::Repertoire::Element::NAttributRepertoire
// -----------------------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

// enum NLib::Module::Repertoire::Element::NAttributRepertoire
typedef enum
{
	NATTRIBUT_REPERTOIRE_NORMAL			= _A_NORMAL,	/* Normal file - No read/write restrictions */
	NATTRIBUT_REPERTOIRE_LECTURE_SEULE	= _A_RDONLY,	/* Read only file */
	NATTRIBUT_REPERTOIRE_CACHE			= _A_HIDDEN,	/* Hidden file */
	NATTRIBUT_REPERTOIRE_SYSTEME		= _A_SYSTEM,	/* System file */
	NATTRIBUT_REPERTOIRE				= _A_SUBDIR,	/* Subdirectory */
	NATTRIBUT_REPERTOIRE_ARCHIVE		= _A_ARCH		/* Archive file */
} NAttributRepertoire;

// V�rifier la taille
nassert( sizeof( NAttributRepertoire ) == sizeof( NU32 ),
	"NLib::Module::Repertoire::Element::NAttributRepertoire" );

#endif // NLIB_MODULE_REPERTOIRE

#endif // !NLIB_MODULE_REPERTOIRE_ELEMENT_NATTRIBUTREPERTOIRE_PROTECT

