#ifndef NLIB_MODULE_REPERTOIRE_ELEMENT_PROTECT
#define NLIB_MODULE_REPERTOIRE_ELEMENT_PROTECT

/*
	Un �lement d'un r�pertoire NRepertoire

	@author SOARES Lucas
*/

// -------------------------------------------
// namespace NLib::Module::Repertoire::Element
// -------------------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

// enum NLib::Module::Repertoire::Element::NAttributRepertoire
#include "NLib_Module_Repertoire_Element_NAttributRepertoire.h"

// struct NLib::Module::Repertoire::Element::NElementRepertoire
#include "NLib_Module_Repertoire_Element_NElementRepertoire.h"

#endif // NLIB_MODULE_REPERTOIRE

#endif // NLIB_MODULE_REPERTOIRE_ELEMENT_PROTECT

