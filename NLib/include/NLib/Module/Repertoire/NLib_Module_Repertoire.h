#ifndef NLIB_MODULE_REPERTOIRE_PROTECT
#define NLIB_MODULE_REPERTOIRE_PROTECT

/*
	Module r�pertoire

	@author SOARES Lucas
*/

// ----------------------------------
// namespace NLib::Module::Repertoire
// ----------------------------------

#ifdef NLIB_MODULE_REPERTOIRE

// R�pertoires
#ifdef IS_WINDOWS
#include <direct.h>
#include <io.h>
#else // IS_WINDOWS
#include <unistd.h>
#endif // !IS_WINDOWS

// namespace NLib::Module::Repertoire::Element
#include "Element/NLib_Module_Repertoire_Element.h"

// struct NLib::Module::Repertoire::NRepertoire
#include "NLib_Module_Repertoire_NRepertoire.h"

/* Codes erreur */
static const NS32 NLIB_ERREUR_REPERTOIRE = -1;
static const NS32 NLIB_ERREUR_RECHERCHE_REPERTOIRE = -1L;

/* Initialiser */
NBOOL NLib_Module_Repertoire_Initialiser( void );

/* D�truire */
void NLib_Module_Repertoire_Detruire( void );

/* Changer r�pertoire courant */
NBOOL NLib_Module_Repertoire_Changer( const char* );

// Restaurer r�pertoire
NBOOL NLib_Module_Repertoire_RestaurerInitial( void );
			
/* Obtenir r�pertoire courant */
__ALLOC char *NLib_Module_Repertoire_ObtenirCourant( void );

/* Obtenir r�pertoire initial */
const char *NLib_Module_Repertoire_ObtenirInitial( void );

/* Obtention chemin absolu �l�ment */
__ALLOC char *NLib_Module_Repertoire_ObtenirCheminAbsolu( const char* );

/* Compter �l�ments */
NU32 NLib_Module_Repertoire_ObtenirNombreElements( const char *filtre /* = "*" */,
	NAttributRepertoire attribut );

// R�pertoire initial
#ifdef NLIB_MODULE_REPERTOIRE_INTERNE
static char *m_repertoireInitial = NULL;
#endif // NLIB_MODULE_REPERTOIRE_INTERNE

/* Fonctions/types d�pendants de l'OS */
#ifdef IS_WINDOWS
// Parcours
#define CHDIR		_chdir
#define GETCWD		_getcwd

// Recherche
#define FINDFIRST	_findfirst
#define FINDNEXT	_findnext
#define FINDCLOSE	_findclose
#define FIND_DATA	struct _finddata64i32_t
#else // IS_WINDOW
// Parcours
#define CHDIR		chdir
#define GETCWD		getcwd

// Recherche
#define FINDFIRST	findfirst
#define FINDNEXT	findnext
#define FINDCLOSE	findclose
#define FIND_DATA	struct _finddata_
#endif // !IS_WINDOWS

#endif // NLIB_MODULE_REPERTOIRE

#endif // !NLIB_MODULE_REPERTOIRE_PROTECT

