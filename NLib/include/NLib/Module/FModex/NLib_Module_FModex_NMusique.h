#ifndef NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT
#define NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT

/*
	Une musique g�r�e via FModex

	@author SOARES Lucas
*/

// -------------------------------------
// struct NLib::Module::FModex::NMusique
// -------------------------------------

#ifdef NLIB_MODULE_FMODEX
// struct NLib::Module::FModex::NMusique
typedef struct
{
	// Musique
	FMOD_SOUND *m_musique;

	// Volume (entre 1 et 100)
	NU32 m_volume;

	// Est en cours de lecture?
	NBOOL m_estLecture;

	// Canal de lecture
	FMOD_CHANNEL *m_canal;

	// Position � la pause
	NU32 m_positionPause;

	// Contexte FModex
	const FMOD_SYSTEM *m_contexte;
} NMusique;
#endif // NLIB_MODULE_FMODEX

/* Construire */
__ALLOC NMusique *NLib_Module_FModex_NMusique_Construire( const char*,
	NU32 volume /* Entre 1 et 100 */,
	NBOOL estRepete );

/* D�truire */
void NLib_Module_FModex_NMusique_Detruire( NMusique** );

/* Lire */
NBOOL NLib_Module_FModex_NMusique_Lire( NMusique* );

/* Pause */
NBOOL NLib_Module_FModex_NMusique_Pause( NMusique* );

/* Arr�ter */
NBOOL NLib_Module_FModex_NMusique_Arreter( NMusique* );

/* Est en lecture? */
NBOOL NLib_Module_FModex_NMusique_EstLecture( const NMusique* );

/* Est en pause? */
NBOOL NLib_Module_FModex_NMusique_EstPause( const NMusique* );

/* Changer le volume */
NBOOL NLib_Module_FModex_NMusique_DefinirVolume( NMusique*,
	NU32 );


#endif // !NLIB_MODULE_FMODEX_NMUSIQUE_PROTECT

