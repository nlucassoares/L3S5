#ifndef NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT

/*
	D�finition d'un client auto-g�r� quant-� la r�ception
	et l'�mission de packets vers le serveur

	@author SOARES Lucas
*/

// --------------------------------------------
// struct NLib::Module::Reseau::Client::NClient
// --------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Reseau::Client::NClient
typedef struct
{
	// Socket
	SOCKET m_socket;

	// Cache packet
	NCachePacket *m_cachePacket;

	// Adressage client
	SOCKADDR_IN m_addr;

	// Est connect�?
	NBOOL m_estConnecte;

	// Est threads en cours?
	NBOOL m_estThreadEnCours;

	// Est erreur?
	NBOOL m_estErreur;

	// Threads
		// Emission
			HANDLE m_threadEmission;
		// Reception
			HANDLE m_threadReception;

	// Callbacks
		// Argument
			void *m_argumentCallback;
		// Reception packet
			__CALLBACK NBOOL ( *m_callbackReceptionPacket )( const NPacket*,
				void* );
		// D�connexion client
			__CALLBACK NBOOL ( *m_callbackDeconnexion )( void* );

	// Temps avant timeout (ignor� si 0)
	NU32 m_tempsAvantTimeout;
} NClient;

/* Construire */
__ALLOC NClient *NLib_Module_Reseau_Client_NClient_Construire( const char *ip,
	NU32 port,
	__CALLBACK NBOOL ( *callbackReceptionPacket )( const NPacket*,
		void* ),
	__CALLBACK NBOOL ( *callbackDeconnexion )( void* ),
	void *argumentCallbacks,
	NU32 tempsAvantTimeout );

/* D�truire */
void NLib_Module_Reseau_Client_NClient_Detruire( NClient** );

/* Connecter */
NBOOL NLib_Module_Reseau_Client_NClient_Connecter( NClient* );

/* D�connecter */
NBOOL NLib_Module_Reseau_Client_NClient_Deconnecter( NClient* );

/* Activer erreur */
void NLib_Module_Reseau_Client_NClient_ActiverErreur( NClient* );

/* Ajouter un packet */
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacket( NClient*,
	__WILLBEOWNED NPacket* );
NBOOL NLib_Module_Reseau_Client_NClient_AjouterPacketCopie( NClient*,
	const NPacket* );

/* Est connect�? */
NBOOL NLib_Module_Reseau_Client_NClient_EstConnecte( const NClient* );

/* Est erreur? */
NBOOL NLib_Module_Reseau_Client_NClient_EstErreur( const NClient* );

/* Obtenir cache packet */
NCachePacket *NLib_Module_Reseau_Client_NClient_ObtenirCachePacket( const NClient* );

/* Obtenir socket */
SOCKET NLib_Module_Reseau_Client_NClient_ObtenirSocket( const NClient* );

/* Obtenir est thread en cours */
NBOOL NLib_Module_Reseau_Client_NClient_EstThreadEnCours( const NClient* );

/* Obtenir l'argument pour le callback */
void *NLib_Module_Reseau_Client_NClient_ObtenirArgumentCallback( const NClient* );

/* Est timeout autoris�? */
NBOOL NLib_Module_Reseau_Client_NClient_EstTimeoutAutorise( const NClient* );

/* Obtenir d�lai avant timeout */
NU32 NLib_Module_Reseau_Client_NClient_ObtenirTimeout( const NClient* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_NCLIENT_PROTECT

