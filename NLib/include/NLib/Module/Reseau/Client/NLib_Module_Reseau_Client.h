#ifndef NLIB_MODULE_RESEAU_CLIENT_PROTECT
#define NLIB_MODULE_RESEAU_CLIENT_PROTECT

/*
	D�finition des �l�ments n�cessaires � un client NClient

	@author SOARES Lucas
*/

// --------------------------------------
// namespace NLib::Module::Reseau::Client
// --------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Module::Reseau::Client::NClient
#include "NLib_Module_Reseau_Client_NClient.h"

// namespace NLib::Module::Reseau::Client::Thread
#include "Thread/NLib_Module_Reseau_Client_Thread.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_CLIENT_PROTECT

