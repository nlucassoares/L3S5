#ifndef NLIB_MODULE_RESEAU_PROTECT
#define NLIB_MODULE_RESEAU_PROTECT

/*
	Module r�seau

	Sous windows, � l'initialisation du module, appel
	de WSAStartup

	@author SOARES Lucas
*/

// ------------------------------
// namespace NLib::Module::Reseau
// ------------------------------

#ifdef NLIB_MODULE_RESEAU

// Headers pour windows
#ifdef IS_WINDOWS
#include <WinSock2.h>
#include <WS2tcpip.h>
#endif // IS_WINDOWS

// Adresse 
#define NLIB_ADRESSE_IP_LOCALE						"127.0.0.1"
#define NLIB_TAILLE_MAXIMALE_CHAINE_IP				15
#define NLIB_TAILLE_MAXIMALE_CHAINE_IP_PORT			21 // XXX.XXX.XXX.XXX:XXXXX

// D�lai avant d'�tre timeout pour le ping
static const NU32 NDUREE_AVANT_TIMEOUT_PING		= 2000;

// D�lai avant nouvelle demande ping
static const NU32 NDELAI_ENTRE_REQUETE_PING		= 5000;

/* Initialiser r�seau */
NBOOL NLib_Module_Reseau_Initialiser( void );

/* Est r�seau initialis�? */
NBOOL NLib_Module_Reseau_EstInitialise( void );

/* D�truire r�seau */
void NLib_Module_Reseau_Detruire( void );

// Variable interne
#ifdef RESEAU_INTERNE
// Contexte WSA pour windows uniquement
#ifdef IS_WINDOWS
WSADATA m_contexteWSA;
#endif // IS_WINDOWS
#endif // RESEAU_INTERNE

// struct NLib::Module::Reseau::NPing
#include "NLib_Module_Reseau_NPing.h"

// namespace NLib::Module::Reseau::Packet
#include "Packet/NLib_Module_Reseau_Packet.h"

// namespace NLib::Module::Reseau::Serveur
#include "Serveur/NLib_Module_Reseau_Serveur.h"

// namespace NLib::Module::Reseau::Socket
#include "Socket/NLib_Module_Reseau_Socket.h"

// namespace NLib::Module::Reseau::Client
#include "Client/NLib_Module_Reseau_Client.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PROTECT

