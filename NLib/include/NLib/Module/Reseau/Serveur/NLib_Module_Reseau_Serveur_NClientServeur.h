#ifndef NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT

/*
	D�finition d'un client g�r� par le serveur

	Il est possible d'ajouter un packet gr�ce au NCachePacket

	Attribution automatique d'un identifiant unique exploitab
	le pour l'identification externe

	La structure dispose d'un membre m_donneeExterne, utilisa
	ble directement par l'utilisateur au moyen de
	NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne
	et
	NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne

	@author SOARES Lucas
*/

// ----------------------------------------------------
// struct NLib::Module::Reseau::Serveur::NClientServeur
// ----------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// Limite pour l'identifiant
#define LIMITE_IDENTIFIANT_CLIENT_SERVEUR		100000

// struct NLib::Module::Reseau::Serveur::NClientServeur
typedef struct NClientServeur
{
	// Socket client
	SOCKET m_socket;

	// Contexte d'adressage
	SOCKADDR_IN m_contexteAdressage;

	// Identifiant unique
	NU32 m_identifiantUnique;

	// Thread emission
#ifdef IS_WINDOWS
	HANDLE m_threadEmission;
#endif // IS_WINDOWS

	// Thread r�ception
#ifdef IS_WINDOWS
	HANDLE m_threadReception;
#endif // IS_WINDOWS

	// Cache packets
	NCachePacket *m_cachePacket;

	// IP du client
	char *m_ip;

	// Est mort? (Doit �tre d�co/supprim�)
	NBOOL m_estMort;

	// Callback reception packet
	__CALLBACK NBOOL ( *m_callbackReception )( const struct NClientServeur*,
		const NPacket* );

	// Donn�es client externe
	char *m_donneeExterne;

	// Ping
	NPing *m_ping;

	// Est en cours?
	NBOOL m_estEnCours;

	// Temps avant timeout
	NU32 m_tempsAvantTimeout;
} NClientServeur;

// Construire le client
__ALLOC NClientServeur *NLib_Module_Reseau_Serveur_NClientServeur_Construire( SOCKET socketServeur,
	__CALLBACK NBOOL ( *callbackReception )( const NClientServeur*,
		const NPacket* ),
	const NBOOL *estConnexionAutorisee,
	NU32 tempsAvantTimeout );

// D�truire le client
void NLib_Module_Reseau_Serveur_NClientServeur_Detruire( NClientServeur** );

// Est mort?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstMort( const NClientServeur* );

// Est en cours?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstEnCours( const NClientServeur* );

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( const NClientServeur* );

// Tuer le client
void NLib_Module_Reseau_Serveur_NClientServeur_Tuer( NClientServeur* );

// D�finir identifiant
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirIdentifiantUnique( NClientServeur*,
	NU32 identifiant );

// Obtenir l'identifiant
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( const NClientServeur* );

// Obtenir SOCKET
SOCKET NLib_Module_Reseau_Serveur_NClientServeur_ObtenirSocket( const NClientServeur* );

// Obtenir cache packet
NCachePacket *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirCachePacket( const NClientServeur* );

// Donner un handle de donn�e au client
void NLib_Module_Reseau_Serveur_NClientServeur_DefinirDonneeExterne( NClientServeur*,
	char *data );

// Obtenir les donn�es externes enregistr�es pour ce client
char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( const NClientServeur* );

// Obtenir ping
const NPing *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( const NClientServeur* );

// Ajouter un packet
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( NClientServeur*,
	__WILLBEOWNED NPacket *packet );
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( NClientServeur*,
	const NPacket* );

// Obtenir adresse ip
const char *NLib_Module_Reseau_Serveur_NClientServeur_ObtenirAdresseIP( const NClientServeur* );

// Est timeout autoris�?
NBOOL NLib_Module_Reseau_Serveur_NClientServeur_EstTimeoutAutorise( const NClientServeur* );

// Obtenir temps avant timeout
NU32 NLib_Module_Reseau_Serveur_NClientServeur_ObtenirTempsAvantTimeout( const NClientServeur* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_NCLIENTSERVEUR_PROTECT

