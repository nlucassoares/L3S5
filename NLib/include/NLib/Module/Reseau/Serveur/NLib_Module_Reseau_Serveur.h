#ifndef NLIB_MODULE_RESEAU_SERVEUR_PROTECT
#define NLIB_MODULE_RESEAU_SERVEUR_PROTECT

/*
	D�finition des �l�ments n�cessaire � un serveur

	@author SOARES Lucas
*/

// ---------------------------------------
// namespace NLib::Module::Reseau::Serveur
// ---------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Module::Reseau::Serveur::NClientServeur
#include "NLib_Module_Reseau_Serveur_NClientServeur.h"

// struct NLib::Module::Reseau::Serveur::NServeur
#include "NLib_Module_Reseau_Serveur_NServeur.h"

// namespace NLib::Module::Reseau::Serveur::Thread
#include "Thread/NLib_Module_Reseau_Serveur_Thread.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_SERVEUR_PROTECT

