#ifndef NLIB_MODULE_RESEAU_PACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PROTECT

/*
	Définition d'un packet

	@author SOARES Lucas
*/

// --------------------------------------
// namespace NLib::Module::Reseau::Packet
// --------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NLib::Reseau::Packet::NPacket
#include "NLib_Module_Reseau_Packet_NPacket.h"

// struct NLib::Reseau::Packet::NCachePacket
#include "NLib_Module_Reseau_Packet_NCachePacket.h"

// Taille buffer packet (octets)
#define NBUFFER_PACKET				(NU32)4096

// namespace NLib::Reseau::Packet::PacketIO
#include "PacketIO/NLib_Module_Reseau_Packet_PacketIO.h"
#include "PacketIO/NLib_Module_Reseau_Packet_PacketIO2.h"

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PROTECT

