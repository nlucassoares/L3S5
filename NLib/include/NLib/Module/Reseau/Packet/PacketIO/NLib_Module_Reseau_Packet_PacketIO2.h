#ifndef NLIB_MODULE_RESEAU_PACKET_PACKETIO2_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PACKETIO2_PROTECT

#ifdef NLIB_MODULE_RESEAU
#ifdef NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT

// ------------------------------------------------
// namespace NLib::Module::Reseau::Packet::PacketIO
// ------------------------------------------------

// Recevoir un packet
__ALLOC NPacket *NLib_Module_Reseau_Packet_PacketIO_RecevoirPacket( SOCKET socket,
	NBOOL estTimeoutAutorise );

// Envoyer un packet
NBOOL NLib_Module_Reseau_Packet_PacketIO_EnvoyerPacket( const NPacket *packet,
	SOCKET socket );

#endif // NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PACKETIO2_PROTECT

