#ifndef NLIB_MODULE_RESEAU_PACKET_PACKETIO_NETAPEIO_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_PACKETIO_NETAPEIO_PROTECT

/*
	Etape pour la transmission de packets

	@author SOARES Lucas
*/

#ifdef NLIB_MODULE_RESEAU
#ifndef NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
// -----------------------------------------------------------
// enum NLib::Module::Reseau::Packet::PacketIO::NEtapePacketIO
// -----------------------------------------------------------

// enum NLib::Reseau::Packet::PacketIO::NEtapePacketIO
typedef enum
{
	NETAPE_PACKET_IO_BUFFER_HEADER,

	NETAPE_PACKET_IO_BUFFER_CONTENU,

	NETAPES_PACKET_IO
} NEtapePacketIO;

#endif // !NMETHODE_TRANSFERT_NON_SECURISE_PACKETIO_NPROJECT
#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_PACKETIO_NETAPEIO_PROTECT

