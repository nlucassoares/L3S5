#ifndef NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT
#define NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT

/*
	D�finition du cache packet, accueillants les packets
	� transmettre sous la forme d'une file

	@author SOARES Lucas
*/

// -------------------------------------------------
// struct NLib::Module::Reseau::Packet::NCachePacket
// -------------------------------------------------

#ifdef NLIB_MODULE_RESEAU

// struct NCachePacket
typedef struct
{
	// Nombre de packet
	NU32 m_nombrePacket;

	// NPacket
	NPacket **m_packet;

	// Mutex
	NMutex *m_mutex;
} NCachePacket;

// Construire cache packet
__ALLOC NCachePacket *NLib_Module_Reseau_Packet_NCachePacket_Construire( void );

// D�truire cache packet
void NLib_Module_Reseau_Packet_NCachePacket_Detruire( NCachePacket** );

// Vider cache
void NLib_Module_Reseau_Packet_NCachePacket_Vider( NCachePacket* );

// Obtenir le nombre de packets
NU32 NLib_Module_Reseau_Packet_NCachePacket_ObtenirNombrePackets( const NCachePacket* );

// Obtenir le packet � envoyer
__ALLOC NPacket *NLib_Module_Reseau_Packet_NCachePacket_ObtenirPacket( const NCachePacket* );

// Est packet(s) dans le cache?
NBOOL NLib_Module_Reseau_Packet_NCachePacket_EstPacketsDansCache( const NCachePacket* );

// Ajouter un packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacket( NCachePacket*,
	__WILLBEOWNED NPacket* );

// Ajouter une copie de packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_AjouterPacketCopie( NCachePacket*,
	const NPacket* );

// Supprimer le packet
NBOOL NLib_Module_Reseau_Packet_NCachePacket_SupprimerPacket( NCachePacket* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_PACKET_NCACHEPACKET_PROTECT



