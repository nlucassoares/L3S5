#ifndef NLIB_MODULE_RESEAU_NPING_PROTECT
#define NLIB_MODULE_RESEAU_NPING_PROTECT

// ----------------------------------
// struct NLib::Module::Reseau::NPing
// ----------------------------------

#ifdef NLIB_MODULE_RESEAU

typedef struct
{
	// Temps derni�re demande
	NU32 m_tempsDerniereDemande;

	// Est en attente de r�ponse
	NBOOL m_estAttenteReponse;

	// Ping actuel
	NU32 m_ping;
} NPing;

/* Construire */
__ALLOC NPing *NLib_Module_Reseau_NPing_Construire( void );

/* D�truire */
void NLib_Module_Reseau_NPing_Detruire( NPing** );

/* Obtenir dur�e depuis dernier ping */
NU32 NLib_Module_Reseau_NPing_ObtenirDureeDepuisDernierPing( const NPing* );

/* Effectuer requ�te */
NBOOL NLib_Module_Reseau_NPing_EffectuerRequete( NPing* );

/* R�ponse � la requ�te */
NBOOL NLib_Module_Reseau_NPing_RecevoirReponseRequete( NPing* );

/* Est requ�te timeout? */
NBOOL NLib_Module_Reseau_NPing_EstTimeout( const NPing* );

/* Est doit effectuer une nouvelle requ�te de ping? */
NBOOL NLib_Module_Reseau_NPing_EstDoitEffectuerRequete( const NPing* );

/* Obtenir ping */
NU32 NLib_Module_Reseau_NPing_ObtenirPing( const NPing* );

/* Remettre � z�ro */
void NLib_Module_Reseau_NPing_MettreAZero( NPing* );

#endif // NLIB_MODULE_RESEAU

#endif // !NLIB_MODULE_RESEAU_NPING_PROTECT

