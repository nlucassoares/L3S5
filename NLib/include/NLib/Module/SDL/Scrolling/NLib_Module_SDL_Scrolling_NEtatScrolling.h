#ifndef NLIB_MODULE_SDL_SCROLLING_NETATSCROLLING_PROTECT
#define NLIB_MODULE_SDL_SCROLLING_NETATSCROLLING_PROTECT

// ---------------------------------------------------
// struct NLib::Module::SDL::Scrolling::NEtatScrolling
// ---------------------------------------------------

#ifdef NLIB_MODULE_SDL

typedef struct NEtatScrolling
{
	// Position clic d�but scrolling
	NSPoint m_positionClic;

	// Etat initial scrolling
	NSPoint m_scrollingInitial;

	// Scrolling
	NSPoint m_scrolling;

	// Est en cours?
	NBOOL m_estEnCours;
} NEtatScrolling;

/* Construire */
__ALLOC NEtatScrolling *NLib_Module_SDL_Scrolling_NEtatScrolling_Construire( void );

/* D�truire */
void NLib_Module_SDL_Scrolling_NEtatScrolling_Detruire( NEtatScrolling** );

/* Obtenir scrolling */
const NSPoint *NLib_Module_SDL_Scrolling_NEtatScrolling_ObtenirScrolling( const NEtatScrolling* );

/* D�but scrolling */
void NLib_Module_SDL_Scrolling_NEtatScrolling_DebutScrolling( NEtatScrolling*,
	const NSPoint *souris );

/* Update scrolling (retourne vrai si une modification a �t� effectu�e) */
NBOOL NLib_Module_SDL_Scrolling_NEtatScrolling_UpdateScrolling( NEtatScrolling*,
	const NSPoint *souris );

/* Fin scrolling */
void NLib_Module_SDL_Scrolling_NEtatScrolling_FinScrolling( NEtatScrolling* );

/* Mettre � z�ro */
void NLib_Module_SDL_Scrolling_NEtatScrolling_MettreZero( NEtatScrolling* );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SCROLLING_NETATSCROLLING_PROTECT

