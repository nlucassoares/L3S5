#ifndef NLIB_MODULE_SDL_NCADRE_PROTECT
#define NLIB_MODULE_SDL_NCADRE_PROTECT

/*
	Cadre applicable sur un objet NFenetre

	@author SOARES Lucas
*/

// --------------------------------
// struct NLib::Module::SDL::NCadre
// --------------------------------

#ifdef NLIB_MODULE_SDL

typedef struct
{
	// Epaisseur
	NU32 m_epaisseur;

	// Origine
	NSPoint m_position;

	// Taille
	NUPoint m_taille;

	// Couleur
	NCouleur m_couleur;

	// Couleur fond
	NCouleur m_couleurFond;

	// Rectangles � tracer
	SDL_Rect m_coordonnee[ NDIRECTIONS + 1 ];

	// Fen�tre
	const NFenetre *m_fenetre;

	// Pour d�placement
		// Position initiale
			NSPoint m_decalageDeplacement;
		// D�placement activ�?
			NBOOL m_estDeplacementActif;
} NCadre;

/* Construire l'objet */
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire( NSPoint position,
	NUPoint taille,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre*,
	NU32 epaisseur );
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire2( const NCadre *src );
__ALLOC NCadre *NLib_Module_SDL_NCadre_Construire3( NS32 x,
	NS32 y,
	NU32 w,
	NU32 h,
	NCouleur couleur,
	NCouleur couleurFond,
	const NFenetre*,
	NU32 epaisseur );

/* D�truire l'objet */
void NLib_Module_SDL_NCadre_Detruire( NCadre** );

/* Obtenir position */
const NSPoint *NLib_Module_SDL_NCadre_ObtenirPosition( const NCadre* );

/* Obtenir taille */
const NUPoint *NLib_Module_SDL_NCadre_ObtenirTaille( const NCadre* );

/* Obtenir couleur */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleur( const NCadre* );

/* Obtenir couleur fond */
const NCouleur *NLib_Module_SDL_NCadre_ObtenirCouleurFond( const NCadre* );

/* Obtenir �paisseur */
NU32 NLib_Module_SDL_NCadre_ObtenirEpaisseur( const NCadre* );

/* D�finir position */
void NLib_Module_SDL_NCadre_DefinirPosition( NCadre*,
	NS32 x,
	NS32 y );

/* D�finir taille */
void NLib_Module_SDL_NCadre_DefinirTaille( NCadre*,
	NU32 x,
	NU32 y );

/* D�finir couleur */
void NLib_Module_SDL_NCadre_DefinirCouleur( NCadre*,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a );

/* D�finir couleur fond */
void NLib_Module_SDL_NCadre_DefinirCouleurFond( NCadre*,
	NU8 r,
	NU8 g,
	NU8 b,
	NU8 a );

/* D�finir �paisseur */
void NLib_Module_SDL_NCadre_DefinirEpaisseur( NCadre*,
	NU32 );

/* Dessiner */
void NLib_Module_SDL_NCadre_Dessiner( const NCadre* );

/* Est en colision? */
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec( const NCadre*,
	const NSRect* );
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec2( const NCadre*,
	const NCadre* );
NBOOL NLib_Module_SDL_NCadre_EstEnColisionAvec3( const NCadre*,
	NSPoint );

/* Calculer intersection */
__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection( const NCadre*,
	const NCadre* );
__ALLOC NCadre *NLib_Module_SDL_NCadre_CalculerIntersection2( const NCadre*,
	const NSRect* );

/* D�placer */
void NLib_Module_SDL_NCadre_Deplacer( NCadre*,
	NSPoint positionSouris,
	NBOOL continuerDeplacement );

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_NCADRE_PROTECT

