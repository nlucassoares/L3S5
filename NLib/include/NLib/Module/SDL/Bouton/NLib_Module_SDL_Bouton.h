#ifndef NLIB_MODULE_SDL_BOUTON_PROTECT
#define NLIB_MODULE_SDL_BOUTON_PROTECT

/*
	Définitions relatives au bouton NBouton

	@author SOARES Lucas
*/

// -----------------------------------
// namespace NLib::Module::SDL::Bouton
// -----------------------------------

#ifdef NLIB_MODULE_SDL
// enum NLib::Module::SDL::Bouton::NEtat
#include "NLib_Module_SDL_Bouton_NEtatBouton.h"

// struct NLib::Module::SDL::Bouton::NBouton
#include "NLib_Module_SDL_Bouton_NBouton.h"

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_BOUTON_PROTECT

