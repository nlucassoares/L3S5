#ifndef NLIB_MODULE_SDL_BOUTON_NETATBOUTON_PROTECT
#define NLIB_MODULE_SDL_BOUTON_NETATBOUTON_PROTECT

/*
	Les diff�rents �tats d'un bouton NBouton

	@author SOARES Lucas
*/

// -------------------------------------------
// enum NLib::Module::SDL::Bouton::NEtatBouton
// -------------------------------------------

#ifdef NLIB_MODULE_SDL
typedef enum
{
	// Rien de particulier
	NETAT_BOUTON_REPOS,

	// Le curseur est au dessus du bouton
	NETAT_BOUTON_SURVOLE,

	// Le bouton est press�
	NETAT_BOUTON_PRESSE,

	NETATS_BOUTON
} NEtatBouton;

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_BOUTON_NETATBOUTON_PROTECT

