#ifndef NLIB_MODULE_SDL_SAISIE_NACCENTSAISIESDL_PROTECT
#define NLIB_MODULE_SDL_SAISIE_NACCENTSAISIESDL_PROTECT

/*
	Les différents types d'accent disponibles pour la
	saisie NSaisieSDL

	@author SOARES Lucas
*/

// ------------------------------------------------
// enum NLib::Module::SDL::Saisie::NAccentSaisieSDL
// ------------------------------------------------

#ifdef NLIB_MODULE_SDL

typedef enum
{
	NACCENT_SAISIE_SDL_AUCUN,

	NACCENT_SAISIE_SDL_AIGU,
	NACCENT_SAISIE_SDL_GRAVE,
	NACCENT_SAISIE_SDL_CIRCONFLEXE,
	NACCENT_SAISIE_SDL_TREMA,
	NACCENT_SAISIE_SDL_TILDE,
	
	NACCENTS_SAISIE_SDL
} NAccentSaisieSDL;

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SAISIE_NACCENTSAISIESDL_PROTECT

