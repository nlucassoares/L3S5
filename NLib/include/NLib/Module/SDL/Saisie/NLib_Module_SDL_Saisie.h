#ifndef NLIB_MODULE_SDL_SAISIE_PROTECT
#define NLIB_MODULE_SDL_SAISIE_PROTECT

/*
	Gestion de la saisie

	@author SOARES Lucas
*/

// -----------------------------------
// namespace NLib::Module::SDL::Saisie
// -----------------------------------

#ifdef NLIB_MODULE_SDL
// enum NLib::Module::SDL::Saisie::NAccentSaisieSDL
#include "NLib_Module_SDL_Saisie_NAccentSaisieSDL.h"

// enum NLib::Module::SDL::Saisie::NModeSaisieSDL
#include "NLib_Module_SDL_Saisie_NModeSaisieSDL.h"

// enum NLib::Module::SDL::Saisie::NRetourSaisieSDL
#include "NLib_Module_SDL_Saisie_NRetourSaisieSDL.h"

// struct NLib::Moduls::SDL::Saisie::NSaisieSDL
#include "NLib_Module_SDL_Saisie_NSaisieSDL.h"

// Constantes
static const NCadre **NSAISIE_SDL_CADRE_TOUT = (NCadre**)0xFFFFFFFF;
static const NU32 NSAISIE_SDL_ID_CADRE_TOUT = (NU32)0xFFFFFFFE;
static const NU32 NSAISIE_SDL_ID_CADRE_AUCUN = (NU32)0xFFFFFFFF;

#endif // NLIB_MODULE_SDL

#endif // !NLIB_MODULE_SDL_SAISIE_PROTECT

