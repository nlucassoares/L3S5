#ifndef NLIB_MODULE_NETAPEMODULE_PROTECT
#define NLIB_MODULE_NETAPEMODULE_PROTECT

/*
	Liste des diff�rentes �tapes pour l'initialisation
	des modules

	@author SOARES Lucas
*/

// -------------------------------
// enum NLib::Module::NEtapeModule
// -------------------------------
typedef enum
{
	NETAPE_MODULE_RESEAU,
	NETAPE_MODULE_SDL,
	NETAPE_MODULE_SDL_IMAGE,
	NETAPE_MODULE_SDL_TTF,
	NETAPE_MODULE_REPERTOIRE,
	NETAPE_MODULE_FMODEX,

	NETAPES_MODULE
} NEtapeModule;

#endif // !NLIB_MODULE_NETAPEMODULE_PROTECT

