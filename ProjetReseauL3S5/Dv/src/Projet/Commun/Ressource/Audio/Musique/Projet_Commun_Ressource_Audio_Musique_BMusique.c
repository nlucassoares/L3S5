#include "../../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------
// struct Projet::Commun::Ressource::Audio::Musique::BMusique
// ----------------------------------------------------------

/* Construire */
__ALLOC BMusique *Projet_Commun_Ressource_Audio_Musique_BMusique_Construire( void )
{
	// Sortie
	__OUTPUT BMusique *out;

	// It�rateurs
	NU32 i,
		j;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BMusique ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger musiques
	for( i = 0; i < BLISTE_MUSIQUES; i++ )
		if( !( out->m_musique[ i ] = NLib_Module_FModex_NMusique_Construire( Projet_Commun_Ressource_Audio_Musique_BListeMusique_ObtenirLien( i ),
			100,
			NTRUE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NLib_Module_FModex_NMusique_Detruire( &out->m_musique[ j ] );

			// Quitter
			return NULL;
		}

	// Aucune musique lue
	out->m_musiqueEnCours = BLISTE_MUSIQUE_AUCUNE;

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Ressource_Audio_Musique_BMusique_Detruire( BMusique **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire les musiques
	for( ; i < BLISTE_MUSIQUES; i++ )
		NLib_Module_FModex_NMusique_Detruire( &(*this)->m_musique[ i ] );

	// Lib�rer
	NFREE( *this );
}

/* Lire musique */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_Lire( BMusique *this,
	BListeMusique musique )
{
	// Lire seulement si diff�rente
	if( musique == this->m_musiqueEnCours )
		return NTRUE;

	// Arr�ter
	Projet_Commun_Ressource_Audio_Musique_BMusique_Arreter( this );

	// Enregistrer
	this->m_musiqueEnCours = musique;

	// Lire
	return NLib_Module_FModex_NMusique_Lire( this->m_musique[ musique ] );
}

/* Est lecture en cours? */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_EstLecture( const BMusique *this )
{
	return ( this->m_musiqueEnCours != BLISTE_MUSIQUE_AUCUNE );
}

/* Arr�ter musique */
NBOOL Projet_Commun_Ressource_Audio_Musique_BMusique_Arreter( BMusique *this )
{
	// V�rifier �tat
	if( this->m_musiqueEnCours == BLISTE_MUSIQUE_AUCUNE )
		return NFALSE;

	// Stopper la musique
	NLib_Module_FModex_NMusique_Arreter( this->m_musique[ this->m_musiqueEnCours ] );

	// Plus de musique lue
	this->m_musiqueEnCours = BLISTE_MUSIQUE_AUCUNE;

	// OK
	return NTRUE;
}

