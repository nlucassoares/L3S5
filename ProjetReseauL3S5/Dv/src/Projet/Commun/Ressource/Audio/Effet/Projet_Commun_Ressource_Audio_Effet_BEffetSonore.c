#include "../../../../../../include/Projet/Projet.h"

// ------------------------------------------------------------
// struct Projet::Commun::Ressource::Audio::Effet::BEffetSonore
// ------------------------------------------------------------

/* Construire */
__ALLOC BEffetSonore *Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Construire( void )
{
	// It�rateurs
	NU32 i,
		j;

	// Sortie
	__OUTPUT BEffetSonore *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEffetSonore ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire les effets
	for( i = 0; i < BLISTE_EFFETS_SONORE; i++ )
		if( !( out->m_son[ i ] = NLib_Module_FModex_NSon_Construire( Projet_Commun_Ressource_Audio_Effet_BListeEffetSonore_ObtenirLien( i ),
			BVOLUME_EFFET_SONORE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// D�truire effets
			for( j = 0; j < i; j++ )
				NLib_Module_FModex_NSon_Detruire( &out->m_son[ j ] );

			// Lib�rer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Detruire( BEffetSonore **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire sons
	for( ; i < BLISTE_EFFETS_SONORE; i++ )
		NLib_Module_FModex_NSon_Detruire( &(*this)->m_son[ i ] );

	// Lib�rer
	NFREE( *this );
}

/* Lire son */
void Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Lire( const BEffetSonore *this,
	BListeEffetSonore effet )
{
	// Lire
	if( !NLib_Module_FModex_NSon_Lire( this->m_son[ effet ] ) )
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FMODEX );
}

