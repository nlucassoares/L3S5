#include "../../../../../../include/Projet/Projet.h"

// ----------------------------------------------------
// namespace Projet::Commun::Carte::Tileset::Chargement
// ----------------------------------------------------

/* Charger les tilesets */
__ALLOC BTileset **Projet_Commun_Carte_Tileset_Chargement_Charger( __OUTPUT NU32 *nombreTileset,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BTileset **out;

	// R�pertoire
	NRepertoire *repertoire;

	// Filtre
	char filtre[ 32 ];

	// It�rateur
	NU32 i, j;

	// Lien
	char lien[ MAX_PATH ];

	// Cr�er le r�pertoire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( )) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}
		
	// Se placer dans le r�pertoire des tilesets
	if( !NLib_Module_Repertoire_Changer( BREPERTOIRE_TILESET ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// D�truire r�pertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Cr�er le filtre
	sprintf( filtre,
		"*.%s",
		BEXTENSION_FICHIER_TILESET );

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		filtre,
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Restaurer le r�pertoire
		NLib_Module_Repertoire_RestaurerInitial( );

		// D�truire r�pertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Restaurer le r�pertoire initial
	NLib_Module_Repertoire_RestaurerInitial( );

	// Enregistrer le nombre de tilesets
	*nombreTileset = NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire );

	// Allouer la m�moire
	if( !( out = calloc( *nombreTileset,
		sizeof( BTileset* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// D�truire r�pertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Construire les tilesets
	for( i = 0; i < *nombreTileset; i++ )
	{
		// Cr�er le lien
		sprintf( lien,
			"%s/%s%d.%s",
			BREPERTOIRE_TILESET,
			BBASE_NOM_TILESET,
			i + 1,
			BEXTENSION_FICHIER_TILESET );

		// Construire tileset
		if( !( out[ i ] = Projet_Commun_Carte_Tileset_BTileset_Construire( lien,
			fenetre ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );
			
			// D�truire la sortie
			for( j = 0; j < i; j++ )
				Projet_Commun_Carte_Tileset_BTileset_Detruire( &out[ j ] );
			NFREE( out );

			// D�truire le r�pertoire
			NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

			// Quitter
			return NULL;
		}
	}	

	// D�truire le r�pertoire
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return out;
}

