#include "../../../../../../../include/Projet/Projet.h"

// -----------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::Bonus::BBonusCase
// -----------------------------------------------------------

/* Construire */
__ALLOC BBonusCase *Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Construire( BListeBonus type,
	NU32 tempsApparition,
	NU32 dureeBonus,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT BBonusCase *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BBonusCase ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_bonus = type;
	out->m_dureeBonus = dureeBonus;
	out->m_tempsApparition = tempsApparition;
	out->m_identifiant = identifiant;

	// Z�ro
	out->m_alpha = 0xFF;

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Detruire( BBonusCase **this )
{
	NFREE( *this );
}

/* Obtenir type */
BListeBonus Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirType( const BBonusCase *this )
{
	return this->m_bonus;
}

/* Obtenir temps apparition */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirTempsApparition( const BBonusCase *this )
{
	return this->m_tempsApparition;
}

/* Obtenir dur�e bonus */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirDureeBonus( const BBonusCase *this )
{
	return this->m_dureeBonus;
}

/* Obtenir modificateur alpha */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirModificateurAlpha( const BBonusCase *this )
{
	return this->m_alpha;
}

/* Obtenir identifiant */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirIdentifiant( const BBonusCase *this )
{
	return this->m_identifiant;
}

/* Update */
void Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Update( BBonusCase *this )
{
	// Si le bonus doit bient�t disparaitre
	if( (NS32)( NLib_Temps_ObtenirTick( ) - this->m_tempsApparition ) >= (NS32)this->m_dureeBonus - ( (NS32)this->m_dureeBonus / (NS32)BPROPORTION_AVANT_CLIGNOTEMENT_DISPARITION_BONUS ) )
	{
		switch( this->m_sensAlpha )
		{
			// D�cr�menter
			case 0:
				if( this->m_alpha > BVITESSE_INCDEC_ALPHA_DISPARITION_BONUS )
					this->m_alpha -= BVITESSE_INCDEC_ALPHA_DISPARITION_BONUS;
				else
					this->m_sensAlpha = !this->m_sensAlpha;
				break;

			// Incr�menter
			case 1:
			default:
				if( this->m_alpha < 0xFF - BVITESSE_INCDEC_ALPHA_DISPARITION_BONUS )
					this->m_alpha += BVITESSE_INCDEC_ALPHA_DISPARITION_BONUS;
				else
					this->m_sensAlpha = !this->m_sensAlpha;
				break;
		}
	}
}

