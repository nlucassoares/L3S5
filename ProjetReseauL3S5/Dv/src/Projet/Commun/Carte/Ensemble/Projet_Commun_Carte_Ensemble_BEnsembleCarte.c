#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------------------
// struct Projet::Commun::Carte::Ensemble::BEnsembleCarte
// ------------------------------------------------------

/* Calculer le checksum (priv�e) */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_CalculerChecksumInterne( const BEnsembleCarte *this )
{
	// Sortie
	__OUTPUT NU32 resultat = 0;

	// It�rateur
	NU32 i;

	// Ajouter nombre cartes
	resultat += this->m_nombreCarte * 3;

	// Ajouter checksum carte
	for( i = 0; i < this->m_nombreCarte; i++ )
		resultat += Projet_Commun_Carte_BCarte_ObtenirChecksum( this->m_carte[ i ] );

	// OK
	return resultat;
}

/* Construire l'ensemble des cartes */
__ALLOC BEnsembleCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Construire( const char *dossierCarte )
{
	// Sortie
	__OUTPUT BEnsembleCarte *out;

	// R�pertoire
	NRepertoire *repertoire;

	// Filtre
	char filtre[ 256 ];

	// Buffer
	char buffer[ MAX_PATH ];

	// It�rateurs
	NU32 i,
		j;

	// Construire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Quitter
		return NULL;
	}

	// Changer de r�pertoire
	if( !NLib_Module_Repertoire_Changer( dossierCarte ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// D�truire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Cr�er le filtre
	sprintf( filtre,
		"*.%s",
		BEXTENSION_FICHIER_CARTE );

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		filtre,
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// D�truire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Restaurer
		NLib_Module_Repertoire_RestaurerInitial( );

		// Quitter
		return NULL;
	}

	// Restaurer
	NLib_Module_Repertoire_RestaurerInitial( );

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEnsembleCarte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// R�cup�rer le nombre de cartes
	if( !( out->m_nombreCarte = NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

		// D�truire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// D�truire r�pertoire
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// Allouer conteneur carte
	if( !( out->m_carte = calloc( out->m_nombreCarte,
		sizeof( BCarte* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les cartes
	for( i = 0; i < out->m_nombreCarte; i++ )
	{
		// Cr�er le lien
		sprintf( buffer,
			"%s/%s%d.%s",
			dossierCarte,
			BBASE_NOM_FICHIER_CARTE,
			i + 1,
			BEXTENSION_FICHIER_CARTE );

		// Charger
		if( !( out->m_carte[ i ] = Projet_Commun_Carte_BCarte_Construire2( buffer ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
			for( j = 0; j < i; j++ )
				Projet_Commun_Carte_BCarte_Detruire( &out->m_carte[ j ] );
			NFREE( out->m_carte );
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Calculer le checksum
	out->m_checksum = Projet_Commun_Carte_Ensemble_BEnsembleCarte_CalculerChecksumInterne( out );

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Carte_Ensemble_BEnsembleCarte_Detruire( BEnsembleCarte **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire cartes
	for( i = 0; i < (*this)->m_nombreCarte; i++ )
		Projet_Commun_Carte_BCarte_Detruire( &(*this)->m_carte[ i ] );
	NFREE( (*this)->m_carte );
			
	// Lib�rer
	NFREE( (*this) );
}

/* Obtenir carte */
const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( const BEnsembleCarte *this,
	NU32 index )
{
	// V�rifier
	if( index >= this->m_nombreCarte )
		return NULL;

	// OK
	return this->m_carte[ index ];
}

const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir2( const BEnsembleCarte *this,
	NU32 identifiant )
{
	// It�rateur
	NU32 i = 0;

	// Chercher
	for( ; i< this->m_nombreCarte; i++ )
		if( Projet_Commun_Carte_BCarte_ObtenirHandle( this->m_carte[ i ] ) == identifiant )
			return this->m_carte[ i ];

	// Introuvable
	return NULL;
}

/* Obtenir nombre carte */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirNombre( const BEnsembleCarte *this )
{
	return this->m_nombreCarte;
}

/* Obtenir checksum */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirChecksum( const BEnsembleCarte *this )
{
	return this->m_checksum;
}

