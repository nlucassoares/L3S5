#include "../../../../../include/Projet/Projet.h"

// --------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatClient
// --------------------------------------------------

/* Construire */
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire( NU32 identifiant,
	NBOOL estClientCourant,
	void *donneeSupplementaire,
	const struct BRessource *ressource )
{
	// Sortie
	__OUTPUT BEtatClient *out;

	// Personnage
	const BPersonnage *personnage;

	// Charset
	const BCharset *charset;

	// Animation
	const NAnimation *animation;

	// Etat animation
	const NEtatAnimation *etatAnimation;

	// Obtenir ressources
	if( !( personnage = Projet_Commun_Ressource_BRessource_ObtenirPersonnage( ressource,
			0 ) )
		|| !( charset = Projet_Commun_Personnage_BPersonnage_ObtenirCharset( personnage ) )
		|| !( animation = Projet_Commun_Personnage_Charset_BCharset_ObtenirAnimationMort( charset ) )
		|| !( etatAnimation = NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEtatClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire �tat d�placement
	if( !( out->m_etatDeplacement = Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire animation mort
	if( !( out->m_etatAnimationMort = NLib_Temps_Animation_NEtatAnimation_Construire2( etatAnimation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// D�truire
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &out->m_etatDeplacement );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Z�ro
	out->m_estPret = BETAT_PRET_PAS_PRET;
	out->m_nombreBombePosee = 0;
	out->m_puissance = BPUISSANCE_INITIALE_JOUEUR;
	out->m_nombreBombeMaximum = BNOMBRE_BOMBE_INITIAL_JOUEUR;
	out->m_estEnVie = NTRUE;
	out->m_estAnimationMortEnCours = NFALSE;

	// Enregistrer
	out->m_identifiant = identifiant;
	out->m_estClientCourant = estClientCourant;
	out->m_tickConnexion = NLib_Temps_ObtenirTick( );
	out->m_donneeSupplementaire = donneeSupplementaire;

	// OK
	return out;
}

__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire2( const BEtatClient *src )
{
	// Sortie
	__OUTPUT BEtatClient *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEtatClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		src,
		sizeof( BEtatClient ) );

	// Allouer nom
	if( src->m_nom != NULL )
		if( !( out->m_nom = calloc( strlen( src->m_nom ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Lib�rer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Dupliquer �tat d�placement
	if( !( out->m_etatDeplacement = Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire2( src->m_etatDeplacement ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer animation mort
	if( !( out->m_etatAnimationMort = NLib_Temps_Animation_NEtatAnimation_Construire2( src->m_etatAnimationMort ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &out->m_etatDeplacement );
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer nom
	if( src->m_nom != NULL)
		memcpy( out->m_nom,
			src->m_nom,
			strlen( src->m_nom ) );

	// OK
	return out;
}

/* D�truire */
void Projet_Commun_Reseau_Client_BEtatClient_Detruire( BEtatClient **this )
{
	// D�truire �tat animation mort
	NLib_Temps_Animation_NEtatAnimation_Detruire( &(*this)->m_etatAnimationMort );

	// D�truire �tat d�placement
	Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &(*this)->m_etatDeplacement );

	// Lib�rer nom
	NFREE( (*this)->m_nom );

	// Lib�rer
	NFREE( *this );
}

/* Obtenir nom */
const char *Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( const BEtatClient *this )
{
	return this->m_nom;
}

/* Obtenir identifiant */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( const BEtatClient *this )
{
	return this->m_identifiant;
}

/* Obtenir charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( const BEtatClient *this )
{
	return this->m_charset;
}

/* Obtenir couleur charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( const BEtatClient *this )
{
	return this->m_couleurCharset;
}

/* Obtenir position */
const NSPoint *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( const BEtatClient *this )
{
	return &this->m_position;
}

/* Est confirm� le lancement? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstConfirmeLancement( const BEtatClient *this )
{
	return this->m_estConfirmeLancement;
}

/* Obtenir donn�e suppl�mentaire */
void *Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( const BEtatClient *this )
{
	return this->m_donneeSupplementaire;
}

/* Obtenir �tat d�placement */
const BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( const BEtatClient *this )
{
	return this->m_etatDeplacement;
}

/* Obtenir �tat animation */
const NEtatAnimation *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatAnimationMort( const BEtatClient *this )
{
	return this->m_etatAnimationMort;
}

/* Obtenir nombre bombes pos�es */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreBombePosee( const BEtatClient *this )
{
	return this->m_nombreBombePosee;
}

/* Obtenir nombre maximum bombes */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreMaximumBombe( const BEtatClient *this )
{
	return this->m_nombreBombeMaximum;
}

/* Est client v�rifi�? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstVerifie( const BEtatClient *this )
{
	return this->m_estChecksumVerifie;
}

/* On doit afficher ce client? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstDoitAfficher( const BEtatClient *this )
{
	return this->m_estEnVie
		|| !NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( this->m_etatAnimationMort );
}

/* V�rifier �tat client */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( const BEtatClient *this )
{
	// V�rifier �tat
	return ( this->m_estChecksumVerifie
		|| NLib_Temps_ObtenirTick( ) - this->m_tickConnexion < BDELAI_KICK_CLIENT_NON_VERIFIE );
}

/* Obtenir tick connexion */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirTickConnexion( const BEtatClient *this )
{
	return this->m_tickConnexion;
}

/* Est pr�t? */
BEtatPret Projet_Commun_Reseau_Client_BEtatClient_EstPret( const BEtatClient *this )
{
	return this->m_estPret;
}

/* Confirmer le lancement */
void Projet_Commun_Reseau_Client_BEtatClient_ConfirmerLancement( BEtatClient *this )
{
	this->m_estConfirmeLancement = NTRUE;
}

/* D�finir nom */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( BEtatClient *this,
	const char *nom )
{
	// Nouveau nom
	char *nouveauNom;

	// Allouer
	if( !( nouveauNom = calloc( strlen( nom ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier
	memcpy( nouveauNom,
		nom,
		strlen( nom ) );

	// Lib�rer
	NFREE( this->m_nom );

	// Enregistrer
	this->m_nom = nouveauNom;

	// OK
	return NTRUE;
}

/* D�finir charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( BEtatClient *this,
	NU32 charset )
{
	this->m_charset = charset;
}

/* D�finir couleur charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( BEtatClient *this,
	NU32 couleur )
{
	this->m_couleurCharset = couleur;
}

/* D�finir donn�e suppl�mentaire */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDonneeSupplementaire( BEtatClient *this,
	void *data )
{
	// Enregistrer
	this->m_donneeSupplementaire = data;
}

/* Obtenir puissance */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( const BEtatClient *this )
{
	return this->m_puissance;
}

/* D�finir �tat v�rification */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( BEtatClient *this,
	NBOOL estVerifie )
{
	this->m_estChecksumVerifie = estVerifie;
}

/* D�finir est pr�t */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( BEtatClient *this,
	BEtatPret etat )
{
	this->m_estPret = etat;
}

/* D�finir position */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( BEtatClient *this,
	NSPoint position )
{
	this->m_position = position;
}

/* D�finir direction */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( BEtatClient *this,
	NDirection direction )
{
	this->m_direction = direction;
}

/* D�finir puissance */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( BEtatClient *this,
	NU32 puissance )
{
	this->m_puissance = puissance;
}

/* D�finir est en vie */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( BEtatClient *this,
	NBOOL estEnVie )
{
	// Est en vie?
	this->m_estEnVie = estEnVie;
}

/* Est en vie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( const BEtatClient *this )
{
	return this->m_estEnVie;
}

/* Obtenir direction */
NDirection Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( const BEtatClient *this )
{
	return this->m_direction;
}

/* Incr�menter nombre bombes maximum */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombeMaximum( BEtatClient *this )
{
	if( this->m_nombreBombeMaximum < BNOMBRE_BOMBE_MAXIMUM_JOUEUR )
		this->m_nombreBombeMaximum++;
}

/* Incr�menter/d�cr�menter nombre bombes pos�es */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( BEtatClient *this )
{
	this->m_nombreBombePosee++;
}

void Projet_Commun_Reseau_Client_BEtatClient_DecrementerNombreBombePosee( BEtatClient *this )
{
	// V�rifier que le nombre de bombes soit correct
	if( this->m_nombreBombePosee > 0 )
		// D�cr�menter
		this->m_nombreBombePosee--;
	else
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );
}

/* Lancer l'animation de mort */
void Projet_Commun_Reseau_Client_BEtatClient_LancerAnimationMort( BEtatClient *this )
{
	// Activer l'animation
	this->m_estAnimationMortEnCours = NTRUE;

	// Mettre l'animation � z�ro
	NLib_Temps_Animation_NEtatAnimation_RemettreAZero( this->m_etatAnimationMort );
}

/* Update */
void Projet_Commun_Reseau_Client_BEtatClient_Update( BEtatClient *this )
{
	// Mettre � jour d�placement
	Projet_Commun_Reseau_Client_BEtatDeplacementClient_Update( this->m_etatDeplacement );

	// Mettre � jour l'animation de mort si n�cessaire
	if( this->m_estAnimationMortEnCours
		&& !NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( this->m_etatAnimationMort ) )
		// Mettre � jour
		NLib_Temps_Animation_NEtatAnimation_Update( this->m_etatAnimationMort );
}

