#include "../../../../../../include/Projet/Projet.h"

// -------------------------------------------------
// namespace Projet::Commun::Reseau::Packet::Lecteur
// -------------------------------------------------

/* Lire donn�e (priv�e) */
NBOOL Projet_Commun_Reseau_Packet_Lecteur_LireDonneeInterne( const char *src,
	NU32 tailleData,
	NU32 *curseur,
	char *dst,
	NU32 tailleALire )
{
	// V�rifier
	if( *curseur + tailleALire > tailleData )
		return NFALSE;

	// Lire
	memcpy( dst,
		src + *curseur,
		tailleALire );

	// Incr�menter curseur
	*curseur += tailleALire;

	// OK
	return NTRUE;
}

// Param�tres
#define PARAMETRES	\
	const char *donnee, \
	NU32 *curseur, \
	NU32 tailleData

// Lire donn�e
#define LIRE( dst, tailleALire ) \
	Projet_Commun_Reseau_Packet_Lecteur_LireDonneeInterne( donnee, \
		tailleData, \
		curseur, \
		(char*)dst, \
		tailleALire )

// Erreur type
#define ERREUR_TYPE( ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR ); \
 \
		/* Quitter */ \
		return BTYPE_PACKET_AUCUN; \
	}

// Erreur
#define ERREUR( ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR ); \
 \
		/* Lib�rer */ \
		NFREE( out ); \
 \
		/* Quitter */ \
		return NULL; \
	}

/* Lire type */
#define DEBUT_FONCTION( tailleNormalePacket ) \
	if( tailleData < tailleNormalePacket ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR ); \
 \
		/* Quitter */ \
		return BTYPE_PACKET_AUCUN; \
	}

BTypePacket Projet_Commun_Reseau_Packet_Lecteur_LireType( PARAMETRES )
{
	// Type
	__OUTPUT BTypePacket sortie;

	// Initialiser
	DEBUT_FONCTION( sizeof( BTypePacket ) );

	// Lire
	if( !LIRE( &sortie,
			sizeof( BTypePacket ) ) )
		ERREUR_TYPE( );

	// OK
	return sortie;
}

#undef DEBUT_FONCTION

// V�rification taille
#define VERIFIER_TAILLE_BOOL( tailleNormalePacket ) \
	tailleNormalePacket != 0 ? ( tailleData < tailleNormalePacket + sizeof( NU32 ) /* Type packet */ ) : NFALSE

#define VERIFIER_TAILLE( tailleNormalePacket ) \
	if( VERIFIER_TAILLE_BOOL( tailleNormalePacket ) ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR ); \
 \
		/* Quitter */ \
		return NULL; \
	}

// D�finition/Allocation
#define DEBUT_FONCTION( type, tailleNormalePacket ) \
	/* Sortie */ \
	__OUTPUT struct type *out; \
 \
	/* V�rifier taille */ \
	VERIFIER_TAILLE( tailleNormalePacket ); \
 \
	/* Allouer la m�moire */ \
	if( !( out = calloc( 1, \
		sizeof( struct type ) ) ) ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED ); \
 \
		/* Quitter */ \
		return NULL; \
	}

/* Lire packet */
// --------------------
// Client vers serveur:
// --------------------
// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR
struct BPacketClientServeurTransmetInformationsJoueur *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurTransmetInformationsJoueur( PARAMETRES )
{
	// Taille nom
	NU32 tailleNom;

	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurTransmetInformationsJoueur,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &tailleNom,
			sizeof( NU32 ) ) )
		ERREUR( );

	// V�rifier taille
	VERIFIER_TAILLE( sizeof( NU32 ) + tailleNom + sizeof( NU32 ) + sizeof( NU32 ) );

	// Lire
		// Nom
			// Allouer
				if( !( out->m_nom = calloc( tailleNom + 1,
					sizeof( char ) ) ) )
					ERREUR( );
			// Copier
					// Nom
					if( !LIRE( out->m_nom,
							tailleNom )
					// ID charset
						|| !LIRE( &out->m_charset,
								sizeof( NU32 ) )
					// ID couleur charset
						|| !LIRE( &out->m_couleurCharset,
								sizeof( NU32 ) ) )
					{
						// Lib�rer
						NFREE( out->m_nom );

						// Erreur
						ERREUR( );
					}

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM
__ALLOC struct BPacketClientServeurTransmetChecksum *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurTransmetChecksum( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurTransmetChecksum,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_checksum,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PING
__ALLOC struct BPacketClientServeurPing *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPing( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurPing,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PONG
__ALLOC struct BPacketClientServeurPong *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPong( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurPong,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET
__ALLOC struct BPacketClientServeurChangeEtatPret *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangeEtatPret( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurChangeEtatPret,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT
__ALLOC struct BPacketClientServeurConfirmeLancement *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurConfirmeLancement( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurConfirmeLancement,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

__ALLOC struct BPacketClientServeurChangeDirection *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangeDirection( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurChangeDirection,
		sizeof( NDirection ) );

	// Lire identifiant
	if( !LIRE( &out->m_direction,
			sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION
__ALLOC struct BPacketClientServeurChangePosition *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangePosition( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurChangePosition,
		sizeof( NDirection ) );

	// Lire identifiant
	if( !LIRE( &out->m_direction,
			sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE
__ALLOC struct BPacketClientServeurPoseBombe *Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPoseBombe( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketClientServeurPoseBombe,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// --------------------
// Serveur vers client:
// --------------------
// BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT
__ALLOC struct BPacketServeurClientConnexionTransmetIdentifiant *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientConnexionTransmetIdentifiant( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientConnexionTransmetIdentifiant,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR
__ALLOC struct BPacketServeurClientReponseInformationsJoueur *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientReponseInformationsJoueur( PARAMETRES )
{
	// Taille nom
	NU32 tailleNom;

	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientReponseInformationsJoueur,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &tailleNom,
			sizeof( NU32 ) ) )
		ERREUR( );

	// V�rifier taille
	VERIFIER_TAILLE( sizeof( NU32 ) + tailleNom + sizeof( NU32 ) + sizeof( NU32 ) + sizeof( NU32 ) );

	// Lire
		// Nom
			// Allouer
				if( !( out->m_nom = calloc( tailleNom + 1,
					sizeof( char ) ) ) )
					ERREUR( );
			// Copier
					// Nom
					if( !LIRE( out->m_nom,
							tailleNom )
					// ID charset
						|| !LIRE( &out->m_charset,
								sizeof( NU32 ) )
					// ID couleur charset
						|| !LIRE( &out->m_couleurCharset,
								sizeof( NU32 ) )
					// Identifiant
						|| !LIRE( &out->m_identifiant,
								sizeof( NU32 ) ) )
					{
						// Lib�rer
						NFREE( out->m_nom );

						// Erreur
						ERREUR( );
					}

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR
__ALLOC struct BPacketServeurClientDeconnexionJoueur *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDeconnexionJoueur( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientDeconnexionJoueur,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT
__ALLOC struct BPacketServeurClientChecksumIncorrect *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientChecksumIncorrect( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientChecksumIncorrect,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_PING
__ALLOC struct BPacketServeurClientPing *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientPing( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientPing,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_PONG
__ALLOC struct BPacketServeurClientPong *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientPong( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientPong,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET
__ALLOC struct BPacketServeurClientDiffuseEtatPret *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseEtatPret( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientDiffuseEtatPret,
		sizeof( NU32 ) + sizeof( BEtatPret ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
			sizeof( NU32 ) )
		|| !LIRE( &out->m_etat,
				sizeof( BEtatPret ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE
__ALLOC struct BPacketServeurClientDiffuseChangementCarte *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementCarte( PARAMETRES )
{
	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientDiffuseChangementCarte,
		sizeof( NU32 ) );

	// Lire taille nom
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT
__ALLOC struct BPacketServeurClientDiffuseLancement *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseLancement( PARAMETRES )
{
	// It�rateurs
	NU32 i,
		j;

	// Taille actuelle
	NU32 tailleActuelle = sizeof( NU32 );

	// D�claration/Allocation
	DEBUT_FONCTION( BPacketServeurClientDiffuseLancement,
		tailleActuelle );

	// Lire joueurs
		// Nombre
			if( !LIRE( &out->m_nombreJoueur,
				sizeof( NU32 ) ) )
				ERREUR( );
		// V�rifier taille
			VERIFIER_TAILLE( ( tailleActuelle += ( ( sizeof( NU32 ) + sizeof( NU32 ) * 2 ) * out->m_nombreJoueur ) ) );
		// Allouer la m�moire
			if( !( out->m_identifiantJoueur = calloc( out->m_nombreJoueur,
					sizeof( NU32 ) ) )
				|| !( out->m_positionJoueur = calloc( out->m_nombreJoueur,
					sizeof( NSPoint ) ) ) )
			{
				// Lib�rer
				NFREE( out->m_identifiantJoueur );

				// Quitter
				ERREUR( );
			}
		// Lire identifiant/position
			for( i = 0; i < out->m_nombreJoueur; i++ )
				// Identifiant
				if( !LIRE( &out->m_identifiantJoueur[ i ],
					sizeof( NU32 ) )
				// Position
					// x
						|| !LIRE( &out->m_positionJoueur[ i ].x,
							sizeof( NS32 ) )
					// y
						|| !LIRE( &out->m_positionJoueur[ i ].y,
							sizeof( NS32 ) ) )
				{
					// Lib�rer
					NFREE( out->m_identifiantJoueur );
					NFREE( out->m_positionJoueur );

					// Quitter
					ERREUR( );
				}

	// Lire carte
		// V�rifier taille
			if( !VERIFIER_TAILLE_BOOL( ( tailleActuelle += ( sizeof( NU32 ) + sizeof( NU32 ) * 2 ) ) ) )
			{
				// Lib�rer
				NFREE( out->m_identifiantJoueur );
				NFREE( out->m_positionJoueur );

				// Quitter
				ERREUR( );
			}
		// Identifiant
			if( !LIRE( &out->m_identifiantCarte,
					sizeof( NU32 ) )
				|| !LIRE( &out->m_tailleCarte.x,
					sizeof( NU32 ) )
				|| !LIRE( &out->m_tailleCarte.y,
					sizeof( NU32 ) ) )
			{
				// Lib�rer
				NFREE( out->m_identifiantJoueur );
				NFREE( out->m_positionJoueur );

				// Quitter
				ERREUR( );
			}
		// V�rifier la taille
			if( !VERIFIER_TAILLE_BOOL( ( tailleActuelle += ( sizeof( NBOOL ) * out->m_tailleCarte.x * out->m_tailleCarte.y ) ) ) )
			{
				// Lib�rer
				NFREE( out->m_identifiantJoueur );
				NFREE( out->m_positionJoueur );

				// Quitter
				ERREUR( );
			}
		// Allouer la m�moire
			// 1D
				if( !( out->m_estCaseRemplie = calloc( out->m_tailleCarte.x,
					sizeof( NBOOL* ) ) ) )
				{
					// Lib�rer
					NFREE( out->m_identifiantJoueur );
					NFREE( out->m_positionJoueur );

					// Quitter
					ERREUR( );
				}
			// 2D
				for( i = 0; i < out->m_tailleCarte.x; i++ )
					if( !( out->m_estCaseRemplie[ i ] = calloc( out->m_tailleCarte.y,
						sizeof( NBOOL ) ) ) )
					{
						// Lib�rer
						for( j = 0; j < i; j++ )
							NFREE( out->m_estCaseRemplie[ j ] );
						NFREE( out->m_estCaseRemplie );
						NFREE( out->m_identifiantJoueur );
						NFREE( out->m_positionJoueur );

						// Quitter
						ERREUR( );
					}
		// Est rempli?
			for( i = 0; i < out->m_tailleCarte.x; i++ )
				if( !LIRE( out->m_estCaseRemplie[ i ],
					sizeof( NBOOL ) * out->m_tailleCarte.y ) )
				{
					// Lib�rer
					for( i = 0; i < out->m_tailleCarte.x; i++ )
						NFREE( out->m_estCaseRemplie[ i ] );
					NFREE( out->m_estCaseRemplie );
					NFREE( out->m_identifiantJoueur );
					NFREE( out->m_positionJoueur );

					// Quitter
					ERREUR( );
				}

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER
__ALLOC struct BPacketServeurClientDiffuseMessageAfficher *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseMessageAfficher( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseMessageAfficher,
		sizeof( NU32 ) );

	// Lire taille message
	if( !LIRE( &out->m_tailleMessage,
		sizeof( NU32 ) ) )
		ERREUR( );

	// Allouer la m�moire
	if( !( out->m_message = calloc( out->m_tailleMessage + 1,
		sizeof( char ) ) ) )
		ERREUR( );

	// Lire message
	if( !LIRE( out->m_message,
		out->m_tailleMessage * sizeof( char ) ) )
	{
		// Lib�rer
		NFREE( out->m_message );

		// Quitter
		ERREUR( );
	}

	// Lire dur�e affichage
	if( !LIRE( &out->m_dureeAffichage,
		sizeof( NU32 ) )
	// Police
		|| !LIRE( &out->m_police,
			sizeof( BPoliceMessageClient ) )
	// Couleur
		|| !LIRE( &out->m_couleur.r,
			sizeof( NU8 ) )
		|| !LIRE( &out->m_couleur.g,
			sizeof( NU8 ) )
		|| !LIRE( &out->m_couleur.b,
			sizeof( NU8 ) )
	// Position
		|| !LIRE( &out->m_position,
			sizeof( BPositionAffichageMessageClient ) )
	// Est doit afficher cadre?
		|| !LIRE( &out->m_estDoitAfficherCadre,
			sizeof( NBOOL ) )
	// Couleur cadre
		|| !LIRE( &out->m_couleurCadre.r,
			sizeof( NU8 ) )
		|| !LIRE( &out->m_couleurCadre.g,
			sizeof( NU8 ) )
		|| !LIRE( &out->m_couleurCadre.b,
			sizeof( NU8 ) )
		|| !LIRE( &out->m_couleurCadre.a,
			sizeof( NU8 ) ) )
	{
		// Lib�rer
		NFREE( out->m_message );

		// Quitter
		ERREUR( );
	}

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE
__ALLOC struct BPacketServeurClientDiffuseDebutPartie *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseDebutPartie( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseDebutPartie,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION
__ALLOC struct BPacketServeurClientDiffuseChangementDirection *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementDirection( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseChangementDirection,
		sizeof( NU32 ) + sizeof( NDirection ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) )
		// Direction
		|| !LIRE( &out->m_direction,
			sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION
__ALLOC struct BPacketServeurClientDiffuseChangementPosition *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementPosition( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseChangementPosition,
		sizeof( NU32 ) + sizeof( NDirection ) + sizeof( NS32 ) * 2 );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) )
		// Direction
		|| !LIRE( &out->m_direction,
			sizeof( NDirection ) )
		// Position
		|| !LIRE( &out->m_nouvellePosition.x,
			sizeof( NS32 ) )
		|| !LIRE( &out->m_nouvellePosition.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE
__ALLOC struct BPacketServeurClientDiffusePoseBombe *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffusePoseBombe( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffusePoseBombe,
		sizeof( NU32 ) * 2 + sizeof( NS32 ) * 2 );

	// Lire identifiant joueur
	if( !LIRE( &out->m_identifiantJoueur,
		sizeof( NU32 ) )
		// Identifiant bombe
		|| !LIRE( &out->m_identifiantBombe,
			sizeof( NU32 ) )
		// Position
		|| !LIRE( &out->m_position.x,
			sizeof( NS32 ) )
		|| !LIRE( &out->m_position.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE
__ALLOC struct BPacketServeurClientRefusePoseBombe *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientRefusePoseBombe( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientRefusePoseBombe,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE
__ALLOC struct BPacketServeurClientBombeExplose *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientBombeExplose( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientBombeExplose,
		sizeof( NU32 ) * 3 );

	// Lire identifiant bombe
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) )
		// Puissance
		|| !LIRE( &out->m_puissance,
			sizeof( NU32 ) )
		// Identifiant joueur
		|| !LIRE( &out->m_identifiantJoueur,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT
__ALLOC struct BPacketServeurClientBlocRempliDetruit *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientBlocRempliDetruit( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientBlocRempliDetruit,
		sizeof( NS32 ) * 2 );

	// Lire position
	if( !LIRE( &out->m_position.x,
		sizeof( NS32 ) )
		|| !LIRE( &out->m_position.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS
__ALLOC struct BPacketServeurClientDiffuseApparitionBonus *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseApparitionBonus( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseApparitionBonus,
		( sizeof( NS32 ) * 2 ) + sizeof( BListeBonus ) + sizeof( NU32 ) * 2 );

	// Lire position
	if( !LIRE( &out->m_position.x,
		sizeof( NS32 ) )
		|| !LIRE( &out->m_position.y,
			sizeof( NS32 ) )
		// Type bonus
		|| !LIRE( &out->m_type,
			sizeof( BListeBonus ) )
		// Dur�e bonus
		|| !LIRE( &out->m_duree,
			sizeof( NU32 ) )
		// Identifiant bonus
		|| !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS
__ALLOC struct BPacketServeurClientDiffuseDisparitionBonus *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseDisparitionBonus( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseDisparitionBonus,
		sizeof( NU32 ) );

	// Lire identifiant
	if( !LIRE( &out->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS
__ALLOC struct BPacketServeurClientDistribueBonus *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDistribueBonus( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDistribueBonus,
		sizeof( NU32 ) * 2 );

	// Lire type
	if( !LIRE( &out->m_typeBonus,
		sizeof( BListeBonus ) )
		// Identifiant joueur
		|| !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR
__ALLOC struct BPacketServeurClientDiffuseMortJoueur *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseMortJoueur( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientDiffuseMortJoueur,
		sizeof( NU32 ) );

	// Lire identifiant joueur
	if( !LIRE( &out->m_identifiantJoueur,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

// BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE
__ALLOC struct BPacketServeurClientAnnonceFinPartie *Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientAnnonceFinPartie( PARAMETRES )
{
	// D�but fonction
	DEBUT_FONCTION( BPacketServeurClientAnnonceFinPartie,
		sizeof( BTypeFinPartie ) + sizeof( NU32 ) );

	// Lire type fin de partie
	if( !LIRE( &out->m_type,
		sizeof( BTypeFinPartie ) )
		// Identifiant
		|| !LIRE( &out->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return out;
}

