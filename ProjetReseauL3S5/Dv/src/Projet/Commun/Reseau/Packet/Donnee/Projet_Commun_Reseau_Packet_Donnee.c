#include "../../../../../../include/Projet/Projet.h"

// ------------------------------------------------
// namespace Projet::Commun::Reseau::Packet::Donnee
// ------------------------------------------------

/* Lib�rer une structure de donn�es de packet */
void Projet_Commun_Reseau_Packet_Donnee_Liberer( void *data,
	BTypePacket type )
{
	// Lib�rer contenu
	Projet_Commun_Reseau_Packet_Donnee_Liberer2( data,
		type );

	// Lib�rer conteneur
	NFREE( data );
}

void Projet_Commun_Reseau_Packet_Donnee_Liberer2( void *data,
	BTypePacket type )
{
	// It�rateur
	NU32 i;

	switch( type )
	{
		case BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR:
			NFREE( ((struct BPacketServeurClientReponseInformationsJoueur*)data)->m_nom );
			break;

		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR:
			NFREE( ((struct BPacketClientServeurTransmetInformationsJoueur*)data)->m_nom );
			break;

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT:
			// Joueur
			NFREE( ((struct BPacketServeurClientDiffuseLancement*)data)->m_identifiantJoueur );
			NFREE( ((struct BPacketServeurClientDiffuseLancement*)data)->m_positionJoueur );

			// Carte
			for( i = 0; i < ((struct BPacketServeurClientDiffuseLancement*)data)->m_tailleCarte.x; i++ )
				NFREE( ((struct BPacketServeurClientDiffuseLancement*)data)->m_estCaseRemplie[ i ] );
			NFREE( ((struct BPacketServeurClientDiffuseLancement*)data)->m_estCaseRemplie );
			break;

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER:
			NFREE( ((struct BPacketServeurClientDiffuseMessageAfficher*)data)->m_message );
			break;

		case BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR:
		case BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS:
		case BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT:
		case BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE:
		case BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE:
		case BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION:
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION:
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE:
		case BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE:
		case BTYPE_PACKET_SERVEUR_CLIENT_PING:
		case BTYPE_PACKET_SERVEUR_CLIENT_PONG:
		case BTYPE_PACKET_CLIENT_SERVEUR_PING:
		case BTYPE_PACKET_CLIENT_SERVEUR_PONG:
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM:
		case BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR:
		case BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT:
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET:
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET:
		default:
			break;
	}
}

