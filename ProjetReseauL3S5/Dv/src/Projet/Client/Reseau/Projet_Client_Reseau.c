#include "../../../../include/Projet/Projet.h"

// --------------------------------
// namespace Projet::Client::Reseau
// --------------------------------

/* Callback r�ception packet */
__CALLBACK NBOOL Projet_Client_Reseau_CallbackReceptionPacket( const NPacket *packet,
	void *client )
{
	// Donn�es
	char *donnee;

	// Type
	BTypePacket type;

	// Lire
	if( !( donnee = Projet_Commun_Reseau_Lire( packet,
		&type ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Communiquer au client
	if( !Projet_Client_BClient_TraiterPacket( client,
		donnee,
		type ) )
		return NFALSE;

	// Lib�rer
	Projet_Commun_Reseau_Packet_Donnee_Liberer( donnee,
		type );

	// OK
	return NTRUE;
}

/* Callback d�connexion */
NBOOL Projet_Client_Reseau_CallbackDeconnexion( void *client )
{
	// Traiter
	Projet_Client_BClient_TraiterDeconnexion( client );

	// OK
	return NTRUE;
}

