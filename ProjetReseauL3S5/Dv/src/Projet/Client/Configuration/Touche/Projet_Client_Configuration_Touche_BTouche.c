#define PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ---------------------------------------------------
// enum Projet::Client::Configuration::Touche::BTouche
// ---------------------------------------------------

/* Obtenir touche par d�faut */
SDL_Keycode Projet_Client_Configuration_Touche_BTouche_ObtenirToucheDefaut( BTouche touche )
{
	return BToucheDefaut[ touche ];
}

