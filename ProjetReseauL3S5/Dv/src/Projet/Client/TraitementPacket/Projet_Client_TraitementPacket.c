#include "../../../../include/Projet/Projet.h"

// ------------------------------------------
// namespace Projet::Client::TraitementPacket
// ------------------------------------------

/* Envoyer requ�te ping */
NBOOL Projet_Client_TraitementPacket_EnvoyerRequetePing( BClient *this )
{
	// Donn�e
	struct BPacketClientServeurPing data;

	// Packet
	NPacket *packet;

	// Composer packet
	data.m_identifiant = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient );

	// Cr�er le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_PING,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer les informations du joueur (priv�e) */
NBOOL Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( BClient *this,
	const char *nom,
	NU32 charset,
	NU32 couleurCharset )
{
	// Donn�es
	struct BPacketClientServeurTransmetInformationsJoueur data;

	// Packet
	NPacket *packet;

	// Allouer la m�moire
	if( !( data.m_nom = calloc( strlen( nom ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier donn�es
	data.m_charset = charset;
	data.m_couleurCharset = couleurCharset;
	memcpy( data.m_nom,
		nom,
		strlen( nom ) );

	// Cr�er le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer changement �tat pr�t */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementEtatPret( BClient *this )
{
	// Packet
	NPacket *packet;

	// Donn�e
	struct BPacketClientServeurChangeEtatPret data;

	// Composer packet
	data.m_identifiant = Projet_Client_BClient_ObtenirIdentifiantJoueurCourant( this );

	// Cr�er packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer lancement partie effectu� */
NBOOL Projet_Client_TraitementPacket_EnvoyerLancementEffectue( BClient *this )
{
	// Packet
	NPacket *packet;

	// Donn�es
	struct BPacketClientServeurConfirmeLancement data;

	// Composer
	data.m_identifiant = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient );

	// Cr�er le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer changement direction */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementDirection( BClient *this,
	NDirection direction )
{
	// Packet
	NPacket *packet;

	// Donn�e
	struct BPacketClientServeurChangeDirection data;

	// Composer
	data.m_direction = direction;

	// Cr�er packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer changement position */
NBOOL Projet_Client_TraitementPacket_EnvoyerChangementPosition( BClient *this,
	NDirection direction )
{
	// Packet
	NPacket *packet;

	// Donn�e
	struct BPacketClientServeurChangeDirection data;

	// Composer
	data.m_direction = direction;

	// Cr�er packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Envoyer pose bombe */
NBOOL Projet_Client_TraitementPacket_EnvoyerPoseBombe( BClient *this )
{
	// Packet
	NPacket *packet;

	// Donn�es
	struct BPacketClientServeurPoseBombe data;

	// Composer le packet
	data.m_identifiant = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient );

	// Cr�er le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT
NBOOL Projet_Client_TraitementPacket_TraiterConnexionTransmetIdentifiant( BClient *this,
	const struct BPacketServeurClientConnexionTransmetIdentifiant *data )
{
	// Donn�es
	struct BPacketClientServeurTransmetChecksum dataReponse;

	// Packet
	NPacket *packet;

	// Joueur
	BEtatClient *joueur;

	// Prot�ger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// D�j� des joueurs dans le cache (donc client externe)
	if( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ) > 0 )
	{
		// V�rifier que le client soit absent du cache
		if( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
			data->m_identifiant ) != NULL )
		{
			// Ne plus prot�ger
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Le client existe d�j�
			return NTRUE;
		}

		// Ajouter client externe
		if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientExterne( this->m_cacheClient,
			data->m_identifiant,
			NULL ) ) )
		{
			// Ne plus prot�ger
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}
	}
	// Aucun joueur (donc on est le joueur courant)
	else
	{
		// Ajouter client courant
		if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientCourant( this->m_cacheClient,
			data->m_identifiant,
			NULL ) ) )
		{
			// Ne plus prot�ger
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}

		// Envoyer checksum ressources
			// Composer
				dataReponse.m_checksum = Projet_Client_BClient_ObtenirChecksumRessource( this );
			// Cr�er packet
				if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM,
					&dataReponse ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Ne plus prot�ger
					Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

					// Quitter
					return NFALSE;
				}
			// Envoyer
				if( !Projet_Client_BClient_EnvoyerPacket( this,
					packet ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

					// Ne plus prot�ger
					Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );
					
					// Quitter
					return NFALSE;
				}
	}

	// Client v�rifi�
	Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( joueur,
		NTRUE );

	// Ne plus prot�ger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterReponseInformationsJoueur( BClient *this,
	const struct BPacketServeurClientReponseInformationsJoueur *data )
{
	// Modifier
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation2( this->m_cacheClient,
		data->m_identifiant,
		data->m_charset,
		data->m_couleurCharset,
		data->m_nom ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Le client est v�rifi�
	Projet_Commun_Reseau_Client_Cache_BCacheClient_DefinirClientVerifie( this->m_cacheClient,
		data->m_identifiant );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterDeconnexionJoueur( BClient *this,
	const struct BPacketServeurClientDeconnexionJoueur *data )
{
	// Supprimer du cache
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClient( this->m_cacheClient,
		data->m_identifiant ) )
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT
NBOOL Projet_Client_TraitementPacket_TraiterChecksumIncorrect( BClient *this,
	const struct BPacketServeurClientChecksumIncorrect *data )
{
	// Buffer
	char buffer[ 2048 ];

	// R�f�rencer
	NREFERENCER( data );

	// Notifier
		// Composer
			sprintf( buffer,
				"[CLIENT] Le checksum des ressources est incorrect. Mettre a jour les assets." );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// D�connecter
	return Projet_Client_BClient_Deconnecter( this );
}

// BTYPE_PACKET_SERVEUR_CLIENT_PING
NBOOL Projet_Client_TraitementPacket_TraiterPing( BClient *this,
	const struct BPacketServeurClientPing *data )
{
	// Donn�e
	struct BPacketClientServeurPong dataReponse;

	// Packet
	NPacket *packet;

	// R�f�rencer
	NREFERENCER( data );

	// Composer
	dataReponse.m_identifiant = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient );

	// Cr�er le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_CLIENT_SERVEUR_PONG,
		&dataReponse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Client_BClient_EnvoyerPacket( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_PONG
NBOOL Projet_Client_TraitementPacket_TraiterPong( BClient *this,
	const struct BPacketServeurClientPong *data )
{
	// R�f�rencer
	NREFERENCER( data );

	// Enregistrer la r�ponse
	if( !NLib_Module_Reseau_NPing_RecevoirReponseRequete( this->m_ping ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseEtatPret( BClient *this,
	const struct BPacketServeurClientDiffuseEtatPret *data )
{
	// Enregistrer
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret2( this->m_cacheClient,
		data->m_identifiant,
		data->m_etat ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementCarte( BClient *this,
	const struct BPacketServeurClientDiffuseChangementCarte *data )
{
	// Enregistrer
	Projet_Commun_Monde_BConfigurationMonde_DefinirIdentifiantCarte( this->m_configurationMonde,
		data->m_identifiant );

	// Mettre � jour cadre menu rejoindre
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreCarte( this->m_menuRejoindre );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseLancement( BClient *this,
	const struct BPacketServeurClientDiffuseLancement *data )
{
	// Joueur
	BEtatClient *joueur;

	// Carte
	const BCarte *carte = NULL;

	// It�rateur
	NU32 i;

	// Prot�ger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// V�rifier nombre clients
	if( data->m_nombreJoueur != Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient )
		|| !( carte = Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( this->m_ensembleCarte,
			data->m_identifiantCarte ) )
		|| Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x != data->m_tailleCarte.x
		|| Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y != data->m_tailleCarte.y )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus prot�ger cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Enregistrer carte
	Projet_Commun_Monde_BConfigurationMonde_DefinirIdentifiantCarte( this->m_configurationMonde,
		data->m_identifiantCarte );

	// Construire le monde
	if( !( this->m_monde = Projet_Commun_Monde_BMonde_Construire( this->m_cacheClient,
		this->m_configurationMonde,
		this->m_ensembleCarte,
		this->m_ressource ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Ne plus prot�ger cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Copier blocs remplis
	Projet_Commun_Monde_BMonde_GenererBloc2( this->m_monde,
		data->m_estCaseRemplie );

	// Placer joueur
	for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir joueur
		if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
			data->m_identifiantJoueur[ i ] ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// D�truire le monde
			Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );

			// Ne plus prot�ger cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}

		// D�finir position
		Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( (BEtatClient*)joueur,
			data->m_positionJoueur[ i ] );
		Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( (BEtatClient*)joueur,
			NBAS );
	}

	// Ne plus prot�ger cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// Sortir du menu rejoindre vers le jeu
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_ValiderLancement( this->m_menuRejoindre );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseMessageAfficher( BClient *this,
	const struct BPacketServeurClientDiffuseMessageAfficher *data )
{
	// V�rifier qu'on soit bien en jeu
	if( this->m_mondeClient == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Transmettre au gestionnaire de message
	Projet_Client_Monde_BMondeClient_AfficherMessage( this->m_mondeClient,
		data->m_message,
		data->m_police,
		data->m_dureeAffichage,
		data->m_couleur,
		data->m_position,
		data->m_estDoitAfficherCadre,
		data->m_couleurCadre );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseDebutPartie( BClient *this,
	const struct BPacketServeurClientDiffuseDebutPartie *data )
{
	// R�f�rencer
	NREFERENCER( data );

	// V�rifier qu'on soit bien en jeu
	if( this->m_mondeClient == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// D�marrer la partie
	Projet_Client_Monde_BMondeClient_DemarrerPartie( this->m_mondeClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementDirection( BClient *this,
	const struct BPacketServeurClientDiffuseChangementDirection *data )
{
	// V�rifier qu'on soit bien en jeu
	if( this->m_mondeClient == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementDirection( this->m_cacheClient,
		data->m_identifiant,
		data->m_direction );
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseChangementPosition( BClient *this,
	const struct BPacketServeurClientDiffuseChangementPosition *data )
{
	// Joueur
	BEtatClient *joueur;

	// V�rifier qu'on soit bien en jeu
	if( this->m_mondeClient == NULL )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Prot�ger cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir le joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		data->m_identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Ne plus prot�ger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Activer d�placement
	Projet_Commun_Reseau_Client_BEtatDeplacementClient_Deplacer( (BEtatDeplacementClient*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( joueur ),
		*Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur ),
		data->m_direction );

	// Enregistrer la nouvelle position
	Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( joueur,
		data->m_nouvellePosition );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( joueur,
		data->m_direction );

	// Si il s'agissait du joueur courant
	if( data->m_identifiant == Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient ) )
		// On valide la fin du d�placement
		Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurDeplacement( this->m_mondeClient );

	// Ne plus prot�ger cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE
NBOOL Projet_Client_TraitementPacket_TraiterRefusePoseBombe( BClient *this,
	const struct BPacketServeurClientRefusePoseBombe *data )
{
	// R�f�rencer
	NREFERENCER( data );

	// Notifier le monde du refus
	Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurPoseBombe( this->m_mondeClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE
NBOOL Projet_Client_TraitementPacket_TraiterDiffusePoseBombe( BClient *this,
	const struct BPacketServeurClientDiffusePoseBombe *data )
{
	// Poser la bombe
	return Projet_Client_Monde_BMondeClient_PoserBombe( this->m_mondeClient,
		data->m_position,
		data->m_identifiantBombe,
		data->m_identifiantJoueur );
}

// BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE
NBOOL Projet_Client_TraitementPacket_TraiterBombeExplose( BClient *this,
	const struct BPacketServeurClientBombeExplose *data )
{
	// Jouer son explosion
	Projet_Client_BClient_JouerEffetSonore( this,
		( data->m_puissance >= BPUISSANCE_MAXIMALE_JOUEUR / 2 ) ? BSON_EXPLOSION_BOMBE_FORTE : BSON_EXPLOSION_BOMBE_NORMALE );

	// Faire exploser la bombe
	return Projet_Commun_Monde_BMonde_ExploserBombe( this->m_monde,
		data->m_identifiant,
		data->m_puissance,
		data->m_identifiantJoueur );
}

// BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT
NBOOL Projet_Client_TraitementPacket_TraiterBlocRempliDetruit( BClient *this,
	const struct BPacketServeurClientBlocRempliDetruit *data )
{
	// Supprimer le bloc
	return Projet_Commun_Monde_BMonde_SupprimerBlocRempli( this->m_monde,
		data->m_position );
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseApparitionBonus( BClient *this,
	const struct BPacketServeurClientDiffuseApparitionBonus *data )
{
	// Faire apparaitre bonus
	return Projet_Client_Monde_BMondeClient_PoserBonus( this->m_mondeClient,
		data->m_position,
		data->m_type,
		data->m_duree,
		data->m_identifiant );
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseDisparitionBonus( BClient *this,
	const struct BPacketServeurClientDiffuseDisparitionBonus *data )
{
	// Faire disparaitre bonus
	return Projet_Client_Monde_BMondeClient_SupprimerBonus( this->m_mondeClient,
		data->m_identifiant );
}

// BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS
NBOOL Projet_Client_TraitementPacket_TraiterDistribueBonus( BClient *this,
	const struct BPacketServeurClientDistribueBonus *data )
{
	// Joueur
	BEtatClient *joueur;

	// Lock cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir joueur courant
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		data->m_identifiant ) ) )
	{
		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Jouer son
	Projet_Client_Monde_BMondeClient_JouerSonBonus( this->m_mondeClient,
		Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient ) == data->m_identifiant,
		data->m_typeBonus );

	// Traiter la prise de bonus
	Projet_Commun_Carte_Bonus_GererPriseBonus( joueur,
		data->m_typeBonus );

	// Unlock cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );
	
	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR
NBOOL Projet_Client_TraitementPacket_TraiterDiffuseMortJoueur( BClient *this,
	const struct BPacketServeurClientDiffuseMortJoueur *data )
{
	// Tuer le client
	return Projet_Client_Monde_BMondeClient_TuerJoueur( this->m_mondeClient,
		data->m_identifiantJoueur );
}

// BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE
NBOOL Projet_Client_TraitementPacket_TraiterAnnonceFinPartie( BClient *this,
	const struct BPacketServeurClientAnnonceFinPartie *data )
{
	// Arr�ter la partie
	Projet_Client_Monde_BMondeClient_ArreterPartie( this->m_mondeClient,
		data->m_type,
		data->m_identifiant );

	// OK
	return NTRUE;
}

