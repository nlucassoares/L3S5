#include "../../../../../include/Projet/Projet.h"

// ---------------------------------------------------
// struct Projet::Client::Monde::Bombe::BEtatPoseBombe
// ---------------------------------------------------

/* Construire */
__ALLOC BEtatPoseBombe *Projet_Client_Monde_Bombe_BEtatPoseBombe_Construire( const struct BClient *client )
{
	// Sortie
	__OUTPUT BEtatPoseBombe *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEtatPoseBombe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = (struct BClient*)client;

	// Z�ro
	out->m_estAttenteReponseServeur = NFALSE;
	out->m_estPoseLock = NFALSE;

	// OK
	return out;
}

/* D�truire */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_Detruire( BEtatPoseBombe **this )
{
	// Lib�rer
	NFREE( *this );
}

/* Poser bombe */
NBOOL Projet_Client_Monde_Bombe_BEtatPoseBombe_PoserBombe( BEtatPoseBombe *this )
{
	// Joueur
	const BEtatClient *joueur;

	// Prot�ger cache client
	Projet_Client_BClient_ProtegerCacheClient( this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus prot�ger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

		// Quitter
		return NFALSE;
	}

	// V�rifier �tat
	if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Ne plus prot�ger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

		// Quitter
		return NFALSE;
	}

	// Ne plus prot�ger cache client
	Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

	// V�rifier �tat
	if( this->m_estAttenteReponseServeur
		|| this->m_estPoseLock )
		return NFALSE;

	// Poser
	if( !Projet_Client_TraitementPacket_EnvoyerPoseBombe( this->m_client ) )
		return NFALSE;

	// On lock la pose
	this->m_estPoseLock = NTRUE;

	// On attend d�sormais une r�ponse du serveur
	return ( this->m_estAttenteReponseServeur = NTRUE );
}

/* Finir de poser bombe */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_FinirPoseBombe( BEtatPoseBombe *this )
{
	this->m_estPoseLock = NFALSE;
}

/* Enregister r�ponse serveur */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_EnregistrerReponseServeur( BEtatPoseBombe *this )
{
	this->m_estAttenteReponseServeur = NFALSE;
}

