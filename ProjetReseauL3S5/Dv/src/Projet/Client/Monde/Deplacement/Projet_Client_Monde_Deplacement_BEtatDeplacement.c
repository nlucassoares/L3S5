#include "../../../../../include/Projet/Projet.h"

// -----------------------------------------------------------
// struct Projet::Client::Monde::Deplacement::BEtatDeplacement
// -----------------------------------------------------------

/* Construire */
__ALLOC BEtatDeplacement *Projet_Client_Monde_Deplacement_BEtatDeplacement_Construire( const struct BClient *client )
{
	// Sortie
	__OUTPUT BEtatDeplacement *out;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BEtatDeplacement ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = client;

	// OK
	return out;
}

/* D�truire */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( BEtatDeplacement **this )
{
	// Lib�rer
	NFREE( (*this) );
}

/* D�placer */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Deplacer( BEtatDeplacement *this,
	NDirection direction )
{
	// Joueur
	const BEtatClient *joueur;

	// Prot�ger cache client
	Projet_Client_BClient_ProtegerCacheClient( (BClient*)this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( (BClient*)this->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus prot�ger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// V�rifier �tat
	if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Ne plus prot�ger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Ne plus prot�ger cache client
	Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

	// V�rifier �tat
	if( direction == this->m_direction
		&& this->m_estEnCours )
		return;

	// Enregistrer
	this->m_tempsDebutPressionTouche = NLib_Temps_ObtenirTick( );
	this->m_direction = direction;
	this->m_estEnCours = NTRUE;
}

/* Stopper d�placement */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_StopperDeplacement( BEtatDeplacement *this,
	NDirection direction )
{
	// Arr�ter
	if( this->m_direction != direction
		|| !this->m_estEnCours )
		return;

	// Il s'agit d'un changement de direction?
	if( NLib_Temps_ObtenirTick( ) - this->m_tempsDebutPressionTouche <= BTEMPS_AVANT_CHANGEMENT_DIRECTION_DEPLACEMENT_CLIENT )
		// Envoyer packet changement de direction
		Projet_Client_TraitementPacket_EnvoyerChangementDirection( (BClient*)this->m_client,
			this->m_direction );

	// Arr�ter
	this->m_estEnCours = NFALSE;
}

/* R�ponse d�placement serveur */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_EnregistrerReponseDeplacementServeur( BEtatDeplacement *this )
{
	this->m_estAttenteReponseDeplacement = NFALSE;
}

/* Est en attente de la r�ponse pour le d�placement? */
NBOOL Projet_Client_Monde_Deplacement_BEtatDeplacement_EstAttenteReponseServeur( const BEtatDeplacement *this )
{
	return this->m_estAttenteReponseDeplacement;
}

/* Update */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Update( BEtatDeplacement *this )
{
	// Etat carte
	const BEtatCarte *etatCarte;

	// Est d�placement impossible?
	NBOOL estDeplacementImpossible = NFALSE;

	// Carte
	const BCarte *carte;

	// Cases �tat
	const BCaseEtatCarte **caseEtat;

	// Joueur courant
	const BEtatClient *joueur;

	// Future position
	NSPoint futurePosition;

	// V�rifier �tat
	if( !this->m_estEnCours
		|| this->m_estAttenteReponseDeplacement )
		return;

	// Obtenir �tat carte
	if( !( etatCarte = Projet_Client_BClient_ObtenirEtatCarte( this->m_client ) )
		|| !( carte = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCarte( etatCarte ) ) )
		return;

	// Prot�ger le cache
	Projet_Client_BClient_ProtegerCacheClient( (BClient*)this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
	{
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
		return;
	}

	// V�rifier qu'il ne soit pas en d�placement
	if( Projet_Commun_Reseau_Client_BEtatDeplacementClient_EstDeplacementActif( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( joueur ) ) )
	{
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
		return;
	}

	// Copier position
	futurePosition = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

	// Calculer future position
	switch( this->m_direction )
	{
		case NHAUT:
			futurePosition.y--;
			break;
		case NBAS:
			futurePosition.y++;
			break;
		case NGAUCHE:
			futurePosition.x--;
			break;
		case NDROITE:
			futurePosition.x++;
			break;

		default:
			break;
	}

	// Obtenir cases
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus prot�ger
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// V�rifier que le joueur puisse passer
		// D�passe les limites de la carte?
			if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( carte,
				futurePosition ) )
				estDeplacementImpossible = NTRUE;
		// Va vers une case avec colision/remplie
			else if( caseEtat[ futurePosition.x ][ futurePosition.y ].m_estRempli
				|| Projet_Commun_Carte_BCarte_ObtenirCases( carte )[ futurePosition.x ][ futurePosition.y ].m_type == BTYPE_BLOC_SOLIDE )
				estDeplacementImpossible = NTRUE;
			else if( caseEtat[ futurePosition.x ][ futurePosition.y ].m_etatBombe != NULL )
				estDeplacementImpossible = NTRUE;

	// Impossible d'aller � cet endroit
	if( estDeplacementImpossible )
	{
		// Changement de direction si diff�rente
		if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( joueur ) != this->m_direction )
			Projet_Client_TraitementPacket_EnvoyerChangementDirection( (BClient*)this->m_client,
				this->m_direction );

		// Arr�t du d�placement
		this->m_estEnCours = NFALSE;

		// Unlock cache
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Il s'agit d'un d�placement (et pas d'un changement de direction)
	if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( joueur ) == this->m_direction
		|| NLib_Temps_ObtenirTick( ) - this->m_tempsDebutPressionTouche > BTEMPS_AVANT_CHANGEMENT_DIRECTION_DEPLACEMENT_CLIENT )
	{
		// Envoyer packet changement de position
		Projet_Client_TraitementPacket_EnvoyerChangementPosition( (BClient*)this->m_client,
			this->m_direction );

		// On attend la r�ponse
		this->m_estAttenteReponseDeplacement = NTRUE;

		// D�marrer le mouvement
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Deplacer( (BEtatDeplacementClient*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( joueur ),
			*Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur ),
			this->m_direction );

		// Enregistrer temps
		this->m_tempsDebutPressionTouche = NLib_Temps_ObtenirTick( );
	}

	// Ne plus prot�ger le cache
	Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
}

