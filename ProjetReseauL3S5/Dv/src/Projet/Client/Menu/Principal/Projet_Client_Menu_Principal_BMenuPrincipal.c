#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------------------
// struct Projet::Client::Menu::Principal::BMenuPrincipal
// ------------------------------------------------------

/* Construire */
__ALLOC BMenuPrincipal *Projet_Client_Menu_Principal_BMenuPrincipal_Construire( const NFenetre *fenetre,
	const BFondEtoileMenu *etoile[ BNOMBRE_COUCHES_ETOILES_MENU ] )
{
	// Sortie
	__OUTPUT BMenuPrincipal *out;

	// It�rateurs
	NU32 i,
		j;

	// Position
	NSPoint position,
		positionPrecedente = { 0, };

	// Taille
	NUPoint taille;

	// Police
	NPolice *police;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BMenuPrincipal ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger la surface
	if( !( out->m_surfaceBomberman = NLib_Module_SDL_Surface_NSurface_Construire2( fenetre,
		Projet_Commun_Ressource_Client_BListeRessourceClient_ObtenirLien( BLISTE_RESSOURCE_CLIENT_MENU_PRINCIPAL_BOMBERMAN ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Centrer le logo
	NLib_Module_SDL_Surface_NSurface_DefinirPosition( out->m_surfaceBomberman,
		NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x / 2 - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_surfaceBomberman )->x / 2 ),
		NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->y / 4 - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_surfaceBomberman )->y / 2 ) );

	// Cr�er la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BLISTE_POLICE_BUBBLEGUM ),
		BTAILLE_POLICE_TITRE_OPTIONS_MENU_PRINCIPAL,
		BCOULEUR_TITRE_MENU_PRINCIPAL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBomberman );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Cr�er le titre
	if( !( out->m_surfaceTitre = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
		fenetre,
		BTITRE_MENU_PRINCIPAL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NLib_Module_SDL_TTF_NPolice_Detruire( &police );
		NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBomberman );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// D�finir la position
	NLib_Module_SDL_Surface_NSurface_DefinirPosition( out->m_surfaceTitre,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_surfaceTitre )->x / 2 ),
		NLib_Module_SDL_Surface_NSurface_ObtenirPosition( out->m_surfaceBomberman )->y + NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_surfaceBomberman )->y + BESPACE_IMAGE_LOGO_TITRE_MENU_PRINCIPAL );

	// D�truire la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Cr�er la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BLISTE_POLICE_LAZY_SUNDAY ),
		BTAILLE_POLICE_OPTIONS_MENU_PRINCIPAL,
		BCOULEUR_OPTIONS_MENU_PRINCIPAL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Lib�rer
		NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceTitre );
		NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBomberman );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Cr�er le texte
	for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
		if( !( out->m_texteBouton[ i ] = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
			fenetre,
			Projet_Client_Menu_Principal_BCodeMenuPrincipal_ObtenirTexte( i ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_texteBouton[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &police );
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceTitre );
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBomberman );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Fermer la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Cr�er les boutons
	for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
	{
		// D�finir la taille
		NDEFINIR_POSITION( taille,
			NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_texteBouton[ i ] )->x + BPADDING_BOUTON_MENU_PRINCIPAL[ NGAUCHE ] + BPADDING_BOUTON_MENU_PRINCIPAL[ NDROITE ],
			NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_texteBouton[ i ] )->y + BPADDING_BOUTON_MENU_PRINCIPAL[ NHAUT ] + BPADDING_BOUTON_MENU_PRINCIPAL[ NBAS ] );

		// D�finir position
		switch( i )
		{
			case 0:
				// Position pr�c�dente � z�ro
				NDEFINIR_POSITION( position,
					NLib_Module_SDL_Surface_NSurface_ObtenirPosition( out->m_surfaceTitre )->x + BPOSITION_PREMIER_BOUTON_MENU_PRINCIPAL.x,
					NLib_Module_SDL_Surface_NSurface_ObtenirPosition( out->m_surfaceTitre )->y + NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_surfaceTitre )->y + BPOSITION_PREMIER_BOUTON_MENU_PRINCIPAL.y );
				break;

			default:
				NDEFINIR_POSITION( position,
					positionPrecedente.x + BMARGING_BOUTON_MENU_PRINCIPAL[ NGAUCHE ],
					positionPrecedente.y + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( out->m_bouton[ i - 1 ] ).y + BMARGING_BOUTON_MENU_PRINCIPAL[ NHAUT ] );
				break;
		}

		// Cr�er
		if( !( out->m_bouton[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
			taille,
			BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR,
			BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND,
			fenetre,
			BEPAISSEUR_BOUTON_MENU_PRINCIPAL ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Lib�rer
			for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
				NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_texteBouton[ i ] );
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_bouton[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &police );
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceTitre );
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceBomberman );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// D�finir la position du texte
		NLib_Module_SDL_Surface_NSurface_DefinirPosition( out->m_texteBouton[ i ],
			position.x + (NS32)( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( out->m_bouton[ i ] ).x / 2 ) - (NS32)( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_texteBouton[ i ] )->x / 2 ),
			position.y + (NS32)( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( out->m_bouton[ i ] ).y / 2 ) - (NS32)( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( out->m_texteBouton[ i ] )->y / 2 ) );

		// Enregistrer
		NDEFINIR_POSITION( positionPrecedente,
			position.x + BMARGING_BOUTON_MENU_PRINCIPAL[ NGAUCHE ],
			position.y + BMARGING_BOUTON_MENU_PRINCIPAL[ NBAS ] );
	}

	// Enregistrer
	out->m_fenetre = fenetre;
	out->m_etoile = etoile;

	// OK
	return out;
}

/* D�truire */
void Projet_Client_Menu_Principal_BMenuPrincipal_Detruire( BMenuPrincipal **this )
{
	// It�rateur
	NU32 i = 0;

	// D�truire boutons/texte
	for( ; i < BCODES_MENU_PRINCIPAL; i++ )
	{
		NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_bouton[ i ] );
		NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_texteBouton[ i ] );
	}

	// D�truire surface bomberman
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceBomberman );

	// D�truire le titre
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceTitre );

	// Lib�rer
	NFREE( (*this) );
}

/* Afficher le menu (priv�e) */
void Projet_Client_Menu_Principal_BMenuPrincipal_AfficherInterne( BMenuPrincipal *this )
{
	// It�rateur
	NU32 i;

	// Nettoyer
	NLib_Module_SDL_NFenetre_Nettoyer( (NFenetre*)this->m_fenetre );

	// Afficher �toiles
	for( i = 0; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
		Projet_Client_Menu_BFondEtoileMenu_Afficher( this->m_etoile[ i ] );

	// Afficher le logo
	NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceBomberman );

	// Afficher le titre
	NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceTitre );

	// Afficher les cadres/texte
	for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
	{
		// Cadre
			// Changer couleur
				switch( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_bouton[ i ] ) )
				{
					case NETAT_BOUTON_SURVOLE:
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR_SURVOL );
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND_SURVOL );
						break;
					case NETAT_BOUTON_PRESSE:
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR_CLIC );
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND_CLIC );
						break;

					default:
					case NETAT_BOUTON_REPOS:
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR );
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( this->m_bouton[ i ],
							BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND );
						break;
				}
			// Dessiner
				NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_bouton[ i ] );

		// Texte
		NLib_Module_SDL_Surface_NSurface_Afficher( this->m_texteBouton[ i ] );
	}

	// Actualiser
	NLib_Module_SDL_NFenetre_Update( (NFenetre*)this->m_fenetre );
}

/* Mettre � jour (priv�e) */
void Projet_Client_Menu_Principal_BMenuPrincipal_MettreAJourInterne( BMenuPrincipal *this,
	BCodeMenuPrincipal *codeRetour )
{
	// It�rateur
	NU32 i;

	// Mettre � jour les �toiles
	for( i = 0; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
		Projet_Client_Menu_BFondEtoileMenu_Update( (BFondEtoileMenu*)this->m_etoile[ i ] );

	// Mettre � jour les boutons
	for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
		NLib_Module_SDL_Bouton_NBouton_Update( this->m_bouton[ i ],
			&this->m_positionSouris,
			NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

	// Analyser les boutons
	for( i = 0; i < BCODES_MENU_PRINCIPAL; i++ )
		switch( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_bouton[ i ] ) )
		{
			case NETAT_BOUTON_PRESSE:
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_bouton[ i ] ) == SDL_BUTTON_LEFT )
				{
					// R�initialiser le cadre
					NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_bouton[ i ] );

					// Enregistrer l'action � effectuer
					*codeRetour = i;

					// Quitter le menu
					this->m_estEnCours = NFALSE;
				}
				break;

			default:
				break;
		}
}

/* Lancer le menu */
BCodeMenuPrincipal Projet_Client_Menu_Principal_BMenuPrincipal_Lancer( BMenuPrincipal *this )
{
	// Ev�nement
	SDL_Event *e;

	// Code retour
	__OUTPUT BCodeMenuPrincipal codeRetour = BCODE_MENU_PRINCIPAL_QUITTER;

	// Activer le menu
	this->m_estEnCours = NTRUE;

	// R�cup�rer l'�venement
	e = NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre );

	// Boucle principale
	do
	{
		// Mettre � jour
		SDL_PollEvent( e );

		// Analyser �venement
		switch( e->type )
		{
			case SDL_QUIT:
				this->m_estEnCours = NFALSE;
				break;

			case SDL_MOUSEMOTION:
				NDEFINIR_POSITION( this->m_positionSouris,
					e->motion.x,
					e->motion.y );
				break;

			default:
				break;
		}

		// Mettre � jour
		Projet_Client_Menu_Principal_BMenuPrincipal_MettreAJourInterne( this,
			&codeRetour );

		// Afficher
		Projet_Client_Menu_Principal_BMenuPrincipal_AfficherInterne( this );
	} while( this->m_estEnCours );

	// OK
	return codeRetour;
}
