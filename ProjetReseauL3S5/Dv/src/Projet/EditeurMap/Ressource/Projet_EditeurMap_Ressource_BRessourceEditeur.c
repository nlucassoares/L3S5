#define PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_INTERNE
#include "../../../../include/Projet/Projet.h"

// -----------------------------------------------------
// enum Projet::EditeurMap::Ressource::BRessourceEditeur
// -----------------------------------------------------

/* Obtenir lien vers ressource */
const char *Projet_EditeurMap_Ressource_BRessourceEditeur_ObtenirLienVersRessource( BRessourceEditeur ressource )
{
	return BRessourceEditeurTexte[ ressource ];
}

