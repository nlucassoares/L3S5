#include "../../../../include/Projet/Projet.h"

// -----------------------------------------------------------------
// struct Projet::EditeurMap::Cadre::Tileset::Grille::BGrilleTileset
// -----------------------------------------------------------------

/* Construire */
__ALLOC BGrille *Projet_EditeurMap_Grille_BGrille_Construire( const BTileset **tileset,
	NU32 nombreTileset )
{
	// Sortie
	__OUTPUT BGrille *out;

	// It�rateur
	NU32 i = 0, j, k;
	NU32 x, y;

	// Ancien y
	NU32 ancienY;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BGrille ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// D�finir la taille horizontale
	out->m_taille.x = BDIMENSION_HORIZONTALE_GRILLE_EDITEUR;

	// Allouer la premi�re dimension
	if( !( out->m_cases = calloc( out->m_taille.x,
		sizeof( BCaseGrille* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Lib�rer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Calculer total de lignes
	for( ; i < nombreTileset; i++ )
		// Ajouter
		out->m_taille.y += ( Projet_Commun_Carte_Tileset_BTileset_EstAnime( tileset[ i ] ) ?
				( ( Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).y / out->m_taille.x ) + ( Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).y % BDIMENSION_HORIZONTALE_GRILLE_EDITEUR ) ? 1 : 0 )
					: Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).y );

	// Allouer la m�moire
		// 1D
			if( !( out->m_cases = calloc( out->m_taille.x,
				sizeof( BCaseGrille* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// 2D
			for( i = 0; i < out->m_taille.x; i++ )
				if( !( out->m_cases[ i ] = calloc( out->m_taille.y,
					sizeof( BCaseGrille ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					for( j = 0; j < i; j++ )
						NFREE( out->m_cases[ j ] );
					NFREE( out->m_cases );

					// Quitter
					return NULL;
				}

	// Z�ro
		// Cases
			for( x = 0; x < out->m_taille.x; x++ )
				for( y = 0; y < out->m_taille.y; y++ )
				{
					// Position
					NDEFINIR_POSITION( out->m_cases[ x ][ y ].m_position,
						BBLOC_CARTE_AUCUN,
						BBLOC_CARTE_AUCUN );

					// Tileset
					out->m_cases[ x ][ y ].m_idTileset = BBLOC_CARTE_AUCUN;

					// Colision
					out->m_cases[ x ][ y ].m_typeBloc = (BTypeBloc)0;
				}
		// Position
		ancienY = y = 0;

	// Ajouter cases
	for( i = 0; i < nombreTileset; i++ )
	{
		// Z�ro
		x = 0;

		// Ajouter
		if( Projet_Commun_Carte_Tileset_BTileset_EstAnime( tileset[ i ] ) )
		{
			// Enregistrer
			ancienY = y;

			// Remplir
			for( j = 0; j < Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).y; j++ )
			{
				// ID
				out->m_cases[ x ][ y ].m_idTileset = i;

				// Position dans le tileset
				NDEFINIR_POSITION( out->m_cases[ x ][ y ].m_position,
					0,
					j );

				// Avancer
				if( x < out->m_taille.x - 1 )
					x++;
				else
				{
					x = 0;
					y++;
				}
			}

			// Ligne suivante
			if( y == ancienY )
				y++;
		}
		else
		{
			// Enregistrer
			ancienY = y;

			// Remplir
			for( k = 0; k < Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).y; k++ )
			{
				for( j = 0; j < Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( tileset[ i ] ).x; j++ )
				{
					// ID
					out->m_cases[ x ][ y ].m_idTileset = i;

					// Position dans le tileset
					NDEFINIR_POSITION( out->m_cases[ x ][ y ].m_position,
						j,
						k );

					// Avancer
					if( x < out->m_taille.x - 1 )
						x++;
					else
					{
						x = 0;
						y++;
					}
				}
			}

			// Ligne suivante
			if( y == ancienY )
				y++;
		}
	}

	// Type de la grille tileset
	out->m_type = BTYPE_GRILLE_TILESET;

	// OK
	return out;
}

__ALLOC BGrille *Projet_EditeurMap_Grille_BGrille_Construire2( NU32 nombreTypesBloc )
{
	// Sortie
	__OUTPUT BGrille *out;

	// It�rateur
	NU32 i,
		j,
		typeBloc = 0;

	// Allouer la m�moire
	if( !( out = calloc( 1,
		sizeof( BGrille ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// D�finir la taille
	NDEFINIR_POSITION( out->m_taille,
		BDIMENSION_HORIZONTALE_GRILLE_EDITEUR,
		1 + ( nombreTypesBloc / BDIMENSION_HORIZONTALE_GRILLE_EDITEUR ) );

	// Allouer la m�moire
		// 1D
			if( !( out->m_cases = calloc( out->m_taille.x,
				sizeof( BCaseGrille* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Lib�rer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// 2D
			for( i = 0; i < out->m_taille.x; i++ )
				if( !( out->m_cases[ i ] = calloc( out->m_taille.y,
					sizeof( BCaseGrille ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Lib�rer
					for( j = 0; j < i; j++ )
						NFREE( out->m_cases[ j ] );
					NFREE( out->m_cases );
					NFREE( out );

					// Quitter
					return NULL;
				}

	// Remplir
	for( i = 0; i < out->m_taille.x; i++ )
		for( j = 0; j < out->m_taille.y; j++ )
			out->m_cases[ i ][ j ].m_typeBloc = typeBloc++;

	// Type de la grille colision
	out->m_type = BTYPE_GRILLE_TYPEBLOC;

	// OK
	return out;
}

/* D�truire */
void Projet_EditeurMap_Grille_BGrille_Detruire( BGrille **this )
{
	// It�rateurs
	NU32 i;

	// Lib�rer les cases
	for( i = 0; i < (*this)->m_taille.x; i++ )
		NFREE( (*this)->m_cases[ i ] );
	NFREE( (*this)->m_cases );

	// Lib�rer
	NFREE( *this );
}

/* Obtenir la taille */
NUPoint Projet_EditeurMap_Grille_BGrille_ObtenirTaille( const BGrille *this )
{
	return this->m_taille;
}

/* Obtenir les cases */
const BCaseGrille **Projet_EditeurMap_Grille_BGrille_ObtenirCase( const BGrille *this )
{
	return this->m_cases;
}

/* Chercher la position d'une case */
NUPoint Projet_EditeurMap_Grille_ChercherPositionCaseTileset( const BGrille *this,
	NU32 tileset,
	NUPoint positionTileset )
{
	// Sortie
	__OUTPUT NUPoint position;

	// V�rifier
	if( positionTileset.x == BBLOC_CARTE_AUCUN
		|| positionTileset.y == BBLOC_CARTE_AUCUN )
		return (NUPoint){ BBLOC_CARTE_AUCUN, BBLOC_CARTE_AUCUN };

	// Chercher
	for( position.x = 0; position.x < this->m_taille.x; position.x++ )
		for( position.y = 0; position.y < this->m_taille.y; position.y++ )
			if( this->m_cases[ position.x ][ position.y ].m_idTileset == tileset
				&& this->m_cases[ position.x ][ position.y ].m_position.x == positionTileset.x
				&& this->m_cases[ position.x ][ position.y ].m_position.y == positionTileset.y )
				return position;

	// Erreur
	return (NUPoint){ BBLOC_CARTE_AUCUN, BBLOC_CARTE_AUCUN };
}

NUPoint Projet_EditeurMap_Grille_ChercherPositionCaseTypeBloc( const BGrille *this,
	BTypeBloc type )
{
	// Sortie
	__OUTPUT NUPoint position;

	// Chercher
	for( position.x = 0; position.x < this->m_taille.x; position.x++ )
		for( position.y = 0; position.y < this->m_taille.y; position.y++ )
			if( this->m_cases[ position.x ][ position.y ].m_typeBloc == type )
				return position;

	// Erreur
	return (NUPoint){ BBLOC_CARTE_AUCUN, BBLOC_CARTE_AUCUN };
}