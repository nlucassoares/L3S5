#ifndef PROJET_CLIENT_MENU_REJOINDRE_BMENUREJOINDRE_PROTECT
#define PROJET_CLIENT_MENU_REJOINDRE_BMENUREJOINDRE_PROTECT

// ------------------------------------------------------
// struct Projet::Client::Menu::Rejoindre::BMenuRejoindre
// ------------------------------------------------------

typedef struct BMenuRejoindre
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Code retour
	__OUTPUT BCodeMenuRejoindre m_codeRetour;

	// Etape menu
	BEtapeMenuRejoindre m_etape;

	// Position souris
	NSPoint m_positionSouris;

	// Client
	struct BClient *m_client;

	// Fen�tre
	NFenetre *m_fenetre;

	// Ressources
	const BRessource *m_ressource;

	// Etoiles
	const BFondEtoileMenu **m_etoile;

	/* BETAPE_MENU_REJOINDRE_CONNEXION */
	// Temps d�but tentative
	NU32 m_tempsDebutTentativeConnexion;

	// Bouton pour saisie adresse
	NBouton *m_boutonAdresse;

	// Adresse
	char *m_adresseConnexion;
	NSurface *m_surfaceAdresseConnexion;

	// Instance saisie
	NSaisieSDL *m_saisieAdresseConnexion;

	// Bouton de validation connexion
	NBouton *m_boutonValidationConnexion;
	NSurface *m_surfaceValidationConnexion;
	NSurface *m_surfaceTitreConnexion;

	// Police pour adresse
	NPolice *m_policeAdresse;

	/* BETAPE_MENU_REJOINDRE_CHOIX_DETAILS */
	// Est doit update nom
	NBOOL m_estDoitUpdateNomJoueur;

	// Identifiant derni�re modification
	NU32 m_identifiantDerniereModification;

	// Police saisie nom
	NPolice *m_policeNomJoueur;

	// Cadres joueurs
	NCadre *m_cadreJoueur[ BNOMBRE_MAXIMUM_JOUEUR ];
	NBouton *m_boutonPersonnage[ BNOMBRE_MAXIMUM_JOUEUR ];
	NBouton *m_boutonNomPersonnage[ BNOMBRE_MAXIMUM_JOUEUR ];
	NSurface *m_surfaceNomPersonnage[ BNOMBRE_MAXIMUM_JOUEUR ];

	NBouton *m_boutonPret[ BNOMBRE_MAXIMUM_JOUEUR ];
	NSurface *m_surfaceTextePret[ BETATS_PRET ];

	// Bouton lancer
	NBouton *m_boutonLancer;
	NSurface *m_surfaceTexteLancer;

	// Saisie nom personnage
	NSaisieSDL *m_saisieNomPersonnage;
	NBOOL m_estSaisieNomPersonnage;

	// Cadre carte
	NBouton *m_cadreCarte;
} BMenuRejoindre;

/* Construire */
__ALLOC BMenuRejoindre *Projet_Client_Menu_Rejoindre_BMenuRejoindre_Construire( const NFenetre*,
	const struct BClient *client,
	const BFondEtoileMenu *etoiles[ BNOMBRE_COUCHES_ETOILES_MENU ] );

/* D�truire */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_Detruire( BMenuRejoindre** );

/* Lancer */
BCodeMenuRejoindre Projet_Client_Menu_Rejoindre_BMenuRejoindre_Lancer( BMenuRejoindre* );
BCodeMenuRejoindre Projet_Client_Menu_Rejoindre_BMenuRejoindre_LancerHote( BMenuRejoindre* );

/* Update cadre carte */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreCarte( BMenuRejoindre* );

/* Remise � z�ro */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_RemettreAZero( BMenuRejoindre* );

/* Valider le lancement de la partie */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ValiderLancement( BMenuRejoindre* );

#endif // !PROJET_CLIENT_MENU_REJOINDRE_BMENUREJOINDRE_PROTECT

