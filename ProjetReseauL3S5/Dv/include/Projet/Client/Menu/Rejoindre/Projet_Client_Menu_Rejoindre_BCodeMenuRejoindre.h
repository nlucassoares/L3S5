#ifndef PROJET_CLIENT_MENU_REJOINDRE_BCODEMENUREJOINDRE_PROTECT
#define PROJET_CLIENT_MENU_REJOINDRE_BCODEMENUREJOINDRE_PROTECT

// --------------------------------------------------------
// enum Projet::Client::Menu::Rejoindre::BCodeMenuRejoindre
// --------------------------------------------------------

typedef enum BCodeMenuRejoindre
{
	BCODE_MENU_REJOINDRE_QUITTER,

	BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL,

	BCODE_MENU_REJOINDRE_LANCER,

	BCODES_MENU_REJOINDRE
} BCodeMenuRejoindre;

#endif // !PROJET_CLIENT_MENU_REJOINDRE_BCODEMENUREJOINDRE_PROTECT

