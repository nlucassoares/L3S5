#ifndef PROJET_CLIENT_MENU_CREER_BCODEMENUCREER_PROTECT
#define PROJET_CLIENT_MENU_CREER_BCODEMENUCREER_PROTECT

// ------------------------------------------------
// enum Projet::Client::Menu::Creer::BCodeMenuCreer
// ------------------------------------------------

typedef enum BCodeMenuCreer
{
	BCODE_MENU_CREER_QUITTER,

	BCODE_MENU_CREER_RETOUR_MENU_PRINCIPAL,

	BCODE_MENU_CREER_LANCER,

	BCODES_MENU_CREER
} BCodeMenuCreer;

#endif // !PROJET_CLIENT_MENU_CREER_BCODEMENUCREER_PROTECT

