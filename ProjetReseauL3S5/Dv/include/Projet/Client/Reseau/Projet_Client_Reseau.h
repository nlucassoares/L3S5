#ifndef PROJET_CLIENT_RESEAU_PROTECT
#define PROJET_CLIENT_RESEAU_PROTECT

// --------------------------------
// namespace Projet::Client::Reseau
// --------------------------------

/* Callback réception packet */
__CALLBACK NBOOL Projet_Client_Reseau_CallbackReceptionPacket( const NPacket*,
	void* );

/* Callback déconnexion */
NBOOL Projet_Client_Reseau_CallbackDeconnexion( void* );

#endif // !PROJET_CLIENT_RESEAU_PROTECT

