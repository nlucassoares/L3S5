#ifndef PROJET_CLIENT_MONDE_DEPLACEMENT_PROTECT
#define PROJET_CLIENT_MONDE_DEPLACEMENT_PROTECT

// --------------------------------------------
// namespace Projet::Client::Monde::Deplacement
// --------------------------------------------

// Temps avant lequel il ne s'agira qu'un d'un changement de direction
static const NU32 BTEMPS_AVANT_CHANGEMENT_DIRECTION_DEPLACEMENT_CLIENT = 85;

// struct Projet::Client::Monde::Deplacement::BEtatDeplacement
#include "Projet_Client_Monde_Deplacement_BEtatDeplacement.h"

#endif // !PROJET_CLIENT_MONDE_DEPLACEMENT_PROTECT

