#ifndef PROJET_CLIENT_MONDE_MESSAGE_BMESSAGEMONDECLIENT_PROTECT
#define PROJET_CLIENT_MONDE_MESSAGE_BMESSAGEMONDECLIENT_PROTECT

// ----------------------------------------------------------
// struct Projet::Client::Monde::Message::BMessageMondeClient
// ----------------------------------------------------------

typedef struct BMessageMondeClient
{
	/* Ressources */
	// Polices
	NPolice *m_police[ BPOLICES_MESSAGE_CLIENT ];

	// Surface
	NSurface *m_surfaceMessage;
	
	// Fen�tre
	const NFenetre *m_fenetre;

	// Mutex
	NMutex *m_mutex;

	// Cadre
	NCadre *m_cadre;

	/* Configuration */
	// Est doit afficher cadre
	NBOOL m_estDoitAfficherCadre;

	// Couleur cadre
	NCouleur m_couleurCadre;

	// Position
	BPositionAffichageMessageClient m_position;

	// Dur�e d'affichage
	NU32 m_dureeAffichageActuelle;

	// Message
	char *m_messageActuel;

	// Couleur
	NCouleur m_couleurActuelle;

	// Police
	BPoliceMessageClient m_policeActuelle;

	// Message fin de partie
	char *m_messageFinPartie;

	// Surface message fin de partie
	NSurface *m_surfaceMessageFinPartie;

	/* ETAT */
	// D�but affichage
	NU32 m_tempsDebutAffichage;

	// Est doit afficher?
	NBOOL m_estDoitAfficher;

	// Est message cr��?
	NBOOL m_estMessageCree;

	// Est message fin partie cr��?
	NBOOL m_estMessageFinPartieCree;

	// Alpha
	NU32 m_alpha;

	// Est doit afficher message de fin de partie?
	NBOOL m_estDoitAfficherFinPartie;
} BMessageMondeClient;

/* Construire */
__ALLOC BMessageMondeClient *Projet_Client_Monde_Message_BMessageMondeClient_Construire( const NFenetre* );

/* D�truire */
void Projet_Client_Monde_Message_BMessageMondeClient_Detruire( BMessageMondeClient** );

/* Update */
void Projet_Client_Monde_Message_BMessageMondeClient_Update( BMessageMondeClient* );

/* Donner un message � afficher */
NBOOL Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessage( BMessageMondeClient*,
	const char *message,
	NU32 dureeAffichage,
	BPoliceMessageClient,
	NCouleur,
	BPositionAffichageMessageClient,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre );

/* Donner un message de fin de partie */
NBOOL Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessageFinPartie( BMessageMondeClient*,
	const char* );

/* Afficher */
void Projet_Client_Monde_Message_BMessageMondeClient_Afficher( const BMessageMondeClient* );

#endif // !PROJET_CLIENT_MONDE_MESSAGE_BMESSAGEMONDECLIENT_PROTECT

