#ifndef PROJET_CLIENT_MONDE_MESSAGE_PROTECT
#define PROJET_CLIENT_MONDE_MESSAGE_PROTECT

// ----------------------------------------
// namespace Projet::Client::Monde::Message
// ----------------------------------------

// Vitesse d'incr�mentation de l'alpha des messages
static const NU32 BVITESSE_INCREMENTATION_ALPHA_MESSAGE_CLIENT		= 15;

// Couleur contour cadre
static const NCouleur BCOULEUR_CONTOUR_CADRE_MESSAGE_CLIENT			= { 0x00, 0x00, 0x00, 0xFF };

// Epaisseur contour cadre
static const NU32 BEPAISSEUR_CONTOUR_CADRE_MESSAGE_CLIENT			= 2;

// Padding
static const NS32 BPADDING_CADRE_MESSAGE_CLIENT[ NDIRECTIONS ]		= { 10, 10, 10, 10 };

// Marge verticale si position haut
static const NU32 BMARGE_VERTICALE_AFFICHAGE_TEXTE_MESSAGE_CLIENT	= 10;

// Couleur message fin partie
static const NCouleur BCOULEUR_MESSAGE_FIN_PARTIE_CLIENT =
{
	0x11,
	0x11,
	0x11,
	0xFF
};

// Message de victoire/�galit�
static const char BMESSAGE_VICTOIRE_FIN_PARTIE_CLIENT[ 64 ]					= "Le joueur %s a gagn�!";
static const char BMESSAGE_EGALITE_FIN_PARTIE_CLIENT[ 64 ]					= "Il s'agit d'une �galit�.";
static const char BMESSAGE_INCONNU_FIN_PARTIE_CLIENT[ 64 ]					= "Fin de la partie.";

// struct Projet::Client::Monde::Message::BMessageMondeClient
#include "Projet_Client_Monde_Message_BMessageMondeClient.h"

#endif // !PROJET_CLIENT_MONDE_MESSAGE_PROTECT

