#ifndef PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT
#define PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT

// ----------------------------------------------------
// struct Projet::Client::Configuration::BConfiguration
// ----------------------------------------------------

typedef struct BConfiguration
{
	// R�solution
	NUPoint m_resolution;

	// Port
	NU32 m_port;

	// Configuration des touches
	BConfigurationTouche *m_touche;

	// Param�tres par d�faut
	char *m_nomDefaut;
	NU32 m_charsetDefaut;
	NU32 m_couleurCharsetDefaut;
} BConfiguration;

/* Construire */
__ALLOC BConfiguration *Projet_Client_Configuration_BConfiguration_Construire( const char* );

/* D�truire */
void Projet_Client_Configuration_BConfiguration_Detruire( BConfiguration** );

/* Obtenir la r�solution */
const NUPoint *Projet_Client_Configuration_BConfiguration_ObtenirResolution( const BConfiguration* );

/* Obtenir le port */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirPort( const BConfiguration* );

/* Obtenir le nom par d�faut */
const char *Projet_Client_Configuration_BConfiguration_ObtenirNomDefaut( const BConfiguration* );

/* Obtenir le charset d�faut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCharsetDefaut( const BConfiguration* );

/* Obtenir couleur charset d�faut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCouleurCharsetDefaut( const BConfiguration* );

/* Obtenir configuration touches */
BConfigurationTouche *Projet_Client_Configuration_BConfiguration_ObtenirConfigurationTouche( const BConfiguration* );

#endif // !PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT

