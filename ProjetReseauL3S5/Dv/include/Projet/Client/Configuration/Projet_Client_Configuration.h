#ifndef PROJET_CLIENT_CONFIGURATION_PROTECT
#define PROJET_CLIENT_CONFIGURATION_PROTECT

// ---------------------------------------
// namespace Projet::Client::Configuration
// ---------------------------------------

// Lien fichier de configuration
static const char BLIEN_FICHIER_CONFIGURATION_CLIENT[ 32 ] = "Client.ini";

// namespace Projet::Client::Configuration::Touche
#include "Touche/Projet_Client_Configuration_Touche.h"

// enum Projet::Client::Configuration::BListeProprieteConfiguration
#include "Projet_Client_Configuration_BListeProprieteConfiguration.h"

// struct Projet::Client::Configuration::BConfiguration
#include "Projet_Client_Configuration_BConfiguration.h"


#endif // !PROJET_CLIENT_CONFIGURATION_PROTECT

