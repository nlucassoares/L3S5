#ifndef PROJET_CLIENT_CONFIGURATION_TOUCHE_PROTECT
#define PROJET_CLIENT_CONFIGURATION_TOUCHE_PROTECT

// -----------------------------------------------
// namespace Projet::Client::Configuration::Touche
// -----------------------------------------------

// enum Projet::Client::Configuration::Touche::BTouche
#include "Projet_Client_Configuration_Touche_BTouche.h"

// struct Projet::Client::Configuration::Touche::BConfigurationTouche
#include "Projet_Client_Configuration_Touche_BConfigurationTouche.h"

#endif // !PROJET_CLIENT_CONFIGURATION_TOUCHE_PROTECT

