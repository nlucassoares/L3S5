#ifndef PROJET_CLIENT_CONFIGURATION_TOUCHE_BCONFIGURATIONTOUCHE_PROTECT
#define PROJET_CLIENT_CONFIGURATION_TOUCHE_BCONFIGURATIONTOUCHE_PROTECT

// ------------------------------------------------------------------
// struct Projet::Client::Configuration::Touche::BConfigurationTouche
// ------------------------------------------------------------------

typedef struct BConfigurationTouche
{
	// Touches
	SDL_Keycode m_touche[ BTOUCHES ];
} BConfigurationTouche;

/* Construire */
__ALLOC BConfigurationTouche *Projet_Client_Configuration_Touche_BConfigurationTouche_Construire( void );
__ALLOC BConfigurationTouche *Projet_Client_Configuration_Touche_BConfigurationTouche_Construire2( NFichierClef* );

/* D�truire */
void Projet_Client_Configuration_Touche_BConfigurationTouche_Detruire( BConfigurationTouche** );

/* Obtenir touche */
SDL_Keycode Projet_Client_Configuration_Touche_BConfigurationTouche_Obtenir( const BConfigurationTouche*,
	BTouche );
BTouche Projet_Client_Configuration_Touche_BConfigurationTouche_ObtenirInverse( const BConfigurationTouche*,
	SDL_Keycode );

#endif // !PROJET_CLIENT_CONFIGURATION_TOUCHE_BCONFIGURATIONTOUCHE_PROTECT

