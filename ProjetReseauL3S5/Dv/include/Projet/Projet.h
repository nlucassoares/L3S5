#ifndef PROJET_PROTECT
#define PROJET_PROTECT

/*
	Header principal � inclure dans tous les fichiers source
	du projet.
	Contient l'inclusion de NLib.

	@author SOARES Lucas
*/

// namespace NLib
#include "../../../../NLib/include/NLib/NLib.h"

// ----------------
// namespace Projet
// ----------------

// OPTIONS
// Temps avant explosion bombe al�atoire?
#define BOPTION_TEMPS_AVANT_EXPLOSION_BOMBE_ALEATOIRE	0

// namespace Projet::Commun
#include "Commun/Projet_Commun.h"

// namespace Projet::Serveur
#include "Serveur/Projet_Serveur.h"

// namespace Projet::Client
#include "Client/Projet_Client.h"

// namespace Projet::EditeurMap
#include "EditeurMap/Projet_EditeurMap.h"

// namespace Projet::Test
#include "Test/Projet_Test.h"


#endif // !PROJECT_PROTECT

