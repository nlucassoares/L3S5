#ifndef PROJET_COMMUN_PERSONNAGE_BPERSONNAGE_PROTECT
#define PROJET_COMMUN_PERSONNAGE_BPERSONNAGE_PROTECT

/*
	D�finition d'un personnage du jeu

	@author SOARES Lucas
*/

// ----------------------------------------------
// struct Projet::Commun::Personnage::BPersonnage
// ----------------------------------------------

// struct Projet::Commun::Personnage::BPersonnage
typedef struct
{
	// Nom du personnage
	char *m_nom;

	// Charset(s)
	BCharset *m_charset;

	// Vitesse
	NU32 m_vitesse;

	// Taux drop
	NU32 m_tauxDrop;
} BPersonnage;

/* Construire */
__ALLOC BPersonnage *Projet_Commun_Personnage_BPersonnage_Construire( const char *nomFichier,
	const NFenetre* );

/* D�truire */
void Projet_Commun_Personnage_BPersonnage_Detruire( BPersonnage** );

/* Obtenir le nom */
const char *Projet_Commun_Personnage_BPersonnage_ObtenirNom( const BPersonnage* );

/* Obtenir la vitesse */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirVitesse( const BPersonnage* );

/* Obtenir le taux de drop */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirTauxDrop( const BPersonnage* );

/* Obtenir la taille de l'animation d�placement */
NUPoint Projet_Commun_Personnage_BPersonnage_ObtenirTailleFrameDeplacement( const BPersonnage* );

/* Obtenir la taille de l'animation mort */
NUPoint Projet_Commun_Personnage_BPersonnage_ObtenirTailleFrameMort( const BPersonnage* );

/* Obtenir le nombre de couleurs */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirNombreCouleur( const BPersonnage* );

/* Obtenir le charset */
const BCharset *Projet_Commun_Personnage_BPersonnage_ObtenirCharset( const BPersonnage* );

/* D�finir position affichage */
void Projet_Commun_Personnage_BPersonnage_DefinirPosition( BPersonnage*,
	NSPoint position );

/* Afficher le charset */
NBOOL Projet_Commun_Personnage_BPersonnage_AfficherCharset( const BPersonnage*,
	NDirection direction,
	NU32 couleur,
	NU32 zoom );
NBOOL Projet_Commun_Personnage_BPersonnage_AfficherCharsetMort( const BPersonnage*,
	NU32 zoom,
	const NEtatAnimation* );

/* Update */
void Projet_Commun_Personnage_BPersonnage_Update( BPersonnage* );

#endif // !PROJET_COMMUN_PERSONNAGE_BPERSONNAGE_PROTECT

