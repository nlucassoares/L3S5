#ifndef PROJET_COMMUN_PERSONNAGE_CHARGEMENT_PROTECT
#define PROJET_COMMUN_PERSONNAGE_CHARGEMENT_PROTECT

/*
	Chargement des personnages

	@author SOARES Lucas
*/

// ------------------------------------------------
// namespace Projet::Commun::Personnage::Chargement
// ------------------------------------------------

// Lien vers personnages
static const char BREPERTOIRE_PERSONNAGE[ 32 ] = "Assets/Personnage";

/* Chargement des personnages */
BPersonnage **Projet_Commun_Personnage_Chargement_Charger( __OUTPUT NU32 *nombrePersonnages,
	const NFenetre* );

#endif // !PROJET_COMMUN_PERSONNAGE_CHARGEMENT_PROTECT

