#ifndef PROJET_COMMUN_RESEAU_PROTECT
#define PROJET_COMMUN_RESEAU_PROTECT

/*
	D�clarations du projet relatives au r�seau
	(packet/client/serveur)

	@author SOARES Lucas
*/

// --------------------------------
// namespace Projet::Commun::Reseau
// --------------------------------

// namespace Projet::Commun::Reseau::Client
#include "Client/Projet_Commun_Reseau_Client.h"

// namespace Projet::Commun::Reseau::Serveur
#include "Serveur/Projet_Commun_Reseau_Serveur.h"

// namespace Projet::Commun::Reseau::Packet
#include "Packet/Projet_Commun_Reseau_Packet.h"

// V�rifier
nassert( BNOMBRE_MAXIMUM_JOUEUR >= 1,
	"Projet::Commun::Reseau::BNOMBRE_MAXIMUM_JOUEUR" );

#endif // !PROJET_COMMUN_RESEAU_PROTECT

