#ifndef PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT

// --------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatClient
// --------------------------------------------------

typedef struct BEtatClient
{
	/* Client */
	// Est client courant? [Le client qui joue ce personnage?]
	NBOOL m_estClientCourant;

	/* Serveur */
	// Donn�es suppl�mentaires [Si serveur: NClientServeur* associ�, Si client: NULL]
	void *m_donneeSupplementaire;

	/* Commun */
		/* Etat */
			// Est client v�rifi�? [Checksum ok?]
			NBOOL m_estChecksumVerifie;

			// Tick connexion
			NU32 m_tickConnexion;

			// Est pr�t?
			BEtatPret m_estPret;
	
			// A confirm� le lancement?
			NBOOL m_estConfirmeLancement;

			// Est en train de jouer l'animation de mort?
			NBOOL m_estAnimationMortEnCours;

			// Etat animation mort
			NEtatAnimation *m_etatAnimationMort;
		/* Propri�t�s */
			// Nom
			char *m_nom;

			// Identifiant
			NU32 m_identifiant;

			// Charset
			NU32 m_charset;

			// Couleur
			NU32 m_couleurCharset;
		/* Etat dans monde */
			// Est en vie
			NBOOL m_estEnVie;

			// Position
			NSPoint m_position;

			// Direction
			NDirection m_direction;

			// D�placement
			BEtatDeplacementClient *m_etatDeplacement;

			// Puissance
			NU32 m_puissance;

			// Nombre de bombes pos�es au maximum
			NU32 m_nombreBombeMaximum;

			// Nombre de bombes pos�es actuellement
			NU32 m_nombreBombePosee;
} BEtatClient;

/* Construire */
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire( NU32 identifiant,
	NBOOL estClientCourant,
	void *donneeSupplementaire,
	const void *ressource );
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire2( const BEtatClient* );

/* D�truire */
void Projet_Commun_Reseau_Client_BEtatClient_Detruire( BEtatClient** );

/* Obtenir nom */
const char *Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( const BEtatClient* );

/* Obtenir identifiant */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( const BEtatClient* );

/* Obtenir charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( const BEtatClient* );

/* Obtenir couleur charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( const BEtatClient* );

/* Obtenir position */
const NSPoint *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( const BEtatClient* );

/* Obtenir �tat d�placement */
const BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( const BEtatClient* );

/* Obtenir �tat animation */
const NEtatAnimation *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatAnimationMort( const BEtatClient* );

/* Obtenir nombre bombes pos�es */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreBombePosee( const BEtatClient* );

/* Obtenir nombre maximum bombes */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreMaximumBombe( const BEtatClient* );

/* Est confirm� le lancement? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstConfirmeLancement( const BEtatClient* );

/* Est pr�t? */
BEtatPret Projet_Commun_Reseau_Client_BEtatClient_EstPret( const BEtatClient* );

/* Est client v�rifi�? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstVerifie( const BEtatClient* );

/* On doit afficher ce client? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstDoitAfficher( const BEtatClient* );

/* V�rifier �tat client */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( const BEtatClient* );

/* Obtenir tick connexion */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirTickConnexion( const BEtatClient* );

/* Confirmer le lancement */
void Projet_Commun_Reseau_Client_BEtatClient_ConfirmerLancement( BEtatClient* );

/* D�finir nom */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( BEtatClient*,
	const char* );

/* D�finir charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( BEtatClient*,
	NU32 );

/* D�finir couleur charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( BEtatClient*,
	NU32 );

/* D�finir �tat v�rification */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( BEtatClient*,
	NBOOL );

/* D�finir est pr�t */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( BEtatClient*,
	BEtatPret );

/* D�finir position */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( BEtatClient*,
	NSPoint );

/* D�finir direction */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( BEtatClient*,
	NDirection );

/* D�finir puissance */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( BEtatClient*,
	NU32 );

/* D�finir est en vie */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( BEtatClient*,
	NBOOL );

/* Est en vie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( const BEtatClient* );

/* Obtenir direction */
NDirection Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( const BEtatClient* );

/* Obtenir donn�e suppl�mentaire */
void *Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( const BEtatClient* );

/* D�finir donn�e suppl�mentaire */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDonneeSupplementaire( BEtatClient*,
	void *data );

/* Obtenir puissance */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( const BEtatClient* );

/* Incr�menter nombre bombes maximum */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombeMaximum( BEtatClient* );

/* Incr�menter/d�cr�menter nombre bombes pos�es */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( BEtatClient* );
void Projet_Commun_Reseau_Client_BEtatClient_DecrementerNombreBombePosee( BEtatClient* );

/* Lancer l'animation de mort */
void Projet_Commun_Reseau_Client_BEtatClient_LancerAnimationMort( BEtatClient* );

/* Update */
void Projet_Commun_Reseau_Client_BEtatClient_Update( BEtatClient* );

#endif // !PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT


