#ifndef PROJET_COMMUN_RESEAU_PACKET_PROTECT
#define PROJET_COMMUN_RESEAU_PACKET_PROTECT

/*
	D�claration relatives aux packets r�seau du projet

	@author SOARES Lucas
*/

// ----------------------------------------
// namespace Projet::Commun::Reseau::Packet
// ----------------------------------------

// enum Projet::Commun::Reseau::Packet::BTypePacket
#include "Projet_Commun_Reseau_Packet_BTypePacket.h"

// namespace Projet::Commun::Reseau::Packet::Donnee
#include "Donnee/Projet_Commun_Reseau_Packet_Donnee.h"

// namespace Projet::Commun::Reseau::Packet::Forge
#include "Forge/Projet_Commun_Reseau_Packet_Forge.h"

// namespace Projet::Commun::Reseau::Packet::Lecteur
#include "Lecteur/Projet_Commun_Reseau_Packet_Lecteur.h"

/* Cr�er packet */
__ALLOC NPacket *Projet_Commun_Reseau_Packet_Creer( BTypePacket,
	__WILLBEOWNED void* );

/* Lire packet */
__ALLOC void *Projet_Commun_Reseau_Lire( const NPacket*,
	__OUTPUT BTypePacket *typePacket );

#endif // !PROJET_COMMUN_RESEAU_PACKET_PROTECT

