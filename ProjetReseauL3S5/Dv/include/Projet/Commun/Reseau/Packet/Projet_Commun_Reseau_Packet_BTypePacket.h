#ifndef PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET
#define PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET

/*
	Les types de packet existants, et leurs d�tails sont
	d�taill�s ici.

	@author SOARES Lucas
*/

// ------------------------------------------------
// enum Projet::Commun::Reseau::Packet::BTypePacket
// ------------------------------------------------

// enum Projet::Commun::Reseau::Packet::BTypePacket
typedef enum BTypePacket
{
	// Packet nul
	BTYPE_PACKET_AUCUN,

	// -------------------
	// CLIENT vers SERVEUR
	// -------------------
	/*
		[Deuxi�me �tape de communication, le client transmet sa requ�te de connexion au serveur]

		Le client demande l'accession � la salle d'attente du serveur.

		Donn�es transmises:
			// Le charset du personnage
			NU32 identifiantCharsetPersonnage;
			
			// La couleur du personnage parmis les couleurs disponibles
			// dans le charset
			NU32 identifiantCouleurPersonnage;

			// Taille nom
			NU32 tailleNomJoueur;

			// Le nom choisi par le joueur
			char *nomJoueur;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR,	

	/*
		[Le client donne son checksum de ressources]

		Le client donne le checksum de ses ressources pour �tre compar�
		� celui du serveur.

		Donn�es transmises:
			// Checksum
			NU32 checksum;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM,
	
	/*
		[Le client demande le ping]

		Le client demande un ping au serveur

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_PING,

	/*
		[Le client r�pond � la demande de ping]

		Le client r�pond � la demande de ping �mise par le serveur

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_PONG,

	/*
		[Le client informe un changement d'�tat pr�t]

		Le client inverse son �tat pr�t/pas pr�t dans le lobby

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET,

	/*
		[Le client annonce au serveur que le lancement a
		�t� correctement effectu�]

		Le client est pr�t � ce que la partie d�marre.

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT,

	/*
		[Le joueur change de direction]

		Le joueur change la direction de son personnage.

		Donn�es transmises:
			// Direction
			NDirection direction;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION,

	/*
		[Le joueur se d�place]

		Le joueur d�place son personnage

		Donn�es transmises:
			// Direction
			NDirection direction;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION,

	/*
		[Le joueur pose une bombe]

		Le joueur appuie sur espace pour poser une bombe

		Donn�es transmises
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE,

	// -------------------
	// SERVEUR vers CLIENT
	// -------------------
	/*
		[Premi�re �tape de la communication]

		Le serveur envoie un identifiant unique au client venant de 
		se connecter.

		Donn�es transmises:
			// L'identifiant attribu� au client par le serveur
			NU32 identifiantJoueur;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT,

	/*
		[Serveur donne ce qu'il autorise comme charset/couleur/nom
		au client]

		Le serveur valide en envoyant la m�me info/modifie ce qu'il
		faut dans les informations que le client lui transmet.

		Donn�es transmises:
			// Taille nom
			NU32 tailleNom;

			// Nom
			char *nom;

			// Charset
			NU32 charset;

			// Couleur charset
			NU32 couleurCharset
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR,

	/*
		[Serveur informe qu'un client se d�connecte]

		Le serveur a d�connect� un joueur et informe les
		autres joueurs

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR,

	/*
		[Serveur informe un client que son checksum est incorrect]

		Le serveur informe un client que son CRC ressource est incorrect

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT,

	/*
		[Serveur fait une demande de ping]

		Le serveur fait une demande de ping au client

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_PING,

	/*
		[Serveur r�pond au ping]

		Le serveur r�pond � la demande de ping du client

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_PONG,

	/*
		[Le serveur diffuse un changement d'�tat pr�t/pas pr�t]

		Changement d'�tat pr�t/pas pr�t dans le lobby diffus�

		Donn�es transmises:
			// Identifiant
			NU32 identifiant;

			// Etat
			BEtatPret etat;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET,

	/*
		[Le serveur informe d'un changement de carte]

		L'h�te de la partie change de carte et en informe
		le(s) client(s)

		Donn�es transmises:
			// Identifiant carte
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE,

	/*
		[Le serveur informe du lancement de la partie]

		Le serveur donne le signal du d�but de partie,
		et attribue une position � chaque joueur

		Donn�es transmises:
			// Identifiant carte
			NU32 identifiantCarte;

			// Nombre joueurs
			NU32 nombreJoueurs;

			// Identifiants joueurs
			NU32 identifiantsJoueurs[ nombreJoueurs ];

			// Position joueur (o� l'indice correspond � l'identifiant dans identifiantsJoueurs)
			NSPoint position[ nombreJoueurs ];

			// Taille carte
			NUPoint tailleCarte;

			// Est case remplie?
			NBOOL estCaseRemplie[ tailleCarte.x ][ tailleCarte.y ];
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT,

	/*
		[Le serveur demande l'affichage d'un message sur
		l'�cran du client]

		Le serveur demande l'affichage d'un message comme
		un compte � rebours � l'�cran

		Donn�es transmises:
			// Taille message
			NU32 tailleMessage;

			// Message
			char *message;

			// Dur�e d'affichage
			NU32 dureeAffichage;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER,

	/*
		[Le serveur donne le top au joueur pour qu'ils puissent
		commencer � jouer]

		D�but de la partie

		Donn�es transmises:
			// Identifiant
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE,

	/*
		[Le serveur diffuse un changement de direction]

		Changement de direction d'un joueur

		Donn�es transmises:
			// Identifiant
			NU32 m_identifiant;

			// Direction
			NDirection m_direction;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION,

	/*
		[Le serveur diffuse un changement de position]

		Changement de position d'un joueur

		Donn�es transmises:
			// Identifiant
			NU32 m_identifiant;

			// Direction
			NDirection m_direction;

			// Nouvelle position
			NSPoint m_nouvellePosition;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION,

	/*
		[Le serveur diffuse une pose de bombe]

		Pose de bombe

		Donn�es transmises:
			// Identifiant joueur
			NU32 m_identifiantJoueur;

			// Identifiant bombe
			NU32 m_identifiantBombe;

			// Position
			NSPoint m_position;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE,

	/*
		[Le serveur refuse une pose de bombe � un client]

		Refus d'une pose de bombe

		Donn�es transmises:
			// Identifiant joueur
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE,

	/*
		[Le serveur pr�vient qu'un bombe explose]

		Une bombe explose

		Donn�es transmises:
			// Identifiant bombe
			NU32 m_identifiantBombe;

			// Puissance
			NU32 m_puissance;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE,

	/*
		[Le serveur pr�vient qu'un bloc rempli a �t� d�truit]

		Destruction d'un bloc rempli

		Donn�es transmises:
			// Position du bloc
			NSPoint m_position;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT,

	/*
		[Le serveur annonce l'apparition d'un bonus]

		Un bonus apparait

		Donn�es transmises:
			// Position
			NSPoint m_position;

			// Type de bonus
			BListeBonus m_type;

			// Dur�e
			NU32 m_dureeBonus;

			// Identifiant bonus
			NU32 m_identifiantBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS,

	/*
		[Le serveur annonce la disparition d'un bonus]

		Disparition d'un bonus

		Donn�es transmises:
			// Identifiant bonus
			NU32 m_identifiantBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS,

	/*
		[Le serveur signale � un client qu'il a eut un bonus]

		Un joueur marche sur un bonus

		Donn�es transmises:
			// Type bonus (BListeBonus)
			NU32 m_typeBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS,

	/*
		[Le serveur annonce la mort d'un joueur]

		Un joueur meurt

		Donn�es transmises:
			// Identifiant joueur
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR,

	/*
		[Le serveur informe que la partie est finie]

		Fin de la partie

		Donn�es transmises:
			// Type de fin de partie
			BTypeFinPartie m_type;

			// Identifiant du joueur gagnant
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE,

	// Fin
	BTYPES_PACKET
} BTypePacket;

// V�rifier
nassert( sizeof( BTypePacket ) == sizeof( NU32 ),
	"Projet::Commun::Reseau::Packet::BTypePacket" );

#endif // !PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET

