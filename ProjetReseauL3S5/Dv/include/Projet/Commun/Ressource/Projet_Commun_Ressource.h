#ifndef PROJET_COMMUN_RESSOURCE_PROTECT
#define PROJET_COMMUN_RESSOURCE_PROTECT

/*
	Définitions relatives aux ressources

	@author SOARES Lucas
*/

// -----------------------------------
// namespace Projet::Commun::Ressource
// -----------------------------------

// namespace Projet::Commun::Ressource::Audio
#include "Audio/Projet_Commun_Ressource_Audio.h"

// namespace Projet::Commun::Ressource::Animation
#include "Animation/Projet_Commun_Ressource_Animation.h"

// namespace Projet::Commun::Ressource::Bonus
#include "Bonus/Projet_Commun_Ressource_Bonus.h"

// struct Projet::Commun::Ressource::BRessource
#include "Projet_Commun_Ressource_BRessource.h"

// namespace Projet::Commun::Ressource::Client
#include "Client/Projet_Commun_Ressource_Client.h"

#endif // PROJET_COMMUN_RESSOURCE_PROTECT

