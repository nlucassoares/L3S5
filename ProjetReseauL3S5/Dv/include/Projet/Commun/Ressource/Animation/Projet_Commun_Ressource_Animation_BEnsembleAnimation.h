#ifndef PROJET_COMMUN_RESSOURCE_ANIMATION_BENSEMBLEANIMATION_PROTECT
#define PROJET_COMMUN_RESSOURCE_ANIMATION_BENSEMBLEANIMATION_PROTECT

// --------------------------------------------------------
// struct Projet::Commun::Ressource::Bombe::BAnimationBombe
// --------------------------------------------------------

typedef struct BAnimationBombe
{
	// Animation
	NAnimation *m_animation[ BLISTE_ANIMATIONS ];
} BEnsembleAnimation;

/* Construire */
__ALLOC BEnsembleAnimation *Projet_Commun_Ressource_Animation_BEnsembleAnimation_Construire( const NFenetre* );

/* D�truire */
void Projet_Commun_Ressource_Animation_BEnsembleAnimation_Detruire( BEnsembleAnimation** );

/* Obtenir */
const NAnimation *Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( const BEnsembleAnimation*,
	BListeAnimation );

/* Update */
void Projet_Commun_Ressource_Animation_BEnsembleAnimation_Update( BEnsembleAnimation* );

/* Afficher */
NBOOL Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher( const BEnsembleAnimation*,
	BListeAnimation,
	NU32 contenuAnimation,
	NSPoint position,
	NU32 zoom );
NBOOL Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher2( const BEnsembleAnimation*,
	BListeAnimation,
	NU32 contenuAnimation,
	NSPoint position,
	NU32 zoom,
	NU32 frame );

#endif // !PROJET_COMMUN_RESSOURCE_ANIMATION_BENSEMBLEANIMATION_PROTECT

