#ifndef PROJET_COMMUN_RESSOURCE_BONUS_PROTECT
#define PROJET_COMMUN_RESSOURCE_BONUS_PROTECT

// ------------------------------------------
// namespace Projet::Commun::Ressource::Bonus
// ------------------------------------------

// Lien vers fichier image bonus
static const char BLIEN_RESSOURCE_ICONE_BONUS[ 32 ] = "Assets/Bonus/Bonus.png";

// struct Projet::Commun::Ressource::Bonus::BRessourceBonus
#include "Projet_Commun_Ressource_Bonus_BRessourceBonus.h"

#endif // !PROJET_COMMUN_RESSOURCE_BONUS_PROTECT

