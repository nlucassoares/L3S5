#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_PROTECT

// -------------------------------------------------
// namespace Projet::Commun::Ressource::Audio::Effet
// -------------------------------------------------

// Volume des effets sonores
static const NU32 BVOLUME_EFFET_SONORE	= 100;

// enum Projet::Commun::Ressource::Audio::Effet::BListeEffetSonore
#include "Projet_Commun_Ressource_Audio_Effet_BListeEffetSonore.h"

// struct Projet::Commun::Ressource::Audio::Effet::BEffetSonore
#include "Projet_Commun_Ressource_Audio_Effet_BEffetSonore.h"

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_PROTECT

