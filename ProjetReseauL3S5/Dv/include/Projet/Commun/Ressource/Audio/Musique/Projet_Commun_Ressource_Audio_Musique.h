#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_PROTECT

// ---------------------------------------------------
// namespace Projet::Commun::Ressource::Audio::Musique
// ---------------------------------------------------

// enum Projet::Commun::Ressource::Audio::Musique::BListeMusique
#include "Projet_Commun_Ressource_Audio_Musique_BListeMusique.h"

// struct Projet::Commun::Ressource::Audio::Musique::BMusique
#include "Projet_Commun_Ressource_Audio_Musique_BMusique.h"

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_PROTECT

