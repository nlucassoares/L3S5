#ifndef PROJET_COMMUN_MONDE_BCONFIGURATIONMONDE_PROTECT
#define PROJET_COMMUN_MONDE_BCONFIGURATIONMONDE_PROTECT

// -------------------------------------------------
// struct Projet::Commun::Monde::BConfigurationMonde
// -------------------------------------------------

typedef struct BConfigurationMonde
{
	// Carte s�lectionn�e
	NU32 m_identifiantCarteSelectionnee;
} BConfigurationMonde;

/* Construire */
__ALLOC BConfigurationMonde *Projet_Commun_Monde_BConfigurationMonde_Construire( void );

/* D�truire */
void Projet_Commun_Monde_BConfigurationMonde_Detruire( BConfigurationMonde** );

/* Obtenir identifiant carte */
NU32 Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( const BConfigurationMonde* );

/* D�finir l'identifiant carte */
void Projet_Commun_Monde_BConfigurationMonde_DefinirIdentifiantCarte( BConfigurationMonde*,
	NU32 id );

#endif // !PROJET_COMMUN_MONDE_BCONFIGURATIONMONDE_PROTECT

