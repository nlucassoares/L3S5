#ifndef PROJET_COMMUN_MONDE_PROTECT
#define PROJET_COMMUN_MONDE_PROTECT

// -------------------------------
// namespace Projet::Commun::Monde
// -------------------------------

// namespace Projet::Commun::Monde::FinPartie
#include "FinPartie/Projet_Commun_Monde_FinPartie.h"

// struct Projet::Commun::Monde::BConfigurationMonde
#include "Projet_Commun_Monde_BConfigurationMonde.h"

// struct Projet::Commun::Monde::BMonde
#include "Projet_Commun_Monde_BMonde.h"

#endif // !PROJET_COMMUN_MONDE_PROTECT

