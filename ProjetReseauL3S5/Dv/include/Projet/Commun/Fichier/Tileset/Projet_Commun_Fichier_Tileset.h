#ifndef PROJET_COMMUN_FICHIER_TILESET_PROTECT
#define PROJET_COMMUN_FICHIER_TILESET_PROTECT

// ------------------------------------------
// namespace Projet::Commun::Fichier::Tileset
// ------------------------------------------

// Extension des fichiers tileset
static const char BEXTENSION_FICHIER_TILESET[ 32 ] = "btl";

// enum Projet::Commun::Fichier::Tileset::BClefFichierTileset
#include "Projet_Commun_Fichier_Tileset_BClefFichierTileset.h"

#endif // !PROJET_COMMUN_FICHIER_TILESET_PROTECT

