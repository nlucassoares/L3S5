#ifndef PROJET_COMMUN_CARTE_BLOC_BBLOC_PROTECT
#define PROJET_COMMUN_CARTE_BLOC_BBLOC_PROTECT

/*
	Un bloc d'une carte, compos� d'un type, d'un type de colision,
	etc.

	@author SOARES Lucas
*/

// -----------------------------------------
// struct Projet::Commun::Carte::Bloc::BBloc
// -----------------------------------------

// Bloc aucun
#define BBLOC_CARTE_AUCUN (NU32)~0

typedef struct BBloc
{
	// Type de bloc
	BTypeBloc m_type;

	// Tileset utilis�
	NU32 m_tileset[ BCOUCHES_CARTE ];

	// Position dans le tileset
	NUPoint m_positionTileset[ BCOUCHES_CARTE ];
} BBloc;

/* Sauvegarder */
NBOOL Projet_Commun_Carte_Bloc_BBloc_Sauvegarder( BBloc*,
	NFichierBinaire* );

/* Charger */
NBOOL Projet_Commun_Carte_Bloc_BBloc_Charger( __OUTPUT BBloc*,
	NFichierBinaire* );

#endif // !PROJET_COMMUN_CARTE_BLOC_BBLOC_PROTECT

