#ifndef PROJET_COMMUN_CARTE_TILESET_PROTECT
#define PROJET_COMMUN_CARTE_TILESET_PROTECT

// ----------------------------------------
// namespace Projet::Commun::Carte::Tileset
// ----------------------------------------

// Taille d'une case de tileset
static const NUPoint BTAILLE_CASE_TILESET =
{
	16,
	16
};

// struct Projet::Commun::Carte::Tileset::BTileset
#include "Projet_Commun_Carte_Tileset_BTileset.h"

// namespace Projet::Commun::Carte::Tileset::Chargement
#include "Chargement/Projet_Commun_Carte_Tileset_Chargement.h"

#endif // !PROJET_COMMUN_CARTE_TILESET_PROTECT

