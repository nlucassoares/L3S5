#ifndef PROJET_COMMUN_CARTE_ETAT_CASE_BCASEETATCARTE_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_CASE_BCASEETATCARTE_PROTECT

// --------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::BCaseEtatCarte
// --------------------------------------------------------

typedef struct BCaseEtatCarte
{
	// Est rempli?
	NBOOL m_estRempli;

	// Etat bombe
	BEtatBombe *m_etatBombe;

	// Etat animation explosion
	BEtatAnimationExplosion *m_etatExplosion;

	// Etat bonus
	BBonusCase *m_etatBonus;
} BCaseEtatCarte;

#endif // !PROJET_COMMUN_CARTE_ETAT_CASE_BCASEETATCARTE_PROTECT