#ifndef PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_PROTECT

// --------------------------------------------------
// namespace Projet::Commun::Carte::Etat::Case::Bonus
// --------------------------------------------------

// Proportion de temps �coul� (TEMPS/BPROPORTION_AVANT_CLIGNOTEMENT_DISPARITION_BONUS) avant d�but clignotement (pour signaler disparition)
static const NU32 BPROPORTION_AVANT_CLIGNOTEMENT_DISPARITION_BONUS	= 4;

// Vitesse inc/d�cr�mentation alpha
static const NU32 BVITESSE_INCDEC_ALPHA_DISPARITION_BONUS			= 5;

// struct Projet::Commun::Carte::Etat::Case::Bonus::BBonusCase
#include "Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase.h"

#endif // !PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_PROTECT

