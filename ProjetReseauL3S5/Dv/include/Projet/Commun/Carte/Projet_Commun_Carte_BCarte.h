#ifndef PROJET_COMMUN_CARTE_BCARTE_PROTECT
#define PROJET_COMMUN_CARTE_BCARTE_PROTECT

/*
	La d�finition d'une carte

	@author SOARES Lucas
*/

// ------------------------------------
// struct Projet::Commun::Carte::BCarte
// ------------------------------------

typedef struct BCarte
{
	// Handle de la carte
	NU32 m_handle;

	// Nom de l'ar�ne
	char *m_nom;

	// Taille
	NUPoint m_taille;

	// Case de la map
	BBloc **m_case;

	// Bloc de remplissage
	NU32 m_tilesetBlocRemplissage;
	NUPoint m_blocRemplissage;

	// Bloc de bordure
	NU32 m_tilesetBlocBordure;
	NUPoint m_blocBordure;

	// Identifiant musique
	NU32 m_musique;

	// Checksum
	NU32 m_checksum;

	// Positions de d�part
	NSPoint m_positionDepart[ BNOMBRE_MAXIMUM_JOUEUR ];
} BCarte;

/* Construire */
__ALLOC BCarte *Projet_Commun_Carte_BCarte_Construire( NU32 handle,
	const char *nom,
	NUPoint taille );
__ALLOC BCarte *Projet_Commun_Carte_BCarte_Construire2( const char *lien );

/* D�truire */
void Projet_Commun_Carte_BCarte_Detruire( BCarte** );

/* Sauvegarder */
NBOOL Projet_Commun_Carte_BCarte_Sauvegarder( const BCarte*,
	const char* );

/* Obtenir la taille */
const NUPoint *Projet_Commun_Carte_BCarte_ObtenirTaille( const BCarte* );

/* Obtenir le nom */
const char *Projet_Commun_Carte_BCarte_ObtenirNom( const BCarte* );

/* Obtenir les cases */
const BBloc **Projet_Commun_Carte_BCarte_ObtenirCases( const BCarte* );

/* Obtenir position de d�part */
const NSPoint *Projet_Commun_Carte_BCarte_ObtenirPositionDepart( const BCarte*,
	NU32 );

/* Obtenir l'handle */
NU32 Projet_Commun_Carte_BCarte_ObtenirHandle( const BCarte* );

/* Obtenir le checksum */
NU32 Projet_Commun_Carte_BCarte_ObtenirChecksum( const BCarte* );

/* Obtenir le tileset de bloc de remplissage */
NU32 Projet_Commun_Carte_BCarte_ObtenirTilesetBlocRemplissage( const BCarte* );

/* Obtenir le bloc de remplissage */
NUPoint Projet_Commun_Carte_BCarte_ObtenirBlocRemplissage( const BCarte* );

/* Obtenir le tileset de bloc de bordure */
NU32 Projet_Commun_Carte_BCarte_ObtenirTilesetBlocBordure( const BCarte* );

/* Obtenir le bloc de bordure */
NUPoint Projet_Commun_Carte_BCarte_ObtenirBlocBordure( const BCarte* );

/* Obtenir la musique */
NU32 Projet_Commun_Carte_BCarte_ObtenirMusique( const BCarte* );

/* Est position correcte? */
NBOOL Projet_Commun_Carte_BCarte_EstPositionCorrecte( const BCarte*,
	NSPoint );

/* Compter le nombre de bloc remplissable */
NU32 Projet_Commun_Carte_BCarte_CompterBlocRemplissable( const BCarte* );

/* D�finir le nom */
NBOOL Projet_Commun_Carte_BCarte_DefinirNom( BCarte*,
	const char* );

/* D�finir la taille */
NBOOL Projet_Commun_Carte_BCarte_DefinirTaille( BCarte*,
	NUPoint );

/* D�finir position de d�part */
void Projet_Commun_Carte_BCarte_DefinirPositionDepart( BCarte*,
	NU32,
	NSPoint );

/* D�finir le bloc de remplissage */
void Projet_Commun_Carte_BCarte_DefinirBlocRemplissage( BCarte*,
	NU32 tileset,
	NUPoint positionTileset );

/* D�finir le bloc de bordure */
void Projet_Commun_Carte_BCarte_DefinirBlocBordure( BCarte*,
	NU32 tileset,
	NUPoint positionTileset );

/* D�finir la musique */
void Projet_Commun_Carte_BCarte_DefinirMusique( BCarte*,
	NU32 musique );

#endif // !PROJET_COMMUN_CARTE_BCARTE_PROTECT

