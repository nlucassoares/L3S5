#ifndef PROJET_COMMUN_CARTE_BONUS_PROTECT
#define PROJET_COMMUN_CARTE_BONUS_PROTECT

// --------------------------------------
// namespace Projet::Commun::Carte::Bonus
// --------------------------------------

// enum Projet::Commun::Carte::Bonus::BListeBonus
#include "Projet_Commun_Carte_Bonus_BListeBonus.h"

/* G�rer la prise de bonus */
void Projet_Commun_Carte_Bonus_GererPriseBonus( BEtatClient*,
	BListeBonus );

#endif // !PROJET_COMMUN_CARTE_BONUS_PROTECT

