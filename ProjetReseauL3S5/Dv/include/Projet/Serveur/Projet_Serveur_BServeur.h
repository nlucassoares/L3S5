#ifndef PROJET_SERVEUR_BSERVEUR_PROTECT
#define PROJET_SERVEUR_BSERVEUR_PROTECT

// --------------------------------
// struct Projet::Serveur::BServeur
// --------------------------------

typedef struct BServeur
{
	// Serveur
	NServeur *m_serveur;

	// Est en cours
	NBOOL m_estEnCours;

	// Cache clients
	BCacheClient *m_cacheClient;

	// Thread de gestion
	HANDLE m_threadGestion;

	// Ensemble des cartes
	BEnsembleCarte *m_carte;

	// Ressources
	BRessource *m_ressource;

	// Fen�tre
	NFenetre *m_fenetre;

	// Monde
	BMonde *m_monde;

	// Monde serveur
	BMondeServeur *m_mondeServeur;

	// Etat
	BEtatServeur m_etat;

	// Port serveur
	NU32 m_port;

	// Configuration monde
	const BConfigurationMonde *m_configurationMonde;
} BServeur;

/* Construire */
__ALLOC BServeur *Projet_Serveur_BServeur_Construire( NU32 port,
	const BConfigurationMonde* );

/* D�truire */
void Projet_Serveur_BServeur_Detruire( BServeur** );

/* Obtenir ressources */
const BRessource *Projet_Serveur_BServeur_ObtenirRessource( const BServeur* );

/* Obtenir cache clients */
const BCacheClient *Projet_Serveur_BServeur_ObtenirCacheClient( const BServeur* );

/* Obtenir checksum ressources */
NU32 Projet_Serveur_BServeur_ObtenirChecksumRessource( const BServeur* );

/* Obtenir configuration monde */
const BConfigurationMonde *Projet_Serveur_BServeur_ObtenirConfigurationMonde( const BServeur* );

/* Obtenir monde */
const BMonde *Projet_Serveur_BServeur_ObtenirMonde( const BServeur* );

/* Est en cours? */
NBOOL Projet_Serveur_BServeur_EstEnCours( const BServeur* );

/* Lancer le serveur */
NBOOL Projet_Serveur_BServeur_Lancer( BServeur* );

/* Ajouter un client */
NBOOL Projet_Serveur_BServeur_AjouterClient( BServeur*,
	NClientServeur *client );

/* Supprimer un client */
NBOOL Projet_Serveur_BServeur_SupprimerClient( BServeur*,
	NClientServeur* );

/* Envoyer packet (si cache prot�g�) */
NBOOL Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( BServeur*,
	__WILLBEOWNED NPacket* );

/* Envoyer packet (si cache non prot�g�) */
NBOOL Projet_Serveur_BServeur_EnvoyerPacketTous( BServeur*,
	__WILLBEOWNED NPacket* );

/* Traiter packet */
NBOOL Projet_Serveur_BServeur_TraiterPacket( BServeur*,
	const NClientServeur*,
	BTypePacket,
	const void *data );

/* Passer en attente de confirmation de lancement des clients */
NBOOL Projet_Serveur_BServeur_ValiderLancementClient( BServeur* );

#endif // !PROJET_SERVEUR_BSERVEUR_PROTECT

