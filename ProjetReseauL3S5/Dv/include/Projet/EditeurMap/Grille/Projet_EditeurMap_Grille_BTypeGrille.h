#ifndef PROJET_EDITEURMAP_GRILLE_BTYPEGRILLE_PROTECT
#define PROJET_EDITEURMAP_GRILLE_BTYPEGRILLE_PROTECT

// --------------------------------------------
// enum Projet::EditeurMap::Grille::BTypeGrille
// --------------------------------------------

typedef enum BTypeGrille
{
	BTYPE_GRILLE_TILESET,
	BTYPE_GRILLE_TYPEBLOC,

	BTYPES_GRILLE
} BTypeGrille;

#endif // !PROJET_EDITEURMAP_GRILLE_BTYPEGRILLE_PROTECT

